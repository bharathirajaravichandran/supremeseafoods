-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 04, 2018 at 07:55 PM
-- Server version: 5.5.59-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `supremeseafoods`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=28 ;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/', NULL, '2018-03-06 23:51:21'),
(2, 0, 2, 'Admin', 'fa-tasks', '', NULL, NULL),
(3, 2, 3, 'Users', 'fa-users', 'auth/users', NULL, NULL),
(4, 2, 4, 'Roles', 'fa-user', 'auth/roles', NULL, NULL),
(5, 2, 5, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL),
(6, 2, 6, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL),
(7, 2, 7, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL),
(8, 10, 12, 'Product Categories', 'fa-barcode', 'categories', '2018-02-26 05:23:05', '2018-04-20 07:09:45'),
(9, 0, 22, 'CMS Pages', 'fa-book', 'cmspages', '2018-02-26 05:42:23', '2018-04-20 07:09:45'),
(10, 0, 9, 'Product Management', 'fa-bars', NULL, '2018-02-26 23:34:07', '2018-04-11 05:09:09'),
(11, 10, 13, 'Product List', 'fa-apple', 'addproduct', '2018-02-26 23:34:22', '2018-04-20 07:09:45'),
(12, 0, 8, 'User Management', 'fa-user-plus', 'users', '2018-03-02 00:27:00', '2018-04-11 05:09:09'),
(13, 0, 23, 'Wish List', 'fa-apple', 'wishlist', '2018-03-05 00:46:07', '2018-04-20 07:09:45'),
(14, 10, 11, 'Whats Available Today', 'fa-archive', 'whatsavailable', '2018-03-07 00:14:55', '2018-04-20 07:05:00'),
(15, 0, 21, 'Whole Sale Enquiry', 'fa-balance-scale', 'wholesaleenquiry', '2018-03-08 03:10:07', '2018-04-20 07:09:45'),
(18, 0, 24, 'Testimonials', 'fa-comments-o', '/testimonials', '2018-04-11 05:10:11', '2018-04-20 07:05:00'),
(19, 0, 14, 'Orders', 'fa-rupee', '/orders', '2018-04-12 05:10:03', '2018-04-20 07:09:45'),
(20, 0, 15, 'Payments Management', 'fa-money', '/payments', '2018-04-12 05:29:09', '2018-04-20 07:09:45'),
(21, 20, 17, 'Payments Modes', 'fa-credit-card-alt', '/paymentmodes', '2018-04-12 05:30:08', '2018-04-20 07:09:45'),
(22, 20, 16, 'Payments', 'fa-rupee', '/payments', '2018-04-12 05:33:07', '2018-04-20 07:09:45'),
(23, 20, 18, 'Invoices', 'fa-newspaper-o', 'invoices', '2018-04-13 07:01:26', '2018-04-20 07:09:45'),
(24, 20, 19, 'Transactions', 'fa-exchange', '/transactions', '2018-04-16 05:04:18', '2018-04-20 07:09:45'),
(25, 20, 20, 'Tracking', 'fa-truck', '/tracking', '2018-04-16 07:52:41', '2018-04-20 07:09:45'),
(26, 10, 10, 'Hub', 'fa-bank', '/hub', '2018-04-20 07:04:44', '2018-04-20 07:05:00'),
(27, 0, 25, 'Notifications', 'fa-bullhorn', '/notifications', '2018-04-23 13:17:26', '2018-04-23 13:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE IF NOT EXISTS `admin_operation_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_operation_log_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=209 ;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-25 05:23:01', '2018-04-25 05:23:01'),
(2, 1, 'admin/orders', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:32:12', '2018-04-25 05:32:12'),
(3, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:32:17', '2018-04-25 05:32:17'),
(4, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:32:21', '2018-04-25 05:32:21'),
(5, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-04-25 05:32:21', '2018-04-25 05:32:21'),
(6, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:50:52', '2018-04-25 05:50:52'),
(7, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:51:17', '2018-04-25 05:51:17'),
(8, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:51:19', '2018-04-25 05:51:19'),
(9, 1, 'admin/whatsavailable', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:51:22', '2018-04-25 05:51:22'),
(10, 1, 'admin', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:51:27', '2018-04-25 05:51:27'),
(11, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:53:23', '2018-04-25 05:53:23'),
(12, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:53:27', '2018-04-25 05:53:27'),
(13, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:53:53', '2018-04-25 05:53:53'),
(14, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:53:55', '2018-04-25 05:53:55'),
(15, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:54:12', '2018-04-25 05:54:12'),
(16, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 05:54:14', '2018-04-25 05:54:14'),
(17, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"Fish","catdescription":"Fish","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 05:58:09', '2018-04-25 05:58:09'),
(18, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 05:58:10', '2018-04-25 05:58:10'),
(19, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 06:02:45', '2018-04-25 06:02:45'),
(20, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"Fish","catdescription":"Fisher Bobble","status":"0","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 06:03:19', '2018-04-25 06:03:19'),
(21, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 06:03:19', '2018-04-25 06:03:19'),
(22, 1, 'admin/categories/1', 'DELETE', '127.0.0.1', '{"_method":"delete","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S"}', '2018-04-25 06:05:47', '2018-04-25 06:05:47'),
(23, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 06:05:48', '2018-04-25 06:05:48'),
(24, 1, 'admin/categories/2', 'DELETE', '127.0.0.1', '{"_method":"delete","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S"}', '2018-04-25 06:05:55', '2018-04-25 06:05:55'),
(25, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 06:05:55', '2018-04-25 06:05:55'),
(26, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-25 06:48:36', '2018-04-25 06:48:36'),
(27, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 06:49:38', '2018-04-25 06:49:38'),
(28, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 06:49:40', '2018-04-25 06:49:40'),
(29, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-25 06:59:55', '2018-04-25 06:59:55'),
(30, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:00:17', '2018-04-25 07:00:17'),
(31, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:00:19', '2018-04-25 07:00:19'),
(32, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:01:47', '2018-04-25 07:01:47'),
(33, 1, 'admin/whatsavailable', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:01:49', '2018-04-25 07:01:49'),
(34, 1, 'admin/whatsavailable/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:01:53', '2018-04-25 07:01:53'),
(35, 1, 'admin/whatsavailable', 'GET', '127.0.0.1', '[]', '2018-04-25 07:01:54', '2018-04-25 07:01:54'),
(36, 1, 'admin/whatsavailable/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:04:21', '2018-04-25 07:04:21'),
(37, 1, 'admin/hub', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:04:47', '2018-04-25 07:04:47'),
(38, 1, 'admin/hub/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:04:49', '2018-04-25 07:04:49'),
(39, 1, 'admin/whatsavailable', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:05:44', '2018-04-25 07:05:44'),
(40, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:07:58', '2018-04-25 07:07:58'),
(41, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:08:04', '2018-04-25 07:08:04'),
(42, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"FISH","catdescription":"FRESH FISH CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:09:54', '2018-04-25 07:09:54'),
(43, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:09:55', '2018-04-25 07:09:55'),
(44, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:09:58', '2018-04-25 07:09:58'),
(45, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"PRAWNS","catdescription":"FRESH PRAWN CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:10:26', '2018-04-25 07:10:26'),
(46, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:10:26', '2018-04-25 07:10:26'),
(47, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:10:33', '2018-04-25 07:10:33'),
(48, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"CRABS","catdescription":"FRESH crab CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:10:50', '2018-04-25 07:10:50'),
(49, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:10:51', '2018-04-25 07:10:51'),
(50, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:11:02', '2018-04-25 07:11:02'),
(51, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"COMBOS","catdescription":"FRESH FISH CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:11:30', '2018-04-25 07:11:30'),
(52, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:11:31', '2018-04-25 07:11:31'),
(53, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:11:42', '2018-04-25 07:11:42'),
(54, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"IMPORTED","catdescription":"FRESH IMPORTED CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:11:55', '2018-04-25 07:11:55'),
(55, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:11:55', '2018-04-25 07:11:55'),
(56, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:11:57', '2018-04-25 07:11:57'),
(57, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"LIVE","catdescription":"FRESH LIVE CUTS","status":"1","_token":"d9WgusHC1keQZleyPB24DJ5Mf5p4JJwwNi9n3c7S","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 07:12:22', '2018-04-25 07:12:22'),
(58, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 07:12:22', '2018-04-25 07:12:22'),
(59, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:12:30', '2018-04-25 07:12:30'),
(60, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:12:33', '2018-04-25 07:12:33'),
(61, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 07:24:02', '2018-04-25 07:24:02'),
(62, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-25 08:12:47', '2018-04-25 08:12:47'),
(63, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 08:12:54', '2018-04-25 08:12:54'),
(64, 1, 'admin/categories/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 08:12:59', '2018-04-25 08:12:59'),
(65, 1, 'admin/categories', 'POST', '127.0.0.1', '{"catname":"Nandu","catdescription":"FRESH NANDU","status":"1","_token":"s3JuFbVaaLigHMMjvfiPU54qoeC1XLaqAWPr9wUS","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/categories"}', '2018-04-25 08:14:29', '2018-04-25 08:14:29'),
(66, 1, 'admin/categories', 'GET', '127.0.0.1', '[]', '2018-04-25 08:14:30', '2018-04-25 08:14:30'),
(67, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-25 23:34:53', '2018-04-25 23:34:53'),
(68, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 23:35:01', '2018-04-25 23:35:01'),
(69, 1, 'admin/categories/9', 'DELETE', '127.0.0.1', '{"_method":"delete","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-25 23:35:08', '2018-04-25 23:35:08'),
(70, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 23:35:08', '2018-04-25 23:35:08'),
(71, 1, 'admin/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 23:41:06', '2018-04-25 23:41:06'),
(72, 1, 'admin/users/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-25 23:43:58', '2018-04-25 23:43:58'),
(73, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Neelamegam","user_last_name":"M","user_email":"neelamegam93@gmail.com","password":"123456789","password_confirmation":"123456789","user_phone_number":"8012837469","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/users"}', '2018-04-25 23:49:00', '2018-04-25 23:49:00'),
(74, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-04-25 23:49:01', '2018-04-25 23:49:01'),
(75, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Neelamegam","user_last_name":"M","user_email":"neelamegam93@gmail.com","password":"123456789","password_confirmation":"123456789","user_phone_number":"8012837469","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-25 23:51:12', '2018-04-25 23:51:12'),
(76, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-04-25 23:51:13', '2018-04-25 23:51:13'),
(77, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Neelamegam","user_last_name":"M","user_email":"neelamegam93@gmail.com","user_password":"123456789","password_confirmation":"123456789","user_phone_number":"8012837469","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-25 23:52:11', '2018-04-25 23:52:11'),
(78, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-04-25 23:52:11', '2018-04-25 23:52:11'),
(79, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Vignesh","user_last_name":"TR","user_email":"vignesh@gmail.com","user_password":"12345","password_confirmation":"12345","user_phone_number":"987456321","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-25 23:56:23', '2018-04-25 23:56:23'),
(80, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-04-25 23:56:23', '2018-04-25 23:56:23'),
(81, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Vignesh","user_last_name":"TR","user_email":"vignesh@gmail.com","user_password":"123456","password_confirmation":"123456","user_phone_number":"9874563211","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-25 23:56:48', '2018-04-25 23:56:48'),
(82, 1, 'admin/users/create', 'GET', '127.0.0.1', '[]', '2018-04-25 23:56:48', '2018-04-25 23:56:48'),
(83, 1, 'admin/users', 'POST', '127.0.0.1', '{"user_first_name":"Vignesh","user_last_name":"TR","user_email":"vignesh@gmail.com","user_password":"123456","password_confirmation":"123456","user_phone_number":"9874563211","user_community":"0","user_status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w"}', '2018-04-26 00:00:56', '2018-04-26 00:00:56'),
(84, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-04-26 00:00:56', '2018-04-26 00:00:56'),
(85, 1, 'admin/testimonials', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-26 00:02:32', '2018-04-26 00:02:32'),
(86, 1, 'admin/testimonials/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-26 00:02:36', '2018-04-26 00:02:36'),
(87, 1, 'admin/testimonials', 'POST', '127.0.0.1', '{"content":"My company needed a new design for our new website and we decided to stop on Brave. No HTML template that we\\u2019ve seen before hasn\\u2019t even come close to what your designers and developers done. It\\u2019s a truly amazing solution, and I recommend it.","user_id":"1","status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/testimonials"}', '2018-04-26 00:08:31', '2018-04-26 00:08:31'),
(88, 1, 'admin/testimonials', 'GET', '127.0.0.1', '[]', '2018-04-26 00:08:31', '2018-04-26 00:08:31'),
(89, 1, 'admin/testimonials/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-26 00:08:54', '2018-04-26 00:08:54'),
(90, 1, 'admin/testimonials', 'POST', '127.0.0.1', '{"content":"This template is a universal solution, which I was looking for so long. With Brave, I can now showcase all my projects and receive instant feedback from my website visitors. It is very easy to use and at the same time a very powerful product.","user_id":"2","status":"1","_token":"1H3qt0PbjKf0dMh4jEnNPShChIIgm2x6fXP2pI9w","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/testimonials"}', '2018-04-26 00:09:01', '2018-04-26 00:09:01'),
(91, 1, 'admin/testimonials', 'GET', '127.0.0.1', '[]', '2018-04-26 00:09:01', '2018-04-26 00:09:01'),
(92, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-26 00:26:33', '2018-04-26 00:26:33'),
(93, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-26 00:26:35', '2018-04-26 00:26:35'),
(94, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-04-26 06:23:49', '2018-04-26 06:23:49'),
(95, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-27 01:35:15', '2018-04-27 01:35:15'),
(96, 1, 'admin/hub', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 01:35:26', '2018-04-27 01:35:26'),
(97, 1, 'admin/hub/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 01:35:29', '2018-04-27 01:35:29'),
(98, 1, 'admin/hub/upload', 'POST', '127.0.0.1', '{"_token":"3z5t1yCneL7817virQH4rUJ8pvxHYf75cJFvi369","hubname":null}', '2018-04-27 01:35:30', '2018-04-27 01:35:30'),
(99, 1, 'admin/hub/create', 'GET', '127.0.0.1', '[]', '2018-04-27 01:35:31', '2018-04-27 01:35:31'),
(100, 1, 'admin/hub/upload', 'POST', '127.0.0.1', '{"_token":"3z5t1yCneL7817virQH4rUJ8pvxHYf75cJFvi369","hubname":null}', '2018-04-27 01:39:47', '2018-04-27 01:39:47'),
(101, 1, 'admin/hub/create', 'GET', '127.0.0.1', '[]', '2018-04-27 01:39:47', '2018-04-27 01:39:47'),
(102, 1, 'admin/hub/create', 'GET', '127.0.0.1', '[]', '2018-04-27 01:48:12', '2018-04-27 01:48:12'),
(103, 1, 'admin/hub/upload', 'POST', '127.0.0.1', '{"_token":"WizbQfV2ToTAvms7jwup01DCQzurdDQKTGA5RxM5","hubname":null}', '2018-04-27 01:48:19', '2018-04-27 01:48:19'),
(104, 1, 'admin/hub/create', 'GET', '127.0.0.1', '[]', '2018-04-27 01:48:19', '2018-04-27 01:48:19'),
(105, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-27 06:52:21', '2018-04-27 06:52:21'),
(106, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 06:52:28', '2018-04-27 06:52:28'),
(107, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 06:52:30', '2018-04-27 06:52:30'),
(108, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-27 07:44:22', '2018-04-27 07:44:22'),
(109, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 07:44:28', '2018-04-27 07:44:28'),
(110, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 07:44:29', '2018-04-27 07:44:29'),
(111, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-04-27 07:44:43', '2018-04-27 07:44:43'),
(112, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-04-27 23:36:47', '2018-04-27 23:36:47'),
(113, 1, 'admin/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:36:51', '2018-04-27 23:36:51'),
(114, 1, 'admin/users/1/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:36:58', '2018-04-27 23:36:58'),
(115, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{"user_first_name":"Neelamegam","user_last_name":"M","user_email":"neelamegam93@gmail.com","password":null,"password_confirmation":"123456789","user_phone_number":"8012837469","user_community":"0","user_status":"1","_token":"quvAOoH1CWQLCZrxJj2Yea51LOdo63xDOv4GLYUo","_method":"PUT","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/users"}', '2018-04-27 23:37:39', '2018-04-27 23:37:39'),
(116, 1, 'admin/users/1/edit', 'GET', '127.0.0.1', '[]', '2018-04-27 23:37:39', '2018-04-27 23:37:39'),
(117, 1, 'admin/users/1', 'PUT', '127.0.0.1', '{"user_first_name":"Neelamegam","user_last_name":"M","user_email":"neelamegam93@gmail.com","password":"123456789","password_confirmation":"123456789","user_phone_number":"8012837469","user_community":"0","user_status":"1","_token":"quvAOoH1CWQLCZrxJj2Yea51LOdo63xDOv4GLYUo","_method":"PUT"}', '2018-04-27 23:38:01', '2018-04-27 23:38:01'),
(118, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-04-27 23:38:02', '2018-04-27 23:38:02'),
(119, 1, 'admin/users/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:38:07', '2018-04-27 23:38:07'),
(120, 1, 'admin/users/2', 'PUT', '127.0.0.1', '{"user_first_name":"Vignesh","user_last_name":"TR","user_email":"vignesh@gmail.com","password":"$2y$10$UWMnFijqg8YWrc5EYZM5n.Huz3eCCAubNQdj\\/mYtQ1L.dcjQn.BSS","password_confirmation":"$2y$10$UWMnFijqg8YWrc5EYZM5n.Huz3eCCAubNQdj\\/mYtQ1L.dcjQn.BSS","user_phone_number":"9874563211","user_community":"0","user_status":"1","_token":"quvAOoH1CWQLCZrxJj2Yea51LOdo63xDOv4GLYUo","_method":"PUT","_previous_":"http:\\/\\/127.0.0.1:8000\\/admin\\/users"}', '2018-04-27 23:38:52', '2018-04-27 23:38:52'),
(121, 1, 'admin/users', 'GET', '127.0.0.1', '[]', '2018-04-27 23:38:52', '2018-04-27 23:38:52'),
(122, 1, 'admin/users', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:50:37', '2018-04-27 23:50:37'),
(123, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:50:41', '2018-04-27 23:50:41'),
(124, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-04-27 23:50:47', '2018-04-27 23:50:47'),
(125, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-04-28 02:26:52', '2018-04-28 02:26:52'),
(126, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-02 00:47:32', '2018-05-02 00:47:32'),
(127, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 00:47:45', '2018-05-02 00:47:45'),
(128, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 00:47:48', '2018-05-02 00:47:48'),
(129, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-02 01:29:23', '2018-05-02 01:29:23'),
(130, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 01:31:47', '2018-05-02 01:31:47'),
(131, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 01:31:49', '2018-05-02 01:31:49'),
(132, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:05:22', '2018-05-02 02:05:22'),
(133, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-02 02:05:25', '2018-05-02 02:05:25'),
(134, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 02:05:47', '2018-05-02 02:05:47'),
(135, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 02:05:50', '2018-05-02 02:05:50'),
(136, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish","vanjaram",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:06:51', '2018-05-02 02:06:51'),
(137, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish","vanjaram",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:07:01', '2018-05-02 02:07:01'),
(138, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish","vanjaram",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:07:04', '2018-05-02 02:07:04'),
(139, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish","vanjaram",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:07:31', '2018-05-02 02:07:31'),
(140, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"vanjaram","product_tags":["fish","vanjaram",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"200","cleaned_price":"180","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:07:35', '2018-05-02 02:07:35'),
(141, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 02:07:40', '2018-05-02 02:07:40'),
(142, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-02 02:11:19', '2018-05-02 02:11:19'),
(143, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 02:11:26', '2018-05-02 02:11:26'),
(144, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Vanjaram FIsh","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"Vanjaram","product_tags":["Vanjaram","Fish",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"250","cleaned_price":"220","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:12:41', '2018-05-02 02:12:41'),
(145, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-02 02:12:42', '2018-05-02 02:12:42'),
(146, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 02:12:50', '2018-05-02 02:12:50'),
(147, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Katla Fish","product_categories":["3"],"product_types":["1"],"product_preferences":["1"],"product_sku":"Katla","product_tags":["Katla","fish",null],"combo_price":null,"min_quantity":"8","max_quantity":"9","whole_price":"180","cleaned_price":"150","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 02:13:52', '2018-05-02 02:13:52'),
(148, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-02 02:13:52', '2018-05-02 02:13:52'),
(149, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 03:49:45', '2018-05-02 03:49:45'),
(150, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 03:49:45', '2018-05-02 03:49:45'),
(151, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Black Crabs","product_categories":["5"],"product_types":["1"],"product_preferences":["6"],"product_sku":"Crabs","product_tags":["black","crabs",null],"combo_price":null,"min_quantity":"5","max_quantity":"7","whole_price":"150","cleaned_price":"50","whole_sale":"on","status":"1","_token":"JA7EzCTcrlrnILxDRbksDG1BvFGJCUAsAU2nH8Y6"}', '2018-05-02 03:51:37', '2018-05-02 03:51:37'),
(152, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-02 03:51:38', '2018-05-02 03:51:38'),
(153, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-02 05:13:14', '2018-05-02 05:13:14'),
(154, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 05:13:38', '2018-05-02 05:13:38'),
(155, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 05:13:41', '2018-05-02 05:13:41'),
(156, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-02 07:37:36', '2018-05-02 07:37:36'),
(157, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-02 07:41:22', '2018-05-02 07:41:22'),
(158, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 07:41:30', '2018-05-02 07:41:30'),
(159, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 07:41:34', '2018-05-02 07:41:34'),
(160, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"White Prawns","product_categories":["4"],"product_types":["5"],"product_preferences":["4"],"product_sku":"Prawns","product_tags":["Prawns","White",null],"combo_price":null,"min_quantity":"9","max_quantity":"9.5","whole_price":"180","cleaned_price":"50","whole_sale":"on","status":"1","_token":"BLInJZPgUm9TWQGCKklCkwwoyMAVppfbTaLiPwWZ"}', '2018-05-02 07:43:30', '2018-05-02 07:43:30'),
(161, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-02 07:43:31', '2018-05-02 07:43:31'),
(162, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-02 23:12:09', '2018-05-02 23:12:09'),
(163, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 23:12:15', '2018-05-02 23:12:15'),
(164, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-02 23:12:44', '2018-05-02 23:12:44'),
(165, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Blue Crabs","product_categories":["5"],"product_types":["4"],"product_preferences":["2"],"product_sku":"Crabs","product_tags":["Crabs","blue",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"120","cleaned_price":"50","whole_sale":"on","status":"1","_token":"HVRfxDy8MbSkGs05Tuw3sMbNBXwoMuchqnhyS2uh"}', '2018-05-03 00:11:34', '2018-05-03 00:11:34'),
(166, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-03 00:11:34', '2018-05-03 00:11:34'),
(167, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Blue Crabs","product_categories":["5"],"product_types":["2"],"product_preferences":["1"],"product_sku":"crabblue","product_tags":["Crabs","blue",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"180","cleaned_price":"50","whole_sale":"on","status":"1","_token":"HVRfxDy8MbSkGs05Tuw3sMbNBXwoMuchqnhyS2uh"}', '2018-05-03 00:13:03', '2018-05-03 00:13:03'),
(168, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-03 00:13:03', '2018-05-03 00:13:03'),
(169, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-03 07:15:11', '2018-05-03 07:15:11'),
(170, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-03 07:15:18', '2018-05-03 07:15:18'),
(171, 1, 'admin/addproduct/5/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-03 07:15:25', '2018-05-03 07:15:25'),
(172, 1, 'admin/editprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Blue Crabs","product_categories":["5"],"product_types":["2"],"product_preferences":["1"],"product_sku":"crabblue","product_tags":["Crabs","blue",null],"combo_price":null,"min_quantity":"7","max_quantity":"9","whole_price":"180","cleaned_price":"50","whole_sale":"on","status":"0","_token":"LkKjmE6QO1vLFJaEtcDFXWtAMi815JXOFXEvWljk","id":"5"}', '2018-05-03 07:15:58', '2018-05-03 07:15:58'),
(173, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-03 07:15:59', '2018-05-03 07:15:59'),
(174, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-03 23:44:40', '2018-05-03 23:44:40'),
(175, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-03 23:44:52', '2018-05-03 23:44:52'),
(176, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-03 23:44:55', '2018-05-03 23:44:55'),
(177, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Red Prawns","product_categories":["4"],"product_types":["5"],"product_preferences":["5"],"product_sku":"Red Prawns","product_tags":["Red Prawns","prawn","Red",null],"combo_price":null,"min_quantity":"10","max_quantity":"20","whole_price":"250","cleaned_price":"25","whole_sale":"on","status":"1","_token":"bcnqJlAuyOK5PISwZgZMoW6ser57kqoSzz586IYJ"}', '2018-05-03 23:46:50', '2018-05-03 23:46:50'),
(178, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-03 23:46:50', '2018-05-03 23:46:50'),
(179, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-04 02:22:54', '2018-05-04 02:22:54'),
(180, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Black Sea Fish","product_categories":["3"],"product_types":["2"],"product_preferences":["5"],"product_sku":"Black Sea","product_tags":["Black Sea Fish","Black",null],"combo_price":null,"min_quantity":"25","max_quantity":"30","whole_price":"250","cleaned_price":"25","whole_sale":"off","status":"1","_token":"SEvb81zzZUGGd04fHwsQkTrUve5NE2Cye0Lwobmd"}', '2018-05-04 02:25:26', '2018-05-04 02:25:26'),
(181, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '[]', '2018-05-04 02:25:27', '2018-05-04 02:25:27'),
(182, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-04 08:13:32', '2018-05-04 08:13:32'),
(183, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:13:52', '2018-05-04 08:13:52'),
(184, 1, 'admin/addproduct/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:14:21', '2018-05-04 08:14:21'),
(185, 1, 'admin/addproduct/2/edit', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:14:21', '2018-05-04 08:14:21'),
(186, 1, 'admin/editprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Katla Fish","product_categories":["3"],"product_types":["1"],"product_preferences":["1"],"product_sku":"Katla","product_tags":["Katla","fish",null],"combo_price":null,"min_quantity":"8","max_quantity":"9","whole_price":"180","cleaned_price":"150","whole_sale":"on","status":"0","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP","id":"2"}', '2018-05-04 08:14:50', '2018-05-04 08:14:50'),
(187, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:14:51', '2018-05-04 08:14:51'),
(188, 1, 'admin', 'GET', '127.0.0.1', '[]', '2018-05-04 08:25:20', '2018-05-04 08:25:20'),
(189, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:25:32', '2018-05-04 08:25:32'),
(190, 1, 'admin/categories', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:25:47', '2018-05-04 08:25:47'),
(191, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:25:52', '2018-05-04 08:25:52'),
(192, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:26:41', '2018-05-04 08:26:41'),
(193, 1, 'admin/addproduct', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:34:09', '2018-05-04 08:34:09'),
(194, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:35:58', '2018-05-04 08:35:58'),
(195, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Baracudda(Seela)","product_categories":["3"],"product_types":["1"],"product_preferences":["1"],"product_sku":"Baracudda","product_tags":["Baracudda(Seela)","Baracudda",null],"combo_price":null,"min_quantity":"7","max_quantity":"20","whole_price":"450","cleaned_price":"150","whole_sale":"on","status":"1","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP"}', '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(196, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(197, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:41:48', '2018-05-04 08:41:48'),
(198, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Anchovy Big(Nethili)","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"Anchovy","product_tags":["Anchovy Big(Nethili)","Anchovy",null],"combo_price":null,"min_quantity":"7","max_quantity":"25","whole_price":"425","cleaned_price":"175","whole_sale":"on","status":"1","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP"}', '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(199, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(200, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:44:57', '2018-05-04 08:44:57'),
(201, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Black Pomfret Big Size(Black Vawaal)","product_categories":["3"],"product_types":["2"],"product_preferences":["1"],"product_sku":"Pomfret","product_tags":["Black Pomfret Big Size","Black Vawaal",null],"combo_price":null,"min_quantity":"7","max_quantity":"25","whole_price":"950","cleaned_price":"500","whole_sale":"on","status":"1","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP"}', '2018-05-04 08:48:14', '2018-05-04 08:48:14'),
(202, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:48:16', '2018-05-04 08:48:16'),
(203, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:49:18', '2018-05-04 08:49:18'),
(204, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"blue crab","product_categories":["5"],"product_types":["2"],"product_preferences":["1"],"product_sku":"SC23","product_tags":["blue crab","crab",null],"combo_price":null,"min_quantity":"8","max_quantity":"15","whole_price":"350","cleaned_price":"50","whole_sale":"on","status":"1","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP"}', '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(205, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:51:08', '2018-05-04 08:51:08'),
(206, 1, 'admin/addproduct/create', 'GET', '127.0.0.1', '{"_pjax":"#pjax-container"}', '2018-05-04 08:51:45', '2018-05-04 08:51:45'),
(207, 1, 'admin/saveprd', 'POST', '127.0.0.1', '{"product_type":"0","productname":"Three Spotted Crab(Kan Nandu)","product_categories":["5"],"product_types":["3"],"product_preferences":["1"],"product_sku":"kan Nandu","product_tags":["Three Spotted Crab(Kan Nandu)","kan nandy",null],"combo_price":null,"min_quantity":"8","max_quantity":"27","whole_price":"400","cleaned_price":"50","whole_sale":"on","status":"1","_token":"DXxkbcBurIpepIPVNpbikGJLOBOpaNkk1YtgZ6BP"}', '2018-05-04 08:53:22', '2018-05-04 08:53:22'),
(208, 1, 'admin/addproduct', 'GET', '127.0.0.1', '[]', '2018-05-04 08:53:23', '2018-05-04 08:53:23');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE IF NOT EXISTS `admin_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_permissions_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE IF NOT EXISTS `admin_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_roles_name_unique` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2018-02-23 05:02:32', '2018-02-23 05:02:32'),
(2, 'Delivery Boy', 'Delivery Boy', '2018-02-28 07:54:10', '2018-02-28 07:54:10');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE IF NOT EXISTS `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 8, NULL, NULL),
(1, 9, NULL, NULL),
(1, 13, NULL, NULL),
(1, 17, NULL, NULL),
(1, 18, NULL, NULL),
(1, 19, NULL, NULL),
(1, 20, NULL, NULL),
(1, 21, NULL, NULL),
(1, 22, NULL, NULL),
(1, 24, NULL, NULL),
(1, 25, NULL, NULL),
(1, 26, NULL, NULL),
(1, 27, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE IF NOT EXISTS `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE IF NOT EXISTS `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_users_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$id7frbPWoYMccofPh5d5pOlcJUBP91YcfSzgdXqTdEgeNglkUPv/C', 'Administrator', NULL, 'EcK3XBx0EnfLfoBx5Wljjctm42UqjERFFflz46NNMUn7SCYMtsM1CzfaNU1b', '2018-02-22 23:32:32', '2018-03-02 05:08:43'),
(2, 'Delivery Boy1', '$2y$10$Oq5T9Nk3CNxS4cEkEBgk/.tpBEx6v4NcylQeIAIv/goWJolcHNgeu', 'Delivery Boy1', NULL, NULL, '2018-02-28 07:54:51', '2018-02-28 07:54:51');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE IF NOT EXISTS `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `catdescription` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateddatetime` datetime NOT NULL,
  `cat_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `catname`, `catdescription`, `createddatetime`, `updateddatetime`, `cat_image`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'FISH', 'FRESH FISH CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/d3b430011b3d8c963508015f70a4906d.png', 1, NULL, '2018-04-25 07:09:54', '2018-04-25 07:09:54'),
(4, 'PRAWNS', 'FRESH PRAWN CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/daaf3de9a9434568353af01bd0c0454f.png', 1, NULL, '2018-04-25 07:10:26', '2018-04-25 07:10:26'),
(5, 'CRABS', 'FRESH crab CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/8870e21aece715eee40cf9d9452a07d5.png', 1, NULL, '2018-04-25 07:10:51', '2018-04-25 07:10:51'),
(6, 'COMBOS', 'FRESH FISH CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/e78f810dc86bb9858a1cff171202721a.png', 1, NULL, '2018-04-25 07:11:30', '2018-04-25 07:11:30'),
(7, 'IMPORTED', 'FRESH IMPORTED CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/1a3ca92c1400dd97dc93b74ced667556.png', 1, NULL, '2018-04-25 07:11:55', '2018-04-25 07:11:55'),
(8, 'LIVE', 'FRESH LIVE CUTS', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'public/category_images/9f8aecb01f5c84280889ba8860d8b0f8.png', 1, NULL, '2018-04-25 07:12:22', '2018-04-25 07:12:22');

-- --------------------------------------------------------

--
-- Table structure for table `cuts`
--

CREATE TABLE IF NOT EXISTS `cuts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cut_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cuttypes`
--

CREATE TABLE IF NOT EXISTS `cuttypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cut_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverycharge`
--

CREATE TABLE IF NOT EXISTS `deliverycharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amount` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliverypincode`
--

CREATE TABLE IF NOT EXISTS `deliverypincode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pincode` int(11) NOT NULL,
  `delivery_hours` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hub`
--

CREATE TABLE IF NOT EXISTS `hub` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `hub`
--

INSERT INTO `hub` (`id`, `name`, `status`) VALUES
(13, 'Madurai', 1),
(14, 'Chennai', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_01_04_173148_create_admin_tables', 1);

-- --------------------------------------------------------

--
-- Table structure for table `myfavorites`
--

CREATE TABLE IF NOT EXISTS `myfavorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifyme`
--

CREATE TABLE IF NOT EXISTS `notifyme` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_cost` double(8,2) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `payment_mode` int(11) NOT NULL,
  `delivery_datetime` datetime NOT NULL,
  `createddatetime` datetime NOT NULL,
  `actual_amount_paid` double(8,2) NOT NULL,
  `remaining_amount` double(8,2) NOT NULL,
  `refund_remaining_amount_to_wallet_status` double(8,2) NOT NULL,
  `lastupdated_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderdeliverylog`
--

CREATE TABLE IF NOT EXISTS `orderdeliverylog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deliveryuser_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `updateddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderdetail`
--

CREATE TABLE IF NOT EXISTS `orderdetail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `type_cuts` int(11) NOT NULL,
  `quantity` double(8,2) NOT NULL,
  `product_cost` double(8,2) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderfeedback`
--

CREATE TABLE IF NOT EXISTS `orderfeedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `feedback` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminapporval` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ordertransaction`
--

CREATE TABLE IF NOT EXISTS `ordertransaction` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `tranx_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tranx_id` int(11) NOT NULL,
  `tranx_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tranx_createddatetime` datetime NOT NULL,
  `tranx_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `preferences`
--

CREATE TABLE IF NOT EXISTS `preferences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `preference_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `preference_createddatetime` datetime NOT NULL,
  `preference_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `preferences`
--

INSERT INTO `preferences` (`id`, `preference_name`, `preference_description`, `preference_createddatetime`, `preference_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Heart Friendly', 'Heart Friendly', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(2, 'Women Friendly', 'Women Friendly', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(3, 'Senior Citizen Friendly', 'Senior Citizen Friendly', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(4, 'Popular', 'Popular', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(5, 'Party Special', 'Party Special', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(6, 'Kids Friendly', 'Kids Friendly', '2018-03-01 00:00:00', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `productname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `combo_price` double(8,2) DEFAULT NULL,
  `product_tags` text COLLATE utf8mb4_unicode_ci,
  `min_quantity` int(11) NOT NULL,
  `max_quantity` int(11) NOT NULL,
  `whole_sale` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `whole_price` double(8,2) NOT NULL,
  `cleaned_price` double(8,2) NOT NULL,
  `default_quantity` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updateddatetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product_type`, `productname`, `product_sku`, `combo_price`, `product_tags`, `min_quantity`, `max_quantity`, `whole_sale`, `whole_price`, `cleaned_price`, `default_quantity`, `createdby`, `updatedby`, `createddatetime`, `updateddatetime`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '0', 'Baracudda(Seela)', 'Baracudda', NULL, 'Baracudda(Seela),Baracudda,', 7, 20, 'on', 450.00, 150.00, NULL, 0, 0, '2018-05-04 08:39:52', '0000-00-00 00:00:00', 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(2, '0', 'Anchovy Big(Nethili)', 'Anchovy', NULL, 'Anchovy Big(Nethili),Anchovy,', 7, 25, 'on', 425.00, 175.00, NULL, 0, 0, '2018-05-04 08:44:02', '0000-00-00 00:00:00', 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(3, '0', 'Black Pomfret Big Size(Black Vawaal)', 'Pomfret', NULL, 'Black Pomfret Big Size,Black Vawaal,', 7, 25, 'on', 950.00, 500.00, NULL, 0, 0, '2018-05-04 08:48:15', '0000-00-00 00:00:00', 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(4, '0', 'blue crab', 'SC23', NULL, 'blue crab,crab,', 8, 15, 'on', 350.00, 50.00, NULL, 0, 0, '2018-05-04 08:51:07', '0000-00-00 00:00:00', 1, NULL, '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(5, '0', 'Three Spotted Crab(Kan Nandu)', 'kan Nandu', NULL, 'Three Spotted Crab(Kan Nandu),kan nandy,', 8, 27, 'on', 400.00, 50.00, NULL, 0, 0, '2018-05-04 08:53:22', '0000-00-00 00:00:00', 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `productcategories`
--

CREATE TABLE IF NOT EXISTS `productcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `productcategories`
--

INSERT INTO `productcategories` (`id`, `product_id`, `cat_id`, `createddatetime`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2018-05-04 14:09:52', 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(2, 2, 3, '2018-05-04 14:14:02', 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(3, 3, 3, '2018-05-04 14:18:15', 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(4, 4, 5, '2018-05-04 14:21:07', 1, NULL, '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(5, 5, 5, '2018-05-04 14:23:22', 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `productcuts`
--

CREATE TABLE IF NOT EXISTS `productcuts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `cut_id` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `productimages`
--

CREATE TABLE IF NOT EXISTS `productimages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `productimages`
--

INSERT INTO `productimages` (`id`, `product_id`, `product_image`, `main_image`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, '140952Baracudda-Whole-1000x750pixels-96ppi-Watermark-NEW.jpg', 1, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(2, 1, '140952Baracudda-Whole-1000x750pixels-96ppi-Watermark-NEW-128x96.jpg', 0, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(3, 1, '140952Baracudda-Slices-1000x750pixels-96ppi-Watermark-NEW-2-128x96.jpg', 0, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(4, 1, '140952Baracudda-Slices-1000x750pixels-96ppi-Watermark-NEW-2-128x96 (1).jpg', 0, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(5, 2, '141402Anchovy-Whole-1000x750pixels-96ppi-Watermark-1.jpg', 1, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(6, 2, '141402Anchovy-Whole-1000x750pixels-96ppi-Watermark-1.jpg', 0, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(7, 2, '141402Anchovy-GG-1000x750pixels-96ppi-Watermark-1.jpg', 0, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(8, 2, '141402Anchovy-Headless-1000x750pixels-96ppi-Watermark-1.jpg', 0, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(9, 3, '141815Black-Pomfret-Whole-1000x750pixels-96ppi-Watermark-1.jpg', 1, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(10, 3, '141815Black-Pomfret-Slices-1000x750pixels-96ppi-Watermark4.jpg', 0, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(11, 3, '141815Black-Pomfret-Skinon-Boneon-Cubes-1000x750pixels-96ppi-Watermark3.jpg', 0, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(12, 3, '141815black-pomfret-fillets-watermark.jpg', 0, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(13, 4, '142107Blue-Crab-Whole-1000x750pixels-96ppi-Watermark-2.jpg', 1, 1, NULL, '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(14, 4, '142107Blue-Crab-Whole-1000x750pixels-96ppi-Watermark-2.jpg', 0, 1, NULL, '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(15, 4, '142107Blue-Crab-Whole-1000x750pixels-96ppi-Watermark-2 (1).jpg', 0, 1, NULL, '2018-05-04 08:51:07', '2018-05-04 08:51:07'),
(16, 5, '142322Live-Green-Crab-Whole-1000x750pixels-96ppi-Watermark-1.jpg', 1, 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22'),
(17, 5, '142322Live-Green-Crab-Whole-1000x750pixels-96ppi-Watermark-1.jpg', 0, 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `productpreference`
--

CREATE TABLE IF NOT EXISTS `productpreference` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `preference_id` int(11) NOT NULL,
  `producttype_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `productpreference`
--

INSERT INTO `productpreference` (`id`, `product_id`, `preference_id`, `producttype_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(2, 2, 1, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(3, 3, 1, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(4, 4, 1, 1, NULL, '2018-05-04 08:51:08', '2018-05-04 08:51:08'),
(5, 5, 1, 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `producttypes`
--

CREATE TABLE IF NOT EXISTS `producttypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `producttypes`
--

INSERT INTO `producttypes` (`id`, `product_id`, `type_id`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, NULL, '2018-05-04 08:39:52', '2018-05-04 08:39:52'),
(2, 2, 2, 1, NULL, '2018-05-04 08:44:02', '2018-05-04 08:44:02'),
(3, 3, 2, 1, NULL, '2018-05-04 08:48:15', '2018-05-04 08:48:15'),
(4, 4, 2, 1, NULL, '2018-05-04 08:51:08', '2018-05-04 08:51:08'),
(5, 5, 3, 1, NULL, '2018-05-04 08:53:22', '2018-05-04 08:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `recipe`
--

CREATE TABLE IF NOT EXISTS `recipe` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `recipe_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipe_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipe_method` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipe_ingredients` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipe_main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `updateddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipecategory`
--

CREATE TABLE IF NOT EXISTS `recipecategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `recipeimage`
--

CREATE TABLE IF NOT EXISTS `recipeimage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `recipe_id` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `referalsettings`
--

CREATE TABLE IF NOT EXISTS `referalsettings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referrer_user_amount` int(11) NOT NULL,
  `refered_user_amount` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `shoptiming`
--

CREATE TABLE IF NOT EXISTS `shoptiming` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `staticpages`
--

CREATE TABLE IF NOT EXISTS `staticpages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pagename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pagecontent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `createddatetime` datetime NOT NULL,
  `updateddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_likes` int(11) NOT NULL,
  `posted_date_time` datetime NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `user_id`, `content`, `no_of_likes`, `posted_date_time`, `createddatetime`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'My company needed a new design for our new website and we decided to stop on Brave. No HTML template that we’ve seen before hasn’t even come close to what your designers and developers done. It’s a truly amazing solution, and I recommend it.', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, '2018-04-26 00:08:31', '2018-04-26 00:08:31'),
(2, 2, 'This template is a universal solution, which I was looking for so long. With Brave, I can now showcase all my projects and receive instant feedback from my website visitors. It is very easy to use and at the same time a very powerful product.', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, '2018-04-26 00:09:01', '2018-04-26 00:09:01');

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producttype_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `producttype_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `producttype_createddatetime` datetime NOT NULL,
  `producttype_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `producttype_name`, `producttype_description`, `producttype_createddatetime`, `producttype_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Long', 'Long', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(2, 'Small', 'Small', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(3, 'Fat', 'Fat', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(4, 'Flat', 'Flat', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(5, 'Short', 'Short', '2018-03-01 00:00:00', 1, NULL, NULL, NULL),
(6, 'Shell', 'Shell', '2018-03-01 00:00:00', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `useraddress`
--

CREATE TABLE IF NOT EXISTS `useraddress` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_role` int(11) NOT NULL,
  `user_first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_profile_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_salt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_pwd` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_confirmation` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_phone_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_community` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_referal_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_refered_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_fb_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_gplus_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_device_token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_device_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_dob` datetime NOT NULL,
  `user_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lastlogin` datetime NOT NULL,
  `user_createddatetime` datetime NOT NULL,
  `user_updatedatetime` datetime NOT NULL,
  `user_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role`, `user_first_name`, `user_last_name`, `email`, `password`, `user_profile_image`, `user_salt`, `old_pwd`, `password_confirmation`, `user_phone_number`, `user_community`, `user_referal_code`, `user_refered_by`, `user_fb_id`, `user_gplus_id`, `user_device_token`, `user_device_type`, `user_dob`, `user_location`, `user_lastlogin`, `user_createddatetime`, `user_updatedatetime`, `user_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'Neelamegam', 'M', 'neelamegam93@gmail.com', '$2y$10$faKvVk9BcFysiCUhA2jdUOK9eb0x4O/FAR5buZvOQLPKEZOHMy7Em', 'public/user_images/6e249905fe2d315f8d2a631cf08686ca.jpg', '', '$2y$10$faKvVk9BcFysiCUhA2jdUOK9eb0x4O/FAR5buZvOQLPKEZOHMy7Em', '$2y$10$faKvVk9BcFysiCUhA2jdUOK9eb0x4O/FAR5buZvOQLPKEZOHMy7Em', '8012837469', '0', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Pothumbu,Madurai.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, '2018-04-25 23:51:12', '2018-04-27 23:38:02'),
(2, 0, 'Vignesh', 'TR', 'vignesh@gmail.com', '$2y$10$UWMnFijqg8YWrc5EYZM5n.Huz3eCCAubNQdj/mYtQ1L.dcjQn.BSS', 'public/user_images/51bd4bdd60b3cdf012a5de49ecd49c0c.jpg', '', '$2y$10$UWMnFijqg8YWrc5EYZM5n.Huz3eCCAubNQdj/mYtQ1L.dcjQn.BSS', '$2y$10$UWMnFijqg8YWrc5EYZM5n.Huz3eCCAubNQdj/mYtQ1L.dcjQn.BSS', '9874563211', '0', '', '', '', '', '', '', '0000-00-00 00:00:00', 'Thirunagar,Madurai.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, '2018-04-26 00:00:56', '2018-04-27 23:38:52'),
(3, 0, 'sastha', 'm', 'sastha@gmail.com', '123456789', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:32:50', '2018-04-27 00:32:50'),
(4, 0, 'sastha', 'm', 'sastha@gmail.com', '$2y$10$4lMCPw2oByVA6Ou1YglszeRamVCJ5HdYNjAzJ3x0wD.JzfxV3Ot32', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:33:49', '2018-04-27 00:33:49'),
(5, 0, 'siraj', 'h', 'siraj@gmail.com', '$2y$10$sGkOm/SQhOkbojQD8LQLz.9HM/FHyTlnin8heunIVNL.zS8cosDvy', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:34:40', '2018-04-27 00:34:40'),
(6, 0, 'sastha', 'm', 'sastha@gmail.com', '$2y$10$AsOXlL1/9x2vlKhIoWvnCeghyqcRfOO.RkfyIE1q4y.VJX3n0Kvem', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:38:13', '2018-04-27 00:38:13'),
(7, 0, 'sastha', 'm', 'sastha@gmail.com', '$2y$10$WxeVysu1Ol.yYB2d6tIrmeptm5XqqZQloGDSsV6iNBAucI7e/INUe', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:39:06', '2018-04-27 00:39:06'),
(8, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$rtayX5su8NRyytJVUroa9.NM2PVQxshqx8K/ul2wK3L7HOuod3VeS', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 00:44:53', '2018-04-27 00:44:53'),
(9, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$GkGVQlna3IS3waXFddmC/u.NXA9O67qeR5/VbtjZyUa2Tb2q9W4qy', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 04:13:55', '2018-04-27 04:13:55'),
(10, 0, 'Siraj', 'H', 'Siraj@gmail.com', '$2y$10$G7ggCWJmjhpa7gepN4QYJ.QrSIbTuwZBHF.4wuFkCQOV9BJKL4g0a', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 04:29:35', '2018-04-27 04:29:35'),
(11, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$CF95KKzZikp6LJKu1ij/..3FeV8DWPL3PRkQZuu06E2vpHK8iu/yy', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 05:08:50', '2018-04-27 05:08:50'),
(12, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$QNaJO4HW2/dgUB5gpB32SONJnPzlbwv/cZSl307POJNvSUjpxG49.', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 05:28:43', '2018-04-27 05:28:43'),
(13, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$9U9Y5oj2XS5EJR71A1fzaucQlfYpHqhKxSNULELh1AwDpawGTqgNe', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 05:29:19', '2018-04-27 05:29:19'),
(14, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$/EwoF7cycxXL1Be8rwKH8.nheF2DhSOH21/jiRR061WS./h0/S3au', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 06:26:46', '2018-04-27 06:26:46'),
(15, 0, 'Vengatesh', 'C', 'vengi@gmail.com', '$2y$10$mm.KBKOmf9vGsj4z36FpV.EBejC2QjdxODzEBEx5NjLpHOjD10uNS', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 06:27:41', '2018-04-27 06:27:41'),
(16, 0, 'kannan', 'm', 'kannan@gmail.com', '$2y$10$YzUf4v6gLbeA1fkjxTghD.zFlAoU7jdZ2qjiWpVNc6px4r/mULaj6', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 07:18:31', '2018-04-27 07:18:31'),
(17, 0, 'pandi', 'p', 'pandi@gmail.com', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-27 07:32:52', '2018-04-27 07:32:52'),
(18, 0, 'Prakash', 'K', 'prakash@gmail.com', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-28 01:57:05', '2018-04-28 01:57:05'),
(19, 0, 'Neelamegam', 'K', 'nnnn@gmail.com', '12345', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-28 02:46:34', '2018-04-28 02:46:34'),
(20, 0, 'kannan', 'M', 'kannan@gmail.com', '123456', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-04-28 05:00:57', '2018-04-28 05:00:57'),
(21, 0, 'Neelamegam', 'M', 'neelamegam1234@gmail.com', '123456789', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-05-01 23:17:35', '2018-05-01 23:17:35'),
(22, 0, '', '', 'sastha1234@gmail.com', '$2y$10$hF8NpQ5M1wgT4xsEvdgG.esf.dPyIHA4H1r6IFe3bc0em6cPhjYuK', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'bmNhzbYnuT1S7j6HIa2eEsnCuw2IDwtteHfIrptmGQys6lAMx8Id5A5I3yiT', '2018-05-02 00:00:30', '2018-05-02 00:00:30'),
(23, 0, 'Neelamegam', 'SMR', 'neelamegam2393@gmail.com', '$2y$10$EWIXULUcbB0lrluhH5UvZ.ldf0udlBQDKastwHqLfFbBg/ClHJr7q', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'CY8TTWBfyX8wSDVYg2z8Rr4nUbRx9ODXfotQuTthJ3IwE8ps5jA2p28snBO7', '2018-05-04 06:03:47', '2018-05-04 06:03:47'),
(24, 0, 'Vengatesh', 'C', 'vengi23@gmail.com', '$2y$10$ld3UpKdt9iGZbLgvFfngg.LBb60uJjUMr8i.dFzyoMIo0aUqSPG4m', '', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, NULL, '2018-05-04 07:05:29', '2018-05-04 07:05:29');

-- --------------------------------------------------------

--
-- Table structure for table `wallet`
--

CREATE TABLE IF NOT EXISTS `wallet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `credited_by` int(11) NOT NULL,
  `debited_by` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `whatsavailable`
--

CREATE TABLE IF NOT EXISTS `whatsavailable` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `used_quantity` int(11) NOT NULL,
  `remaining_quantity` int(11) NOT NULL,
  `createddatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `wholesaleenquiry`
--

CREATE TABLE IF NOT EXISTS `wholesaleenquiry` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deliverydatetime` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
