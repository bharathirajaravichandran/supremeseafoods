<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_role');
            $table->string('user_first_name');
            $table->string('user_last_name');
            $table->string('user_email');
            $table->string('user_password');
            $table->string('user_profile_image');
            //$table->string('user_type');
            $table->string('user_salt');
            $table->string('old_pwd');
            $table->string('password_confirmation');
            $table->string('user_phone_number');
            $table->string('user_community');
            $table->string('user_referal_code');
            $table->string('user_refered_by');
            $table->string('user_fb_id');
            $table->string('user_gplus_id');
            $table->text('user_device_token');
            $table->string('user_device_type');
            $table->dateTime('user_dob');
            $table->string('user_location');
            $table->dateTime('user_lastlogin');
            $table->dateTime('user_createddatetime');
            $table->dateTime('user_updatedatetime');
            $table->integer('user_status');
            $table->rememberToken();
            $table->timestamps();
        });
        
        Schema::create('useraddress', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('address');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->integer('pincode');
            $table->integer('phone');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });        
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('catname');
            $table->string('cat_image');
            $table->text('catdescription');
            $table->dateTime('createddatetime');
            $table->dateTime('updateddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });        
        Schema::create('staticpages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pagename');
            $table->text('pagecontent');
            $table->dateTime('createddatetime');
            $table->dateTime('updateddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });     
        Schema::create('preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('preference_name');
            $table->text('preference_description');
            $table->dateTime('preference_createddatetime');
            $table->integer('preference_status');
            $table->rememberToken();
            $table->timestamps();
        }); 
        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('producttype_name');
            $table->text('producttype_description');
            $table->dateTime('producttype_createddatetime');
            $table->integer('producttype_status');
            $table->rememberToken();
            $table->timestamps();
        });  
        Schema::create('productcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('cat_id');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
                              
        Schema::create('cuts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cut_name');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });                        

        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_type');
            $table->string('product_title');
            $table->string('product_sku');
            $table->float('combo_price');
            $table->text('product_tags');
            $table->integer('min_quantity');
            $table->integer('max_quantity');
            $table->string('whole_sale');
            $table->float('whole_price');
            $table->float('cleaned_price');
            $table->integer('default_quantity');
            $table->integer('createdby');
            $table->integer('updatedby');
            $table->string('product_main_image');
            $table->dateTime('createddatetime');
            $table->dateTime('updateddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('productcuts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('cut_id');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
        Schema::create('productpreference', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('preference_id');
            $table->integer('producttype_status');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('producttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('type_id');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('productimages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('product_image');
            $table->integer('main_image');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
        Schema::create('testimonials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('content');
            $table->integer('no_of_likes');
            $table->dateTime('posted_date_time');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
        Schema::create('wallet', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('order_id');
            $table->integer('type');
            $table->float('amount');
            $table->text('description');
            $table->integer('credited_by');
            $table->integer('debited_by');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_type');
            $table->integer('user_id');
            $table->float('order_cost');
            $table->text('address');
            $table->text('city');
            $table->text('state');
            $table->text('country');
            $table->integer('phone');
            $table->integer('payment_mode');
            $table->dateTime('delivery_datetime');
            $table->dateTime('createddatetime');
            $table->float('actual_amount_paid');
            $table->float('remaining_amount');
            $table->float('refund_remaining_amount_to_wallet_status');
            $table->integer('lastupdated_by');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
        Schema::create('orderdetail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('product_id');
            $table->integer('type_cuts');
            $table->float('quantity');
            $table->float('product_cost');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });   
        Schema::create('orderdeliverylog', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deliveryuser_id');
            $table->integer('order_id');
            $table->dateTime('createddatetime');
            $table->dateTime('updateddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
        Schema::create('ordertransaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->string('tranx_type');
            $table->integer('tranx_id');
            $table->text('tranx_details');
            $table->dateTime('tranx_createddatetime');
            $table->integer('tranx_status');
            $table->rememberToken();
            $table->timestamps();
        });       
                                                
        Schema::create('orderfeedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('user_id');
            $table->integer('rating');
            $table->text('feedback');
            $table->integer('adminapporval');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });                                               
        Schema::create('recipecategory', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name');
            $table->text('cat_description');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
        
        Schema::create('recipe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id');
            $table->string('recipe_name');
            $table->text('recipe_description');
            $table->text('recipe_method');
            $table->text('recipe_ingredients');
            $table->string('recipe_main_image');
            $table->integer('created_by');
            $table->dateTime('createddatetime');
            $table->dateTime('updateddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
                 
        Schema::create('recipeimage', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipe_id');
            $table->string('image');
            $table->integer('main_image');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
                
        Schema::create('whatsavailable', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('quantity');
            $table->integer('used_quantity');
            $table->integer('remaining_quantity');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });          
        
        Schema::create('menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id');
            $table->integer('parent_id');
            $table->integer('menu_order');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });      
            
        Schema::create('myfavorites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('product_id');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });        
          
        Schema::create('cuttypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('cut_name');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
                 
        Schema::create('referalsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('referrer_user_amount');
            $table->integer('refered_user_amount');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        }); 
                 
        Schema::create('wholesaleenquiry', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('product_id');
            $table->string('quantity');
            $table->text('description');
            $table->dateTime('deliverydatetime');
            $table->integer('status');
            $table->integer('updated_by');
            $table->rememberToken();
            $table->timestamps();
        });     
             
        Schema::create('deliverycharge', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('amount');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });          
        
        Schema::create('deliverypincode', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pincode');
            $table->string('delivery_hours');
            $table->dateTime('createddatetime');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });  
                
        Schema::create('shoptiming', function (Blueprint $table) {
            $table->increments('id');
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
        
        Schema::create('notifyme', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('product_id');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });          
                                                      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
