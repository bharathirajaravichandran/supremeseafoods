<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
      protected $table = 'useraddress';
      
    protected $fillable = [
      'user_id', 'address','city', 'state', 'country','pincode','phone',
        ];
}
