<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Category;

class CommonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    { 
		
	
          view()->composer('*', function ($view)
        {
             //como menu package 
             @$categories = Category::	limit(6)->get();
             $view->with(compact('categories'));
        });
    

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
