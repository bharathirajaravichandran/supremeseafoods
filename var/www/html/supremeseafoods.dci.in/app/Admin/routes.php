<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->get('wholesaleview/{id}', 'WholesaleenquiryController@wholesaleview');
    $router->post('saveprd', 'ProductController@saveprd');
    $router->post('editprd', 'ProductController@editprd');
    $router->post('saveavailable', 'WhatsavailableController@saveavailable');
    $router->post('editavailable', 'WhatsavailableController@editavailable');
    //$router->get('whatsavailbletoday/{id}', 'WhatsavailableController@whatsavailbletoday');
    $router->get('orderdetailsview/{id}', 'OrdersController@orderdetailsview');
    $router->get('pincodeview/{id}', 'PincodesController@pincodeview');
    $router->get('pincodes/create/{id}', 'PincodesController@create');
    $router->get('invoice/{id}', 'InvoicesController@invoice');
 $router->post('save', 'UsersController@saves');

/*product_ajax kannan*/
       $router->get('getproducts', 'WhatsavailableController@getproducts');
       $router->post('ReportGen', 'WhatsavailableController@saveavailable');
    
    
    //$router->get('/hub/create','HubController@create');
	$router->post('/hub/upload','HubController@showUploadFile');
	$router->post('/pincodes/upload','PincodesController@showUploadFile');
	$router->post('deleteimage','ProductController@deleteimage');
	


    $router->post('ad', 'ReportController@reports');
    /*$router->resource('report', 'ReportController');
    $router->resource('delivery', 'DassignmentController');*/
    
    $router->get('user/view/{q?}', 'UsersController@view'); /*user view kannan*/

    $router->resources([
        'categories'          => CategoryController::class,
        'cmspages'            => StaticpagesController::class,
        'addproduct'          => ProductController::class,
        'productimaeges'      => ProductimagesController::class,
        'users'               => UsersController::class,
        'wishlist'            => MyfavoritesController::class,
        'whatsavailable'      => WhatsavailableController::class,
        'wholesaleenquiry'    => WholesaleenquiryController::class,
        'paymentmodes'        => PaymentModesController::class,
        'payments'            => PaymentsController::class,
		'testimonials'        => TestimonialsController::class,
		'orders'              => OrdersController::class,
        'orderdetails'        => OrderdetailController::class, 
        'invoices'            => InvoicesController::class, 
        'transactions'        => TransactionsController::class, 
        'tracking'            => TrackingController::class, 
        'hub'                 => HubController::class, 
        'pincodes'            => PincodesController::class, 
        'notifications'       => NotificationController::class, 
        'report'              => ReportController::class, 
        'delivery'            => DassignmentController::class, 
        'cuts'                => CutsController::class, 
        'state'               => StateController::class, 
        'deliveryboy'         => Delivery_boyController::class, 
        'site'                => Site_settingsController::class, 
     ]);

});


