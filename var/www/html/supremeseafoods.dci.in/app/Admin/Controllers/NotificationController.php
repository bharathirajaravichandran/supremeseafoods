<?php

namespace App\Admin\Controllers;

use App\Models\Notification;
use App\Models\Users;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use DB;

class NotificationController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
		
		//~ $users = Users::get(); // All users
		//~ $csvExporter = new \Laracsv\Export();
		//~ $csvExporter->build($users, ['user_last_name', 'user_first_name'])->download();
		//~ exit();
		
		
        return Admin::content(function (Content $content) {

             $content->header('Notifications');
            $content->description('To notify the users');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Notifications');
            $content->description('To notify the users');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

           $content->header('Notifications');
            $content->description('To notify the users');

            $content->body($this->form());
        });
    }


    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Notification::class, function (Grid $grid) {
				//$grid->disableCreation();
				
				//$grid->column('user.user_first_name');
				
				  $grid->disableExport();              
				$grid->users()->display(function ($users)
				{
					if($users=='All')
						return 'All';
					else
					{
						$users_array = explode(",",$users);
						$user_names_array = array();
						foreach($users_array as $users_array_item)
						{
							if($users_array_item!='All')
							{
								$user = DB::table('users')->where(['Id' => $users_array_item])->first();
								$user_names_array[] = $user->user_first_name;
							}
						}
						return implode(",",$user_names_array);
					}
                });
                
				$grid->message('Message');
           
				$grid->type('Type');
				
				$grid->status()->display(function ($status)
				{
                return $status == 0 ? "In Active": "Active";
                });
            
				$grid->filter(function($filter){

    // Remove the default id filter
				$filter->disableIdFilter();

    // Add a column filter
				//$filter->like('user.user_first_name', 'Name');
				
				$filter->equal('status', 'Status')->select([0 => 'In Active', 1 => 'Active']);
			    
			    $filter->equal('type', 'Type');


});
      
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Notification::class, function (Form $form) {
			
			$users = Users::all();
			$user_array = array();
			$user_array['All'] = 'All Users';
			foreach($users as $user)
				$user_array[$user->id] = $user->user_first_name;
				
			$form->multipleSelect('users')->options($user_array);
			$form->text('type')->rules('required');
			$form->textarea('message')->rules('required');
			$form->select('status')->options([0 => 'In Active', 1 => 'Active']);
                    $form->disableReset();
        });
    }
}
