<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Tree;

class CategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
			
            $content->header('All categories');

            $content->body($this->grid());
        });
    }





    protected function grid()
    {
        return Admin::grid(Category::class, function (Grid $grid) {

           // $grid->model()->gender(Request::get('gender'));

            //$grid->model()->with('profile')->orderBy('id', 'DESC');

            $grid->paginate(20);

            $grid->id('ID')->sortable();

            //$grid->name()->editable();

            //~ $grid->column('expand')->expand(function () {
//~ 
                //~ $profile = array_only($this->profile, ['homepage', 'gender', 'birthday', 'address', 'last_login_at', 'last_login_ip', 'lat', 'lng']);
//~ 
                //~ return new Table([], $profile);
//~ 
            //~ }, 'Profile');
//
            //~ $grid->column('position')->openMap(function () {
//~ 
                //~ return [$this->profile['lat'], $this->profile['lng']];
//~ 
            //~ }, 'Position');

           // $grid->column('profile.homepage')->urlWrapper();

            //$grid->email()->prependIcon('envelope');

            //$grid->profile()->mobile()->prependIcon('phone');

            //$grid->column('profile.age')->progressBar(['success', 'striped'], 'xs')->sortable();

           // $grid->profile()->age()->sortable();

            $grid->created_at();

            $grid->updated_at();

            //~ $grid->filter(function (Grid\Filter $filter) {
//~ 
//~ //                $filter->disableIdFilter();
//~ 
                //~ $filter->equal('address.province_id', 'Province')
                    //~ ->select(ChinaArea::province()->pluck('name', 'id'))
                    //~ ->load('address.city_id', '/demo/api/china/city');
//~ 
                //~ $filter->equal('address.city_id', 'City')->select()
                    //~ ->load('address.district_id', '/demo/api/china/district');
//~ 
                //~ $filter->equal('address.district_id', 'District')->select();
            //~ });
//~ 
            //~ $grid->tools(function ($tools) {
                //~ $tools->append(new UserGender());
            //~ });

            $grid->actions(function ($actions) {

                if ($actions->getKey() % 2 == 0) {
                    $actions->disableDelete();
                    $actions->append('<a href=""><i class="fa fa-eye"></i></a>');
                } else {
                    $actions->disableEdit();
                    $actions->prepend('<a href=""><i class="fa fa-paper-plane"></i></a>');
                }
            });
        });
    }





    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $content->header('Edit category');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
            $content->header('Create new category');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function tree()
    {
        return Category::tree(function (Tree $tree) {

            $tree->branch(function ($branch) {

                $src = config('admin.upload.host') . '/' . $branch['logo'] ;

                $logo = "<img src='$src' style='max-width:30px;max-height:30px' class='img'/>";

                return "{$branch['id']} - {$branch['title']} $logo";

            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Category::form(function (Form $form) {

            $form->display('id', 'ID');

            $form->select('parent_id')->options(Category::selectOptions());

            $form->text('title')->rules('required');
            $form->textarea('desc')->rules('required');
            $form->image('logo');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}
