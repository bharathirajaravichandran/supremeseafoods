<?php

namespace App\Admin\Controllers;

use App\Models\Dassignment;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\admin_users;
use App\Models\Order;
use App\Models\Delivery_boy;

use Illuminate\Support\Facades\DB;



class DassignmentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Delivery Management');
            $content->description('');
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) 
        {
            $content->header('Delivery Management');
            $content->description('');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Delivery Management');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Delivery_boy::class, function (Grid $grid) {
             $grid->name('Delivery Boy Name');
			
			$grid->disableExport();

   //          $grid->column('user.name',"Delivery boy");
			// $grid->order_id('order_id');

			// $grid->status('status');
	        $grid->column("")->display(function () {
                return '<a class="btn-xs btn-primary" href="'.url('/').'/admin/deliveryview/'.$this->id.'">Details</a>';
            });
                  $grid->filter(function($filter)
                {
					$filter->disableIdFilter();          // Remove the default id filter
					$filter->equal('Delivery_boy_id', 'Delivery_boy_id');           // Add a column filter
					$filter->equal('order_id', 'order_id');
					
				    
				});

            
        });
        
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Dassignment::class, function (Form $form) {
         
							
			$users = admin_users::where('username','!=','admin')->get();
			$user_array = array();
			
			foreach($users as $user)
			{

				$user_array[$user->id] = $user->name;

		}
		            	$form->select('delivery_boy_id','Delivery_boy')->options($user_array);
					  $form->select('order_id','order')->options(Order::all()->pluck('id', 'id'));
            	$form->select('status')->options([ 'In Progress' => 'In Progress']);
            			 $form->disableReset(); 
				});
			
            	
    }
     public function d_view($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $detail_array=array();
           // $ordersView = DB::table('orderdetail')->get();
             
            $ordersView = DB::table('delivery')
                                ->select('delivery.*','product.productname')
                                ->join('product', 'orderdetail.product_id', '=', 'product.id')
                                ->where('orderdetail.order_id',$id)->get();
                foreach($ordersView as $ordersViews){
                    $detail_array[]=$ordersViews;              
                }
                
                //~ echo "<pre>";
                //~ print_r($detail_array);
                //~ exit;
                //~ 

            $content->header('Order Details');
            $content->description('The detailed description of the order details');
            $content->body(view('orderdetailsview',['orderdetails'=>$detail_array]));
        });
    }
}
