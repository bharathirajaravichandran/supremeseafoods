<?php

namespace App\Admin\Controllers;

use App\Models\Transactions;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Extensions\TransactionExport;

class TransactionsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Transactions');
            $content->description('To manage the transactions');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Transactions');
            $content->description('To manage the transactions');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

             $content->header('Transactions');
            $content->description('To manage the transactions');


            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Transactions::class, function (Grid $grid) {
			
			$grid->disableCreateButton();
			$grid->disableActions();
			
			$grid->disableRowSelector();
			 
			$grid->order_id('Order ID')->sortable();
			//$grid->column('username','User Name');
			$grid->column('order.order_type','Order Type');
			$grid->column('order.order_cost','Order Cost(in Rs)');
			
			$grid->tranx_type('Type');
			$grid->tranx_details('Details');
			
			$grid->column('tranx_status')->display(function () {
				
				if($this->tranx_status==1)
					return "Success"; 
				elseif($this->tranx_status==2)
					return "Failure";
				else
					return "";	
			});
						  
			
			$grid->filter(function($filter){

				$filter->disableIdFilter();
				$filter->equal('id', 'Order ID');
				$filter->equal('tranx_status','Status')->select(['1' => 'Success','2'=>'Failure']);

			});
						$grid->exporter(new TransactionExport());

            //$grid->created_at();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Transactions::class, function (Form $form) {

            $tranx_status = [ 1 => 'Success', 2=> 'Failure'];
            $form->select('tranx_status','Status')->rules('required')->options($tranx_status);

        });
    }
}
