<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use DB;
use App\User;
use App\Models\Order;
use App\Models\Product;

class HomeController extends Controller
{
    public function index()
    {  
        return Admin::content(function (Content $content) {

            $content->header('Dashboard');
            $content->description('The Dashboard of supreme sea foods');
            
            $users_count = User::all()->count();            
            $active_users_count   = User::where('user_status', 1)->count();
            $inactive_users_count = User::where('user_status', 0)->count();
            $orders_count = Order::all()->count();
            $products_count = Product::all()->count();
            $delivery_boys_count = DB::table('admin_users')->count();
            
            /*
            $sales_productwise_monthly = DB::select("SELECT sum(quantity) as qty, MONTH(orderdetail.createddatetime) as month,product.productname FROM `orderdetail` inner join product on orderdetail.product_id = product.id GROUP BY MONTH(orderdetail.createddatetime),product_id ORDER BY sum(quantity) desc");
            $chart_data = array();
            foreach($sales_productwise_monthly as $sale_monthly)
				$chart_data[$sale_monthly->productname][$sale_monthly->month] = $sale_monthly->qty;
			            
            //zero fill for other months
            foreach($chart_data as $k=>$v)
            {
				for($i=1;$i<=12;$i++)
				{
					if(empty($chart_data[$k][$i]))
						$chart_data[$k][$i] = 0;	
				}
				ksort($chart_data[$k]);
			}
			$line = array();
			$counter = 0;
			foreach($chart_data as $k=>$v)
			{
				$line[$counter]['label'] = $k;
				$month_values = implode(",",$v);
				$line[$counter]['data'] = [$month_values];
				if($counter==0)
				{
					$line[$counter]['backgroundColor']	= "rgb(255,99,71)";
					$line[$counter]['borderColor']	= "rgb(128,0,0)";
				}
				elseif($counter==1)
				{
					$line[$counter]['backgroundColor']	= "rgb(0,100,0)";
					$line[$counter]['borderColor']	= "rgb(50,205,50)";
				}
				elseif($counter==2)
				{
					$line[$counter]['backgroundColor']	= "rgb(100,149,237)";
					$line[$counter]['borderColor']	= "rgb(135,206,250)";
				}
				else
					break;
				$line[$counter]['borderWidth']	= 2;
				$counter++;
			}
			$data = json_encode($line);
			$data = str_replace('"data":["','"data":[',$data);
			$data = str_replace('"],"backgroundColor','],"backgroundColor',$data);
			*/						           
            $content->body(view('dashboard',[
				'users_count' => $users_count,
				'active_users_count' => $active_users_count,
				'inactive_users_count' => $inactive_users_count,
				'orders_count' => $orders_count,
				'products_count' => $products_count,
				'delivery_boys_count' => $delivery_boys_count,
				//'data' => $data
				])         
            );

            //$content->row("<b>Under Construction</b>");

           // $content->row(function (Row $row) {

                //~ $row->column(4, function (Column $column) {
                    //~ $column->append(Dashboard::environment());
                //~ });
//~ 
                //~ $row->column(4, function (Column $column) {
                    //~ $column->append(Dashboard::extensions());
                //~ });
//~ 
                //~ $row->column(4, function (Column $column) {
                    //~ $column->append(Dashboard::dependencies());
                //~ });
            //});
        });
    }
}
