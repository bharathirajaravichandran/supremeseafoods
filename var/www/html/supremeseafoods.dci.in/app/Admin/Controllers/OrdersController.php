<?php

namespace App\Admin\Controllers;
use App\Models\Order;
use DB;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Extensions\OrderExport;


class OrdersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Orders List ');
            $content->description("List of orders Placed");
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Orders List');
            $content->description('Edit Orders');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Orders List');
            $content->description(' Create Order');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {
         //   $grid->disableCreateButton();
            //$id = '';
           // $grid->actions(function ($actions) {
            //$actions->append('<a href="orderdetails?id='.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
               // $actions->append(' <a class="btn-xs btn-primary" href="'.url('/').'/admin/orderdetailsview/'.$actions->getKey().'">Details</a> ');
              //  $id = $actions->getKey();
           // });
            $grid->id('Order ID')->sortable();

			// $grid->actions(function ($actions) {
			// 	$actions->disableEdit();
			// });
            $grid->order_type('Order Type');
			$grid->column('user.user_first_name','User Name');

            // $grid->model()->with('orderdetail')->orderBy('id', 'DESC');

            // $grid->column('Order Details')->expand(function () {
            //     $details = array_only($this->orderdetail, ['id','order_id','product_id','quantity','product_cost']);
            //         return new Table([], $details);
            // }, 'Order Details');


            $grid->order_cost('Order Cost(in Rs)')->sortable();
            $grid->address('Address');
            $grid->city('City')->sortable();
           
            $grid->phone('Contact Number');
            //$grid->status('Delivery Status');
            $grid->delivery_datetime('Delivery Scheduled')->sortable();
            $grid->created_at('Order Creation Date');

    

            $grid->column('status')->display(function () {
                return ($this->status==1)?'<span class="btn btn-success btn-xs">Completed</span>':'<span class="btn btn-danger btn-xs">Processing</span>';
            });
            
            //$grid->status()->using(['1' => 'Cancelled', '0' => 'Processing']);
                        
            $grid->column("")->display(function () {
                return '<a class="btn-xs btn-primary" href="'.url('/').'/admin/orderdetailsview/'.$this->id.'">Details</a>';
            });
                        
            $grid->filter(function($filter){

                $filter->disableIdFilter();

                $filter->like('order_type', 'Order Type');
                $filter->like('city', 'City');
                $filter->like('state', 'State');
                $filter->like('order_cost', 'Order Cost');
                $filter->equal('status','Status')->select(['0' => 'In Active','1'=>'Active']);
			});  
				$grid->exporter(new OrderExport());
         
        });
    }

        public function orderdetailsview($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $detail_array=array();
           // $ordersView = DB::table('orderdetail')->get();
             
            $ordersView = DB::table('orderdetail')
								->select('orderdetail.*','product.productname')
								->join('product', 'orderdetail.product_id', '=', 'product.id')
								->where('orderdetail.order_id',$id)->get();
                foreach($ordersView as $ordersViews){
                    $detail_array[]=$ordersViews;              
                }
                
                //~ echo "<pre>";
                //~ print_r($detail_array);
                //~ exit;
                //~ 

            $content->header('Order Details');
            $content->description('The detailed description of the order details');
            $content->body(view('orderdetailsview',['orderdetails'=>$detail_array]));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
            return Admin::form(Order::class, function (Form $form) {
                //$form->currency('order_cost', 'Order Cost')->symbol('￥')->rules('required');
   
                $records = DB::table('users')->get();           
                $users=array();
                    foreach ($records as $record) {
                        $users[$record->id]=$record->user_first_name;
                    }
                $form->select('user_id','User Name')->rules('required')->options($users);
                $form->text('address', 'Customer Address')->rules('required');
                $form->text('city', 'Customer City')->rules('required');
                $form->text('state', 'Customer State')->rules('required');
                $form->text('phone', 'Contact Number')->rules('required');
                $form->date('delivery_datetime', 'Change of Delivery Date')->format('YYYY-MM-DD HH:mm:ss')->rules('required');
                $status = [ 1 => 'Active', 0=> 'In Active'];
                $form->select('status','User Status')->rules('required')->options($status);
                	$form->disableReset();
        });
    }
}
