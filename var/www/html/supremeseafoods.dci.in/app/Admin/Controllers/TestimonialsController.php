<?php

namespace App\Admin\Controllers;

use App\Models\Testimonials;
use DB;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TestimonialsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Testimonials');
            $content->description('The testimonials from various customers');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Testimonials');
            $content->description('The testimonials from various customers');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Testimonials');
            $content->description('Create new Testimonial');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Testimonials::class, function (Grid $grid) {

           // $grid->id('ID')->sortable();
            //$grid->user_id('User ID')->sortable();
			$grid->disableExport();
            $grid->column('user.user_first_name','User Name');
            $grid->content('Testimonial Content');
            $grid->no_of_likes('No of Likes')->sortable();


            $grid->column('User status')->display(function () {
                return ($this->status==1)?'Active':'In Active';
            });

            $grid->filter(function($filter){

                $filter->disableIdFilter();

                $filter->like('content', 'Testimonial Content');
                $filter->like('no_of_likes', 'No of Likes');
                $filter->equal('status','User Status')->select(['0' => 'In Active','1'=>'Active']);

            });



        //    $grid->createddatetime();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Testimonials::class, function (Form $form) {

          //  $form->display('id', 'ID');
            
            $form->textarea('content', 'Testimonial Content')->rules('required');      
            $records = DB::table('users')->get();           
            $users=array();
                foreach ($records as $record) {
                    $users[$record->id]=$record->user_first_name;
                }
            $form->select('user_id','User Name')->rules('required')->options($users);
            $status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('status','User Status')->rules('required')->options($status);
            			$form->disableReset();

           // $form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
