<?php

namespace App\Admin\Controllers;

use App\Models\Cuts;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CutsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

           $content->header('Cuts Management');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Cuts Management');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Cuts Management');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Cuts::class, function (Grid $grid) {
            	$grid->disableExport();
            $grid->id('ID');
            $grid->cut_name('Cut_Name');
            $grid->status()->display(function ($status)
				{
                    return $status == 0 ? "active": "Inactive";
                });
           $grid->filter(function($filter)
                {
					$filter->disableIdFilter();          // Remove the default id filter
					$filter->like('Cut_Name', 'Cut_Name');           // Add a column filter
					$filter->equal('status', 'Status')->select([0 => 'inactive', 1 => 'active']);
				  
				});

   
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Cuts::class, function (Form $form) {

             $form->display('id', 'ID');
             $form->text('cut_name');
             $form->select('status')->options([0 => 'active', 1 => 'inactive']);
          $form->disableReset();
        });
    }
}
