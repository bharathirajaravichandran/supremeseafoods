<?php

namespace App\Admin\Controllers;

use App\Models\Site_settings;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class Site_settingsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Site Settings'); 
            /*$content->description('description');*/
            $content->body($this->grid()); 
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Site Settings');
            /*$content->description('description');*/

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Site Settings');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Site_settings::class, function (Grid $grid) {
            /*$grid->id('ID')->sortable();*/
            $grid->site_name('Site Name');
            $grid->phone('Phone');
            $grid->mobile('Mobile');
            $grid->email('Email');
            $grid->copyright('Copyright');
            $grid->address('Address');
            $grid->created_at();
            $grid->updated_at();
            $grid->disableCreateButton();
            $grid->disableExport();   
            $grid->disableRowSelector();   
            $grid->disableFilter();   
            /*$grid->tools(function ($tools)
             {
                            $tools->batch(function ($batch) 
                            {
                               $batch->disableDelete();
                            });
             });*/
            $grid->actions(function ($actions) {
                 $actions->disableDelete();    
                 
            });         
     });

}

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Site_settings::class, function (Form $form){
            $form->display('id', 'ID');
            $form->hidden('created_at', 'Created At');
            $form->hidden('updated_at', 'Updated At');
            $form->text('site_name', 'Site Name');
            $form->mobile('phone', 'Phone');
            $form->mobile('mobile', 'Mobile');
            $form->email('email', 'Email');
            $form->email('alter_email', 'Alter Email');
            $form->text('copyright', 'Copyright');
            $form->textarea('address', 'Address');
            $form->disablereset();
        });
    }
     
}
