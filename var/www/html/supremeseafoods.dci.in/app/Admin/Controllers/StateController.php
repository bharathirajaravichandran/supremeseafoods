<?php

namespace App\Admin\Controllers;

use App\Models\State;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class StateController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('State');
            $content->description('list');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('State');
            $content->description('list');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('State');
            $content->description('list');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(State::class, function (Grid $grid) {
			$grid->disableExport();
            $grid->id('ID');
            $grid->state('state');
            $grid->status()->display(function ($status)
				{
                    return $status == 0 ? "active": "Inactive";
                });
                  $grid->image('State Image')->image();   
                  $grid->pictures()->image();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(State::class, function (Form $form) {

             $form->display('id', 'ID');
             $form->text('state')->rules('required');
             $form->select('status')->options([0 => 'active', 1 => 'inactive']);
             $form->image('image','State Image')->uniqueName()->move('public/state_images')->removable();
              $form->multipleImage('pictures')->move('public/state_images')->removable();
			$form->disableReset();
        });
    }
}
