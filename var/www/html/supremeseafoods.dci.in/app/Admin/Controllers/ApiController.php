<?php

namespace App\Admin\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Models\Users; 
use App\Models\Myfavorites;
use App\Models\Product;
use App\Models\Category;
use App\Models\Productimages;
use App\Models\Cuts;
use App\Models\Useraddress;
use App\Models\Pincodes;
use App\Models\wallet;
use App\Models\Cart;
use App\Models\Order;
use Carbon\Carbon;

class ApiController extends Controller
{
/*
this function used to check the login
 */
	public function test(Request $request) {
		$validator = Validator::make($request->all(), [
            'param1' => 'required',
            //'password' => 'required',
        ]);

        if ($validator->fails()) {
        	$response['message'] = $validator->messages();
        	$response['status'] = 400;
            return response()->json($response,200);
        }
        else
        {
			$data['data'] = $request->param1;
			$data['status'] = 200;
			$data['message'] = 'TEST API is Working';
			return response()->json($data,200);
		}
	}
	
	
	public function login(Request $request) {
		$validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
        	$response['message'] = $validator->messages();
        	$response['status'] = 400;
            return response()->json($response,200);
        }
        
        $password = $request->password;
		$user_name = $request->email;
		$users = Users::where('user_email',$user_name)->orWhere('user_phone_number',$user_name)->get();
				
		if(count($users)){ 
			foreach($users as $user){
				if($user && password_verify($password,$user->password)) {
			     /* $users_data = Users::where('user_email',$user_name)->orWhere('user_phone_number',$user_name);
			      $users_data->toArray();*/
					/*$data['data'] = $this->array_null_values_replace($users_data);*/	
					$user_da = Users::find($user->id);	

                    @$wish_count = Myfavorites::where('user_id',@$user->id)->count();				   
                    @$cart_count = Cart::where('user_id',@$user->id)->count();				   
				    $user_da->wishlist_status   = @$wish_count;
				    $user_da->cart_status       = @$cart_count;								
					$data['data']    = $this->array_null_values_replace($user_da->toArray());						
					$data['status']  = 200;
					$data['message'] = 'User Found Successfully';					
				}elseif(!password_verify($password,$user->password)) {
					$data['status'] = 400;
					$data['message'] = 'Invalid password';
				}else{
					$data['status'] = 400;
					$data['message'] = 'Invalid email/phone';
				}			
				

				return response()->json($data,200);
			}
		}
		else{
			$data['status'] = 400;
			$data['message'] = "user not found";
			return response()->json($data,200);
		}
	}


	public function base64_decode_image($data)
	{
		$ProfileImagedate = md5(date('Y-m-d H:i:s'));
		$ProfileImageData = base64_decode($data);
		$profileImageFilename = $ProfileImagedate . ".png";		
		$images_path = 'public/user_images/'.$profileImageFilename;
		$path = \Storage::disk('public')->put($images_path,$ProfileImageData);
		return $profileImageFilename;
    }

   
	public function register(Request $request) {
		$validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'email' => 'required',
            'password' => 'required',
            
        ]);
         if ($validator->fails()) {
        	$response['message'] = $validator->messages();
        	$response['status'] = 400;
            return response()->json($response,200);
        }

       // images 64 converter
      $img_name = $this->base64_decode_image($request->user_image);
    /*$data = $request->user_image;
    $png_url = "user-".time().".png";
    $path = public_path().'public/uploads/public/user_images/' . $png_url;
    $image_name =  Image::make(file_get_contents($data->base64_image))->save($path);     */
       //images 64 converter
		$user_first_name  = $request->firstname;
		$email  = $request->email;
		$phone  = $request->phone;
		$password = $request->password;
		$usercount = Users::where('user_email',$email)->orWhere('user_phone_number',$phone)->count();
		if($usercount==0)
		{
			$user = new Users();
			$user->user_first_name = $user_first_name;
			$user->user_email  = $email;
			$user->user_status = 1;
			$user->user_phone_number   = $phone;
			$user->user_type           =  'user';
			$user->user_profile_image = ($img_name)? $img_name : '' ;
			$user->password = bcrypt($password);
			$user->save();

            $user_data = Users::find($user->id);
			$data['status'] = 200;
			$data['data'] = $this->array_null_values_replace($user_data->toArray());
			$data['message'] = 'SignUp Successfully';
		}
		else
		{
			$response['message'] = "User email/Phone already exists!";
        	$response['status'] = 400;
            return response()->json($response,200);
		}
		
		
		return response()->json($data,200);
	}
	
	public function pincodes(Request $request) {
		
		$pincodes = DB::table('hub')
							->join('pincodes', 'pincodes.hub_id', '=', 'hub.id')
							->get();
		
		$data['status'] = 200;
		$data['data'] = $this->array_null_values_replace($pincodes->toArray());
		return response()->json($data,200);
	
	}
	
	
	
	//categories start
	public function categories(Request $request)
	 {		
		$Category = Category::all();		
		$data['status'] = 200;
		$data['categories'] = $this->array_null_values_replace($Category->toArray());	
		return response()->json($data,200);
	
	 }
	//categories end
	
	 
	 public function category_product(Request $request)
	 {		
		$Category = DB::table('product')
							->join('productcategories', 'productcategories.product_id', '=', 'product.id')
							->join('productimages', 'productimages.product_id', '=', 'productcategories.product_id')
							->select('product.*','productcategories.*','productimages.*')
							->where('productcategories.cat_id',$request->input('category_id'))
							->groupBy('productcategories.product_id')
							->get();		
		if(@$Category->count() ==0)
		{
	      $data['status'] = 201;
		  $data['message'] = 'No products';
		  return response()->json($data,201);
		}	
			


		if(@$request->category_id && @$request->user_id)
		{
              foreach($Category as $Categ)
			{          
				if($Categ->combo_price == null || $Categ->default_quantity == null || $Categ->remember_token == null )
				{ 
				   @$wish_count = Myfavorites::where('user_id',@$request->user_id)->where('product_id',$Categ->product_id)->count();
				    $Categ->combo_price       = ''; 
				    $Categ->default_quantity  = '';
				    $Categ->remember_token    = '';
				    $Categ->wishlist_status   = @$wish_count;			     
			    }
			        $Categy[] = $Categ;
			}

		}
	else{	// get category related product data
		foreach($Category as $Categ)
		{          
			if($Categ->combo_price == null || $Categ->default_quantity == null || $Categ->remember_token == null )
			    {
			   @$wish_count = Myfavorites::where('user_id',@$request->user_id)->where('product_id',$Categ->product_id);
			    $Categ->combo_price       = ''; 
			    $Categ->default_quantity  = '';
			    $Categ->remember_token    = '';
			    $Categ->wishlist_status   = '0';			     
		        }
		        $Categy[] = $Categ;
		}
	  }
	  
		$data['status']     = 200;
		$data['categories'] = $this->array_null_values_replace($Categy);
		return response()->json($data,200);
	
	 }
	 
	 
	 public function product_detail(Request $request)
	 {		
		$product       = Product::where('id',$request->input('product_id'))->get();
		$Productimages = Productimages::select('product_image')
		                              ->where('product_id',$request->input('product_id'))
		                              ->where('main_image',1)
		                              ->get();
        $Productotherimages = Productimages::select('product_image')
                                      ->where('product_id',$request->input('product_id'))
                                      ->where('main_image','!=',1)
                                      ->get();	   

		if(@$product->count() ==0)
		{
	      $data['status'] = 201;
		  $data['product'] = 'This product id does\'t not exists'; 	
		  return response()->json($data,201);
		}
		
		foreach($product as $prod)
		{
			if($prod->remember_token == null && $prod->default_quantity == null)
			    {			      
			       $prod->default_quantity = '';
			       $prod->remember_token   = '';
			       $prod->combo_price      = '';
		        }		        
		        $product = $prod;
		}		
				 
        $Cut = Cuts::where('status',1)->get();
        $data['status'] = 200;
		$data['product'] = $this->array_null_values_replace($product->toArray());
		$data['product_image']        =  ($Productimages) ? $Productimages : '' ;
		$data['other_product_images'] =  ($Productotherimages) ? $Productotherimages : '' ;
		$data['type_of_cuts'] =  ($Cut) ? $this->array_null_values_replace($Cut->toArray()) : '' ;
		/*$data['quantity'] = array();
		$data['cut'] = array();
		dd($data);	*/			
		return response()->json($data,200);
	
	 }

	 //wishlist
	 public function wishlist(Request $request)
	 {		

        if(@$request->user_id !='' && @$request->product_id == '')
        {   

        $user = Users::find($request->user_id);
        if(count($user) !=0 )
        	{ 
        	    $wishlists = DB::table('myfavorites')
							->join('product', 'product.id', '=', 'myfavorites.product_id')		
							->select('product.*','myfavorites.*')
							->where('myfavorites.user_id',$request->user_id)							
							->get();						

				if(count($wishlists) != 0)
				{                        
                         $new_array = array();
                         $wishl = array();                         
					     foreach($wishlists as $wish)
					              {  	

					              	 @$wish_count = Myfavorites::where('user_id',$wish->user_id)->where('product_id',$wish->product_id);
					                 @$wish->wishlist_status = @$wish_count->count();

					                 $Productimages = Productimages::select('product_image')
		                              ->where('product_id',@$wish->product_id)
		                              ->where('main_image',1)
		                              ->first();		                             

					            @$wish->product_image = (@$Productimages) ? @$Productimages->product_image : '';                 
                                @$wishl[] = $wish;
					              }
			            $Productimages = Productimages::select('product_image')
		                              ->where('product_id',@$wishlist[0]->product_id)
		                              ->where('main_image',1)
		                              ->first();	
		                $wishlist = $this->array_null_values_replace($this->object_to_array($wishl));            	                              
						$data['status']               =  200;
						$data['wishlist']             =  $wishlist ; 
						$data['product_image']        =  ($Productimages) ? $Productimages : '' ;
				        return response()->json($data,200);

				        
		       }
		   else{
                        $data['status'] = 201;
						$data['wishlist_message'] = 'you don\'t have wishlist';						
				        return response()->json($data,200);
		       }

           }
           {
             $data['status']  = 201;
		     $data['message'] = "User does't exists";
		     return response()->json($data,201);
           }

             //wishlist view             
        }
   elseif(@$request->user_id !='' && @$request->product_id !='') // wishlist added start 
        {
             $Myfavorites = Myfavorites::where('user_id',$request->user_id)->where('product_id',$request->product_id);
            
             if(@$Myfavorites->count() !=0)
                 {  
                 	//delete
                 	$Myfavorites->forcedelete();
                 	$data['status']            = 201;
					$data['wishlist_responce'] = 'Removed Successfully';
					$Myfavorites_count = Myfavorites::where('user_id',$request->user_id)->count();
					$data['wishlist_count']    = $Myfavorites_count ;							
				    return response()->json($data,201);

                 }else
                 {  
                 	            
                 	$Myfavorit = new Myfavorites();
                 	$Myfavorit->user_id    = (@$request->user_id) ? @$request->user_id : '' ;
                 	$Myfavorit->product_id = (@$request->product_id) ? @$request->product_id : '' ;
                 	$Myfavorit->status     = 1;                 	
                 	$Myfavorit->save();
                 	$Myfavorites_count = Myfavorites::where('user_id',$request->user_id)->count();
                 	//insert
                 	$data['status']            = 200;
					$data['wishlist_responce'] = 'Added Successfully';						
					$data['wishlist_count']    = $Myfavorites_count ;						
				    return response()->json($data,200);                 	

                 }       
        } // wishlist added end 
    else{
                    $data['status']            = 201;
					$data['message'] = 'Error';						
				    return response()->json($data,201);       
        } 

	 	/*dd($request->user_id);
		$Category = Category::all();		
		$data['status'] = 200;
		$data['categories'] = $Category;		
		return response()->json($data,200);*/
	
	 }
	 //wishlist

function object_to_array($object) {
    return (array) $object;
}

	 //user start
	public function user_detail(Request $request)
	 {		
	 	if(@$request->user_id !='')
	 	{  
	 	   @$users = Users::find(@$request->user_id);
	 	   if(count(@$users) !=0 )
	 	    {
	           $data['status']    = 200;
	           $data['user_data'] = $this->array_null_values_replace($users->toArray());
	           return response()->json($data,200);
           	}
           else
           {
             	$data['status'] = 201;	
             	$data['message'] = "User does't exists";			
		        return response()->json($data,201);
           }		
	 	}
				
		$data['status'] = 201;			
		return response()->json($data,201);
	
	 }
	//user end


	//product search start
	public function productsearch(Request $request)
	 {		
	 	if(@$request->keyword !='')    
	 	{  
	 	   @$productname = Product::where('productname','like','%' .@$request->keyword .'%')->get();

	 	   if(count(@$productname) !=0 )
	 	    {
                
                    $Category = DB::table('product')
							->join('productcategories', 'productcategories.product_id', '=', 'product.id')
							->join('productimages', 'productimages.product_id', '=', 'productcategories.product_id')
							->select('product.*','productcategories.*','productimages.*')
							->where('productname','like','%' .@$request->keyword .'%')
							->groupBy('productcategories.product_id')
							->get();		
					if(@$Category->count() ==0)
					{
				      $data['status'] = 201;
				      $data['message'] = "No products";		 
					  return response()->json($data,201);
					}				


		
			foreach($Category as $Categ)
			{          
				if($Categ->combo_price == null || $Categ->default_quantity == null || $Categ->remember_token == null )
				    {
				   @$wish_count = Myfavorites::where('user_id',@$request->user_id)->where('product_id',$Categ->product_id)->count();				   
				    $Categ->combo_price       = ''; 
				    $Categ->default_quantity  = '';
				    $Categ->remember_token    = '';
				    $Categ->wishlist_status   = $wish_count;			     
			        }
			        $Categy[] = $Categ;
			}	   
	    
		$data['status']     = 200;
		$data['product_details'] = $this->array_null_values_replace($Categy);
		return response()->json($data,200);
           	}
           else
           {
             	$data['status'] = 201;	
             	$data['message'] = "No products";			
		        return response()->json($data,201);
           }		
	 	}				
		$data['status'] = 201;		
		$data['message'] = "No keywords";		
		return response()->json($data,201);	
	 }
	//product search  end





	  //singale delivery address view start
	public function addressview(Request $request)
	 {		
	 	if(@$request->user_id !='')
	 	{   
           
	 	   @$users = Useraddress::where('user_id',@$request->user_id)->get();
	 	   if(count(@$users) !=0 )
	 	    {

	 	    	        @$user_last_address = Useraddress::where('user_id',@$request->user_id)     
                                            ->where('default_address',1)
                                            ->get();              
                            if(count($user_last_address) !=0)
                                {
                                      @$user_last_address = Useraddress::where('user_id',@$request->user_id)     
							                                          ->where('default_address',1)
							                                          ->first();
            
                                }
                           else{
                                     @$user_last_address = Useraddress::where('user_id',@$request->user_id)
                                                ->orderBy('id','DESC')
                                                ->limit(1)->first(); 	
                               }        
	           $data['status']    = 200;
	           $data['delivery_address'] = $this->array_null_values_replace($user_last_address->toArray());	
	           $data['user_data'] = $this->array_null_values_replace($users->toArray());	          
	           return response()->json($data,200);
          	
           }
       else{
           	$data['status'] = 201;	
            $data['message'] = "No address";		
		    return response()->json($data,201);
           }	
	 	
	  }
				
		$data['status'] = 201;		
		$data['message'] = "User id required";	
		return response()->json($data,201);
	
	 }
	//single delivery address view end



	  //user delivery address view start
	public function singleaddressview(Request $request)
	 {		
	 	if(@$request->delivery_address_id !='')
	 	{   
           
	 	   @$users = Useraddress::where('id',$request->delivery_address_id)->get();
	 	   if(count(@$users) !=0 )
	 	    {

	           $data['status']    = 200;
	           $data['user_data'] = $this->array_null_values_replace($users->toArray());	
	           /*$data['user_data'] = $this->array_null_values_replace($users->toArray());	*/          
	           return response()->json($data,200);
          	
           }
       else{
           	$data['status'] = 201;	
            $data['message'] = "No address";		
		    return response()->json($data,201);
           }	
	 	
	  }
				
		$data['status'] = 201;		
		$data['message'] = "id required";	
		return response()->json($data,201);
	
	 }
	//user delivery address view end



	  //user delivery address start
	public function addressedit(Request $request)
	 {		

          if($request->type=='add' && @$request->user_id !='')
          {
                      @$users = new Useraddress();
                      $users->address       = (@$request->address) ? $request->address : '';
                      $users->user_id       = (@$request->user_id) ? $request->user_id : '';
                      $users->name          = (@$request->name) ? $request->name : '';
                      $users->email         = (@$request->email) ? $request->email : '';
                      $users->city          = (@$request->city) ? $request->city : '';
                      $users->state    		= (@$request->state) ? $request->state : '';
                      $users->country  		= (@$request->country) ? $request->country : '';
                      $users->pincode  		= (@$request->pincode) ? $request->pincode : '';
                      $users->phone    		= (@$request->phone) ? $request->phone : '';
                      $users->status   		= 1;
                      $users->default_address   		= 0;
                      $users->save();
                      @$user = Useraddress::where('user_id',@$request->user_id)->get();
			          $data['status']     = 200;
			          $data['message']    = 'Delivery address added successfully';
			          $data['user_data']  = $this->array_null_values_replace($user->toArray());
			          return response()->json($data,200);

          }elseif($request->type=='edit' && @$request->delivery_address_id !='')
          {
                      @$users = Useraddress::find(@$request->delivery_address_id);
                      $users->address       = (@$request->address) ? $request->address : '';
                      $users->user_id       = (@$request->user_id) ? $request->user_id : '';
                      $users->name          = (@$request->name) ? $request->name : '';
                      $users->email         = (@$request->email) ? $request->email : '';
                      $users->city          = (@$request->city) ? $request->city : '';
                      $users->state    		= (@$request->state) ? $request->state : '';
                      $users->country  		= (@$request->country) ? $request->country : '';
                      $users->pincode  		= (@$request->pincode) ? $request->pincode : '';
                      $users->phone    		= (@$request->phone) ? $request->phone : '';
                      $users->status   		= 1;
                      $users->default_address   		= 0;
                      $users->save();
                      @$user = Useraddress::where('id',@$request->delivery_address_id)->get();
			          $data['status']     = 200;
			          $data['message']    = 'Delivery address updated successfully';
			          $data['user_data']  = $this->array_null_values_replace($user->toArray());
			          return response()->json($data,200);
          }
        else
          {
          	if(@$request->delivery_address_id !='')
          	    {
	             $user_address = useraddress::find(@$request->delivery_address_id);	            
	             if(count($user_address) !=0)
	             {
                        $user_address->delete();
                        $data['message']    = 'Delivery address deleted successfully';
	                    $data['status'] = 200;	
	                    return response()->json($data,200);
	             }	
	                    
	                    $data['status'] = 201;
	                    $data['message'] = "No address";	
	                    return response()->json($data,201);	             
                }
            else{
                  $data['status'] = 201;	
                  $data['message'] = "Delivery id required";
                }
		     return response()->json($data,201); 
          }
				
		$data['status'] = 201;		
		$data['message'] = "Some field required";	
		return response()->json($data,201);
	
	 }
	//user delivery address end


	  //user delivery address view start
	public function change_delivery_address(Request $request)
	 {		
	 	if(@$request->delivery_address_id !='')
	 	{   
           
	 	   @$users = Useraddress::find(@$request->delivery_address_id);
	 	   if(count(@$users) !=0 )
	 	    { 	 	
	 	       DB::table('useraddress')->where('user_id',@$users->user_id)->where('default_address',1)->update(['default_address' =>0]); 
	 	                $users->default_address = 1 ;
	 	                $users->save();

               @$user_address = Useraddress::where('user_id',@$users->user_id)
                                            ->where('id','!=',$request->delivery_address_id)
                                            ->where('default_address',1)
                                            ->get();                          
	           $data['status']    = 200;
	           $data['delivery_address'] = $this->array_null_values_replace($users->toArray());	
	           $data['user_data'] = $this->array_null_values_replace($user_address->toArray());	          
	           return response()->json($data,200);
          	
           }
       else{
           	$data['status'] = 201;	
            $data['message'] = "No address";		
		    return response()->json($data,201);
           }	
	 	
	   }
				
		$data['status'] = 201;		
		$data['message'] = " id required";	
		return response()->json($data,201);
	
	 }
	//user delivery address view end


		

   public function array_null_values_replace($result)
	{
          if(is_array($result))
          { 
            array_walk_recursive($result, function (&$item, $key)
             {  
               $item = null === $item ? '' : $item;
             });
            return $result;
          }                 
          else
           {  
                $r['Status'] = 'Failed';
                $r['message'] = "Failed";
                return response()->json($r);
           }
    }


     //cuts start
	public function all_cuts(Request $request)
	 {	   
	 	   $Cut = Cuts::where('status',1)->get();		 	   	 	   
	 	   if(count(@$Cut) !=0 )
	 	    {
	           $data['status']          = 200;
	           $data['Cuts']            = $this->array_null_values_replace($Cut->toArray());
	           return response()->json($data,200);
           	}
           else
            {
             	$data['status']  = 201;	
             	$data['message'] = "No Cuts";		
		        return response()->json($data,201);
            }					
		$data['status'] = 201;
		$data['message'] = "Field required";			
		return response()->json($data,201); 
	
	 }
	 //cuts end


	 //change location start
	public function change_location(Request $request)
	 {		
	 	if(@$request->location_id !='')    
	 	{  
	 	   @$location = Pincodes::find(@$request->location_id);

	 	   if(count(@$location) !=0 )
	 	    {
	           $data['status']          = 200;
	           $data['location']        = $this->array_null_values_replace($location->toArray());	
	           return response()->json($data,200);
           	}
           else
           {
             	$data['status'] = 201;	
             	$data['message'] = "No pincodes";		
		        return response()->json($data,201);
           }		
	 	}
				
		$data['status']  = 201;
		$data['message'] = "Field required";			
		return response()->json($data,201);
	
	 }
	//change location  end


//update location start
	public function update_location(Request $request)
	 {		
	 	if(@$request->location_id !='' && @$request->user_id !='')    
	 	{  
	 	   $location = Pincodes::find(@$request->location_id);

	 	   if(count(@$location) !=0 )
	 	    {  
                  @$Users = Users::find(@$request->user_id);
                  if(@$Users)
                  {    
                         @$Users->user_location = $location->pincode ;
                         @$Users->save();
                  }

	           $data['status']          = 200;
	           $data['location']        = $this->array_null_values_replace($location->toArray());	
	           return response()->json($data,200);
           	}
           else
           {
             	$data['status'] = 201;	
             	$data['message'] = "No pincodes";		
		        return response()->json($data,201);
           }		
	 	}
				
		$data['status']  = 201;
		$data['message'] = "Field required";			
		return response()->json($data,201);
	
	 }
//update location  end



    //Product filter start
	public function product_filter(Request $request)
	 {	
	    $Category = DB::table('product')
	 						->join('productcategories', 'productcategories.product_id', '=', 'product.id')
							->join('productimages', 'productimages.product_id', '=', 'productcategories.product_id')
							->select('product.*','productcategories.*','productimages.*')
							->where('productcategories.cat_id',$request->category_id)
							->where('productcategories.product_id',$request->product_id)
							->groupBy('productcategories.product_id')
							->get();		

		if(@$Category->count() ==0)
		{
	      $data['status'] = 201;
	      $data['message'] = "No Products";		 
		  return response()->json($data,201);
		}
                   $category = array();
		foreach($Category as $Categ)
		{
                    $Categ->combo_price       = ''; 
				    $Categ->default_quantity  = '';
				    $Categ->remember_token    = '';
				    $category[] = $Categ ;
		}	         
		       
		       $data['status']              = 200;
	           $data['product_data']        = $this->array_null_values_replace($category);	
	           return response()->json($data,200); 

	 }
	//Product filter end


   //forgot password start
	public function forgot_password(Request $request) 
	{
		$validator = Validator::make($request->all(), [
            'data' => 'required',            
        ]);
        if ($validator->fails()) {
        	$response['message'] = $validator->messages();
        	$response['status'] = 400;
            return response()->json($response,200);
        }        
        $user_data = $request->data;		
		$users = Users::where('user_email',$user_data)->orWhere('user_phone_number',$user_data)->get();		
		if(count($users))
		{
						$user = Users::find(@$users[0]->id);				
					$data['data']    = $this->array_null_values_replace($user->toArray());
					$data['status']  = 200;
					$data['message'] = 'User Found Successfully';
					return response()->json($data,200);
			
		}
    else{
			$data['status'] = 400;
			$data['message'] = "user not found";
			return response()->json($data,200);
		}
	}
  //forgot password end


	//wallet  start
	public function wallet(Request $request) 
	{
		$validator = Validator::make($request->all(), [
            'user_id' => 'required',            
        ]);
        if ($validator->fails()) {
        	$response['message'] = $validator->messages();
        	$response['status'] = 400;
            return response()->json($response,200);
        }    

        if($request->user_id !='' && $request->amount !='')
        {
                //insert  and update
	                $wallet = wallet::where('user_id',$request->user_id)->get();
                if(count($wallet) !=0)
                  {
	                $wallet->user_id     = $request->user_id;
	                $wallet->amount	     = $request->amount;
	                $wallet->description = $request->description;
	                $wallet->credited_by = $request->credited_by;
	                $wallet->debited_by  = $request->debited_by;
	                $wallet->status      = 1 ;
	                $wallet->save();

                    $wallet_data = wallet::where('user_id',$request->user_id)->get();
                    $data['wallet_data']        = $this->array_null_values_replace($wallet_data->toArray());
					$data['status']             = 200;					
					return response()->json($data,200);
				  }
		     else{  
		     	    $wallet = new wallet();
                    $wallet->user_id     = $request->user_id;
	                $wallet->amount	     = $request->amount;
	                $wallet->description = $request->description;
	                $wallet->credited_by = $request->credited_by;
	                $wallet->debited_by  = $request->debited_by;
	                $wallet->status      = 1 ;
	                $wallet->save();
                    $wallet_data = wallet::where('user_id',$wallet->user_id)->get();
                    $data['wallet_data']        = $this->array_null_values_replace($wallet_data->toArray());
					$data['status']             = 200;					
					return response()->json($data,200);
				 }
        }
       elseif($request->user_id !='' && $request->amount =='')
        {
                    $wallet_data = wallet::where('user_id',$request->user_id)->get();
               if(count($wallet_data) !=0)
                {
                    $data['wallet_data']        = $this->array_null_values_replace($wallet_data->toArray());
					$data['status']             = 200;					
					return response()->json($data,200);
				}else{  $data['status']             = 201;					
					return response()->json($data,201); }
        }else
        {        	       
					$data['status']             = 201;					
					return response()->json($data,201);
        }    

	}
  //wallet end


	//user password update start
	public function user_password_update(Request $request) 
	{
		$validator = Validator::make($request->all(), [
            'user_id'          => 'required',            
            'password'         => 'required',            
            'confirm_password' => 'required',            
        ]);
        if ($validator->fails()) 
        {
        	$response['message'] = $validator->messages();
        	$response['status'] = 201;
            return response()->json($response,201);
        }          	
		$users = Users::find($request->user_id);			
		if(count($users))
		{
					$users->password = bcrypt($request->confirm_password);
					$users->old_pwd  = $users->old_pwd;
					$users->password_confirmation  = bcrypt($request->confirm_password);
					$users->save() ;
                    $user = Users::find($request->user_id);
					$data['data']    = $this->array_null_values_replace($user->toArray());
					$data['status']  = 200;
					$data['message'] = 'Password Successfully Updated';
					return response()->json($data,200);
			
		}
    else{
			$data['status'] = 201;
			$data['message'] = "user not found";
			return response()->json($data,201);
		}
	}
   //user password update end

public function order_details(Request $request) 
{
     if($request->order_id !='')
     {
     	     $order_details = DB::table('order')
							->join('orderdetail', 'orderdetail.order_id', '=', 'order.id')						
							->select('order.*','orderdetail.*')
							->where('order.user_id',$request->order_id)
							/*->groupBy('productcategories.product_id')*/
							->get();
                     if(count($order_details) ==0 )
                        { 
                            $data['status']  = 201;							
							return response()->json($data,201);
						}				
					$data['order_details']    = $order_details ;
					$data['status']           = 200;
					$data['message']          = 'Order details';
					return response()->json($data,200);		
	  }else
	  {
            $data['status']  = 201;
			$data['message'] = "Order id not found";
			return response()->json($data,201);
	  }

}


public function order_histroy(Request $request) 
{
     if($request->user_id !='')
     {
     	     $order_histroy = DB::table('order')
							->join('orderdetail', 'orderdetail.order_id', '=', 'order.id')						
							->select('order.*','orderdetail.*')
							->where('order.user_id',$request->user_id)
							/*->groupBy('productcategories.product_id')*/
							->get();
                     if(count($order_histroy) ==0 )
                        { 
                            $data['status']  = 201;							
							return response()->json($data,201) ;
						}				
					$data['order_histroy']    = $order_histroy ;
					$data['status']           = 200;
					$data['message']          = 'Order histroy';
					return response()->json($data,200);		
	  }else
	  {
            $data['status']  = 201;
			$data['message'] = "Order id not found";
			return response()->json($data,201);
	  }

}


public function social_login_user_check(Request $request) 
{
	    if($request->social_id)
	    {
     	     $user_data = Users::where('user_fb_id',$request->social_id)
			     	     ->orwhere('user_gplus_id',$request->social_id)
			     	     ->orwhere('user_twitter_id',$request->social_id)
			     	     ->first();
			    if(count($user_data) !=0)
			    {                    
                    $user = Users::find($user_data->id);
					$data['data']    = $this->array_null_values_replace($user->toArray());
					$data['status']           =  200;
					$data['message']          = 'User details';
					return response()->json($data,200);	
			    }else
			    {
                    $data['status']  = 201;
					$data['message'] = "New user";
					return response()->json($data,201);
			    }
	   
        }
	 else
	   {
            $data['status']  = 201;
			$data['message'] = "id not found";
			return response()->json($data,201);
	   }

}


public function social_login_user_save(Request $request) 
{
	   
        if($request->photo !='')
           {   

               /* $ProfileImagedate = md5(date('Y-m-d H:i:s'));
		        $profileImageFilename = $ProfileImagedate . ".png";
				$images_path = 'public/user_images/'.$profileImageFilename;
				$path   = \Storage::disk('public')->put($images_path,$request->photo);
				$photos = $profileImageFilename;*/
				$ProfileImageData = $request->photo ;
				$ProfileImagedate = md5(date('Y-m-d H:i:s'));		
				$profileImageFilename = $ProfileImagedate . ".png";			
				$images_path = $_SERVER['DOCUMENT_ROOT'].'/uploads/public/user_images/'.$profileImageFilename;       
				copy($ProfileImageData, $images_path);
		   

	       }

	    if($request->type=="gm")
	    { 

	 $user = Users::where('user_gplus_id',$request->social_id)->orwhere('user_phone_number',$request->phone)->count();
	    	if($user == 0)
	    	{
     	     $user_data = new Users();
     	     $user_data->user_first_name     = ($request->name)  ? $request->name  : '' ;
     	     $user_data->user_email          = ($request->email) ? $request->email : '' ;
     	     $user_data->user_phone_number   = ($request->phone) ? $request->phone : '' ;
     	$user_data->user_profile_image  = ($profileImageFilename) ? 'public/user_images/'.$profileImageFilename : '' ;
     	     $user_data->user_gplus_id       = ($request->social_id) ? $request->social_id : '' ;
     	     $user_data->user_type           =  'user';
     	     $user_data->user_status         = 1 ;
     	     $user_data->save();
     	            $user = Users::find($user_data->id);
					$data['data']             = $this->array_null_values_replace($user->toArray());
					$data['status']           =  200;
					$data['message']          = 'User details';
					return response()->json($data,200);	
			}else
			{
                    $data['status']           =  201;
					$data['message']          = 'Phone & gplus id already exists';
					return response()->json($data,201);
			}
			    
	   
        }
        elseif($request->type=="fb")
        {  
        $user = Users::where('user_fb_id',$request->social_id)->orwhere('user_phone_number',$request->phone)->count();
	    	if($user == 0)
	    	{
        	 $user_data = new Users();
     	     $user_data->user_first_name     = ($request->name)  ? $request->name  : '' ;
     	     $user_data->user_email          = ($request->email) ? $request->email : '' ;
     	     $user_data->user_phone_number   = ($request->phone) ? $request->phone : '' ;
     	 $user_data->user_profile_image  = ($profileImageFilename) ? 'public/user_images/'.$profileImageFilename : '' ;
     	     $user_data->user_fb_id          = ($request->social_id)    ? $request->social_id    : '' ;
     	     $user_data->user_status         = 1 ;
     	     $user_data->user_type           =  'user';
     	     $user_data->save();
     	            $user = Users::find($user_data->id);
					$data['data']             = $this->array_null_values_replace($user->toArray());
					$data['status']           =  200;
					$data['message']          = 'User details';
					return response()->json($data,200);	
					}else
			{
                    $data['status']           =  201;
					$data['message']          = 'Phone & facebook id already exists';
					return response()->json($data,201);
			}

        }
        elseif($request->type=="tw")
        {  
       $user = Users::where('user_twitter_id',$request->social_id)->orwhere('user_phone_number',$request->phone)->count();
	    	if($user == 0)
	    	{
        	 $user_data = new Users();
     	     $user_data->user_first_name     = ($request->name)  ? $request->name  : '' ;
     	     $user_data->user_email          = ($request->email) ? $request->email : '' ;
     	     $user_data->user_phone_number   = ($request->phone) ? $request->phone : '' ;
     	$user_data->user_profile_image  = ($profileImageFilename) ? 'public/user_images/'.$profileImageFilename : '' ;
     	     $user_data->user_twitter_id     = ($request->social_id)    ? $request->social_id    : '' ;
     	     $user_data->user_status         = 1 ;
     	     $user_data->user_type           =  'user';
     	     $user_data->save();
     	            $user = Users::find($user_data->id);
					$data['data']             = $this->array_null_values_replace($user->toArray());
					$data['status']           =  200;
					$data['message']          = 'User details';
					return response()->json($data,200);	
			}
			else
			{
                    $data['status']           =  201;
					$data['message']          = 'Phone & twitter id already exists';
					return response()->json($data,201);
			}
        }
	 else
	   {
            $data['status']  = 201;			
			return response()->json($data,201);
	   }

}

public function place_order(Request $request) 
{
     if($request->order_id !='')
     {
     	     $place_order = DB::table('order')
							->join('orderdetail', 'orderdetail.order_id', '=', 'order.id')						
							->join('payments', 'payments.order_id', '=', 'orderdetail.order_id')					
							->join('paymentmodes', 'paymentmodes.id', '=', 'payments.payment_mode')					
							->select('order.*','orderdetail.*','payments.*','paymentmodes.*')
							->where('order.id',$request->order_id)
							/*->groupBy('productcategories.product_id')*/
							->get();
                     if(count($place_order) ==0 )
                        { 
                            $data['status']  = 201;							
							return response()->json($data,201) ;
						}				
					$data['place_order']      = $place_order ;
					$data['status']           = 200;
					$data['message']          = 'Place order histroy';
					return response()->json($data,200);		
	  }else
	  {
            $data['status']  = 201;
			$data['message'] = "Order id not found";
			return response()->json($data,201);
	  }

}

public function recipes_list(Request $request) 
{
                 if($request->recipe_id !='')
                  {
     	            $recipes_list =  DB::table('recipe')
							->join('recipeimage', 'recipeimage.recipe_id', '=', 'recipe.id')						
							->select('recipe.*','recipeimage.*')
							->where('recipe.id',$request->recipe_id)					
							->groupBy('recipe.id')
							->get();   	           
                     if(count($recipes_list) ==0 )
                        { 
                            $data['status']  = 201;							
							return response()->json($data,201) ;
						}				
					$data['recipes_list']      = $recipes_list ;
					$data['status']            = 200;
					$data['message']           = 'Recipes Details';
					return response()->json($data,200);	
				 }else
					{
						$data['status']  = 201;						
						return response()->json($data,201);
					}	
	 

}

public function recipes_detail(Request $request) 
{
    
     	            $recipes_list =  DB::table('recipe')
							->join('recipeimage', 'recipeimage.recipe_id', '=', 'recipe.id')						
							->select('recipe.*','recipeimage.*')					
							->groupBy('recipe.id')
							->get();   	           
                     if(count($recipes_list) ==0 )
                        { 
                            $data['status']  = 201;							
							return response()->json($data,201) ;
						}				
					$data['recipes_detail']      = $recipes_list ;
					$data['status']            = 200;
					$data['message']           = 'Recipes Detail';
					return response()->json($data,200);		
	 

}


 public function today_product(Request $request)
	 {		
		$Category = DB::table('product')
							->join('productcategories', 'productcategories.product_id', '=', 'product.id')
							->join('productimages', 'productimages.product_id', '=', 'productcategories.product_id')
					->join('whatsavailable', 'whatsavailable.product_id', '=', 'productimages.product_id')
							->select('product.*','productcategories.*','productimages.*','whatsavailable.*')
							->where('productcategories.cat_id',$request->category_id)							
							->where('whatsavailable.createddatetime',Carbon::today()->toDateString())				
							->groupBy('productcategories.product_id')
							->get();				
		if(@$Category->count() ==0)
		{
	      $data['status'] = 201;		 
		  return response()->json($data,201);
		}
	else{	// get category related product data

		 $wish_count = 0;
		foreach($Category as $Categ)
		{          
			if($Categ->combo_price == null || $Categ->default_quantity == null || $Categ->remember_token == null )
			    {
			   @$wish_count = Myfavorites::where('user_id',@$request->user_id)->where('product_id',$Categ->product_id)->count();
			    $Categ->combo_price       = ''; 
			    $Categ->default_quantity  = '';
			    $Categ->remember_token    = '';
			    $Categ->created_at        = '';
			    $Categ->updated_at        = '';
			    $Categ->wishlist_status   = $wish_count ;			     
		        }
		        $Categy[] = $Categ;
		}
	
	  
		$data['status']     = 200;
		$data['categories'] = $this->array_null_values_replace($Categy);
		return response()->json($data,200);
	
	  }
	}

   
 public function addcart(Request $request)
    {  
    	
    	if($request->method == 'add')
    	{
             $cart = Cart::where('product_id',$request->product_id)
                     ->where('user_id',$request->user_id)
                     ->where('type',$request->type)
                     ->where('cuts_type',$request->cuts_type)
                     ->get();
        if($cart->count() !=0)
         {
                   //update
         	       $cart_data = Cart::where('product_id',$request->product_id)
                     ->where('user_id',$request->user_id)
                     ->where('type',$request->type)
                     ->where('cuts_type',$request->cuts_type)
                     ->first();

                   $produc_quantity = $cart_data->quantity + $request->quantity;
                   $produc_price    = $produc_quantity * $request->total_price ; 
                   $cart_data->product_id     =  $request->product_id ;
                   $cart_data->quantity       =  $produc_quantity ;
                   $cart_data->type           =  $request->type ;
                   $cart_data->cuts_type      =  $request->cuts_type ;
                   $cart_data->total_price    =  $produc_price ;
                   $cart_data->user_id        =	 $request->user_id ;
                   $cart_data->save();
 
                   $cart_counts = Cart::where('user_id',$request->user_id)->get(); 
                   $cart_datas = Cart::where('id',$cart_data->id)->get();
                   $cart_d = array();
                        foreach($cart_datas as $cart)
                        {    
                        	 $cart->cart_count = count($cart_counts) ;
                             $cart_d[] = $cart ;
                        }                  
                   $data['status']      = 200;
                   $data['message']     = 'Your cart updated successfully';
				   $data['cart_data'] = $this->array_null_values_replace($cart_d);
				   return response()->json($data,200);
         }
        else
         {         $produc_price    = $request->quantity * $request->total_price;
                   
                   $cart_data = new Cart();
                   $cart_data->product_id     =  $request->product_id ;
                   $cart_data->quantity       =  $request->quantity ;
                   $cart_data->type           =  $request->type ;
                   $cart_data->cuts_type      =  $request->cuts_type ;
                   $cart_data->total_price    =  $produc_price ;
                   $cart_data->user_id        =	 $request->user_id ;
                   $cart_data->save();

                   $cart_count = Cart::where('user_id',$request->user_id)->get();
                   $cart_datas = Cart::where('id',$cart_data->id)->get();
                   $cart_d = array();
                        foreach($cart_datas as $cart_da)
                        {    $cart_da->cart_count = count($cart_count) ;
                             $cart_d[] = $cart_da ;
                        }


                   $data['status']      = 200;
                   $data['message']     = 'Your cart inserted successfully';
				   $data['cart_data'] = $this->array_null_values_replace($cart_d);
				   return response()->json($data,200);
         }
     }else
     {
          //delete action ;
     	    $cart_data = Cart::find($request->cart_id);
     	    if(count($cart_data) !=0)
     	{
            Cart::find($request->cart_id)->delete();
            $cart_datas = Cart::where('user_id',$request->user_id)->get(); 
            $data['status']      = 200;
            $data['message']     = 'Your cart deleted successfully';
            $data['cart_count']  = count($cart_datas);		   
		    return response()->json($data,200);
		}else
		{
            $data['status']      = 201;           	   
		    return response()->json($data,201);
		}
     }

    }


public function cartview(Request $request) 
       {    
                
     	         $cart_data = Cart::where('user_id',$request->user_id)->get();     	         
                 if(count($cart_data) !=0)
                 {      
                          $cart_datas = array();
                        foreach($cart_data as $cart_da)
                        {    
                             
         $product       = Product::find($cart_da->product_id);

		 $Productimages = Productimages::select('product_image')
		                              ->where('product_id',$cart_da->product_id)
		                              ->where('main_image',1)
		                              ->first();		                              
		      $cart_da->product_name = (@$product->productname) ? @$product->productname : '';
		      $cart_da->description  = (@$product->product_description) ? @$product->product_description : '' ;
		      $cart_da->image        = (@$Productimages->product_image) ? @$Productimages->product_image : '' ;        
                        	 $cart_da->cart_count = count($cart_data) ;
                             $cart_datas[] = $cart_da ;
                        }

                        $cart_sum = Cart::where('user_id',$request->user_id)->sum('total_price'); 
		                $data['status']                = 200;
		                $data['message']               = 'Your cart lists';
		                $data['cart_total_amount']     = $cart_sum ;
						$data['cart_data'] = $this->array_null_values_replace($cart_datas);
						return response()->json($data,200);	
                 }else
                 {
                    
					$data['status']            = 201;					
					return response()->json($data,201);	
                 }
     	         
       }


public function updatecart(Request $request) 
       {    
                
     	         $cart_data = Cart::find($request->cart_id);     	         
                 if(count($cart_data) !=0)
                 {      
                          $split_amount = $cart_data->total_price / $cart_data->quantity ;                 

                        if($request->type == 'add')
                        {                           

                             $quantity =  $cart_data->quantity + 1 ;
                             $amount   =  $quantity * $split_amount ;
                        }else
                        {    
                                //not updated quantity when equal 1
                        	if($cart_data->quantity == 1)
                        	     {
                                     $cart_datas = Cart::find($request->cart_id);
			                         $cart_sum = Cart::where('user_id',$cart_datas->user_id)->sum('total_price'); 
					                 $data['status']                = 201;
					                 $data['message']               = 'you don\'t update this cart atleast one quantity you have ';
					                 $data['cart_total_amount']     = $cart_sum ;
									 $data['cart_data'] = $this->array_null_values_replace($cart_datas->toArray());
									 return response()->json($data,201);	
                                 }
                                 //not updated quantity when equal 1

                             $quantity =  $cart_data->quantity - 1 ;
                             $amount   =  $quantity * $split_amount ;
                        }
                                                 
	                     $cart_data->quantity    = $quantity ;
	                     $cart_data->total_price = $amount ;
	                     $cart_data->save() ;

                         $cart_datas = Cart::find($request->cart_id);
                         $cart_sum = Cart::where('user_id',$cart_datas->user_id)->sum('total_price'); 
		                 $data['status']                = 200;
		                 $data['message']               = 'Your cart updated successfully';
		                 $data['cart_total_amount']     = $cart_sum ;
						 $data['cart_data'] = $this->array_null_values_replace($cart_datas->toArray());
						 return response()->json($data,200);	      

                 }else
                 {
                    
					$data['status']            = 201;					
					return response()->json($data,201);	
                 }
     	         
       }

    public function cancelorder(Request $request) 
       {
                $orders = Order::find($request->order_id) ;
                if($orders)
                {    
                	$orders->status = '1' ;
                	$orders->save();

                    $data['status']            = 200;
					$data['message']           = 'Order Cancelled';
					return response()->json($data,200);	
                }else
                {
                    $data['status']            = 201;					
					return response()->json($data,201);	
                }     	           

       }

  public function imagecopy(Request $request)
	 {  
		$ProfileImageData = 'http://www.google.co.in/intl/en_com/images/srpr/logo1w.png';
		$ProfileImagedate = md5(date('Y-m-d H:i:s'));		
		$profileImageFilename = $ProfileImagedate . ".png";			
		$images_path = $_SERVER['DOCUMENT_ROOT'].'/uploads/public/user_images/'.$profileImageFilename;       
		if (copy($ProfileImageData, $images_path)) 
		   {
		        echo "Copy success!"; dd($profileImageFilename) ; exit;
		   }else
		   {
		        echo "Copy failed.";  dd($profileImageFilename) ; exit;
		   } 
     }


    public function gustuser(Request $request)
    {

      
	    	if($request->user_device_id != '')
	    	{
                $user = Users::where('user_devices_id',$request->user_device_id)->count();
	    	if($user == 0)
	    	{

     	     $user_data = new Users();
     	     $user_data->user_first_name     =  '' ;
     	     $user_data->user_email          =  '' ;
     	     $user_data->user_phone_number   =  '' ;     
     	     $user_data->user_gplus_id       =  '' ;
     	     $user_data->user_devices_id     =  $request->user_device_id ;
     	     $user_data->user_type           =  'gust_user';
     	     $user_data->user_status         =  1 ;
     	     $user_data->save();

     	            $user = Users::find($user_data->id);
					$data['data']             = $this->array_null_values_replace($user->toArray());
					$data['status']           =  200;
					$data['message']          = 'User details';
					return response()->json($data,200);	
            }else
            {       $users = Users::where('user_devices_id',$request->user_device_id)->first();                    
					$data['data']             = $this->array_null_values_replace($users->toArray());
					$data['status']           =  200;
					$data['message']          = 'Updated User details';
					return response()->json($data,200);	
            }

			}
		else
			{
                    $data['status']           =  201;
					$data['message']          = 'Device id Required';
					return response()->json($data,201);
			}

   }



   //product search start
	public function product_overall_filter(Request $request)
	 {		

	if(@$request->category_id !=''|| @$request->price !='' || @$request->name !='' )    
	 	{  
	 	   @$productname = Product::where('productname','like','%' .@$request->name .'%')
	 	                           ->get();

	 	   if(count(@$productname) !=0 )
	 	    {
                
                    $Category = DB::table('product')
							->join('productcategories', 'productcategories.product_id', '=', 'product.id')
							->join('productimages', 'productimages.product_id', '=', 'productcategories.product_id')
							->select('product.*','productcategories.*','productimages.*')
							->where('productname','like','%' .@$request->name .'%')
							->where('cleaned_price','>=',@$request->price)
							->where('productcategories.cat_id',@$request->category_id)
							->groupBy('productcategories.product_id')
							->get();		
					if(@$Category->count() ==0)
					{
				      $data['status'] = 201;
				      $data['message'] = "No products";		 
					  return response()->json($data,201);
					}				


		
			foreach($Category as $Categ)
			{          
				if($Categ->combo_price == null || $Categ->default_quantity == null || $Categ->remember_token == null )
				    {
				   @$wish_count = Myfavorites::where('user_id',@$request->user_id)->where('product_id',$Categ->product_id)->count();				   
				    $Categ->combo_price       = ''; 
				    $Categ->default_quantity  = '';
				    $Categ->remember_token    = '';
				    $Categ->wishlist_status   = $wish_count;			     
			        }
			        $Categy[] = $Categ;
			}	   
	    
		$data['status']     = 200;
		$data['categories'] = $this->array_null_values_replace($Categy);
		return response()->json($data,200);
           	}
           else
           {
             	$data['status']  = 201;	
             	$data['message'] = "No products";			
		        return response()->json($data,201);
           }		
	 	}				
		$data['status'] = 201;		
		$data['message'] = "No keywords";		
		return response()->json($data,201);	
	 }
	//product search  end




}
