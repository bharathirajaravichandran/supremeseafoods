<?php

namespace App\Admin\Controllers;

use App\Models\Tracking;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class TrackingController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Tracking');
            $content->description('To manage the order trackings');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Tracking');
            $content->description('To manage the order trackings');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

             $content->header('Tracking');
            $content->description('To manage the order trackings');


            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Tracking::class, function (Grid $grid) {
			$grid->disableExport();

			$grid->disableCreateButton();
			//$grid->disableActions();
			
			$grid->disableRowSelector();
			 
			$grid->order_id('Order ID')->sortable();
			//$grid->column('username','User Name');
			$grid->column('order.order_type','Order Type');
			$grid->column('order.order_cost','Order Cost(in Rs)');
			
			$grid->tracking_number('Tracking Number');
						
			$grid->column('status')->display(function () {
				
				if($this->status==1)
					return "Delivered"; 
				elseif($this->status==2)
					return "In Progress";
				else
					return "";	
			});
						  
			
			$grid->filter(function($filter){

				$filter->disableIdFilter();
				$filter->equal('id', 'Order ID');
				$filter->equal('status','Status')->select(['1' => 'Delivered','2'=>'In Progress']);

			});
			
			$grid->actions(function ($actions) {
               
                    $actions->disableDelete();
               });
						

            //$grid->created_at();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Tracking::class, function (Form $form) {

            $status = [ 1 => 'Delivered', 2=> 'In Progress'];
            $form->select('status','Status')->rules('required')->options($status);
            	$form->disableReset();

        });
    }
}
