<?php

namespace App\Admin\Controllers;

use App\Models\Orderdetail;
use DB;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class OrderdetailController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Order Details List');
            $content->description("List of orders Placed");
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Order Details List');
            $content->description('Edit Orders');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Order Details List');
            $content->description('create orders');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Orderdetail::class, function (Grid $grid) {
            $grid->disableCreateButton();
            // $grid->actions(function ($actions) {
            // $actions->append('<a href="testimonials"><i class="fa fa-eye"></i></a>');
            // });
            $grid->id('ID')->sortable();

			// $grid->actions(function ($actions) {
                    //  $actions->disableEdit();
			// });
            // $grid->order_id('Order ID');
                    $grid->column('order.id', 'Order ID');
                    $grid->column('product.productname', 'Product Name');
            $grid->quantity('Quantity')->sortable();
                    $grid->column('order.order_cost','Product Cost')->sortable();
                    $grid->product_cost('Product Cost')->sortable();
            $grid->type_cuts('Type of Cuts');
            $grid->column('createddatetime', 'Created Date')->display(function ($createddatetime) {
                return date_format(date_create($createddatetime),"d F Y");
            });
            $grid->column('User status')->display(function () {
                return ($this->status==1)?'Active':'In Active';
            });

            $grid->filter(function($filter){

                $filter->disableIdFilter();

                $filter->like('quantity', 'Quantity');
                $filter->like('type_cuts', 'Type of Cuts');
                $filter->equal('status','User Status')->select(['0' => 'In Active','1'=>'Active']);
			});           
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form() {
            return Admin::form(Orderdetail::class, function (Form $form) {
                $form->text('quantity', 'Quantity')->rules('required');
                $form->text('type_cuts', 'Type of Cuts')->rules('required');
                    $form->currency('product_cost', 'Product Cost')->symbol('￥')->rules('required');
                    $status = [ 1 => 'Active', 0 => 'In Active'];
                    $form->select('status', 'User Status')->rules('required')->options($status);

                    //To Update Changed amount in Orders Table
                    $form->saved(function (Form $form) {
                        $updatingOrderID = $form->model()->order_id;
                        
                        $orderCost = 0;
                        $orderDetails = DB::table('orderdetail')->where('order_id', $updatingOrderID)->get();
                        foreach($orderDetails as $orderDetail)
                        {
							$orderCost += $orderDetail->quantity * $orderDetail->product_cost;
						}
						
                        DB::table('order')->where('id', $updatingOrderID)->update(['order_cost' => $orderCost]);
                    return redirect('admin/orders');
        });
                });
    }

}
