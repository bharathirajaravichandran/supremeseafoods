<?php

namespace App\Admin\Controllers;

use App\Models\Category;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Extensions\CategoryExport;
class CategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Product Categories');
            $content->description('The grid of product categories');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Product Categories');
            $content->description('The grid of product categories');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {   
        return Admin::content(function (Content $content) {

            $content->header('Product Categories');
            $content->description('The grid of product categories');
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Category::class, function (Grid $grid) {

            //$grid->id('ID')->sortable();
            $grid->catname('Category Name')->sortable();
            $grid->link('Link')->sortable();
            $grid->catdescription('Category Description')->sortable();
            $grid->cat_image ('Category Image')->setAttributes(['width' => '80px','height' => '80px'])->image();
            $grid->home_cat_image ('Home Category Image')->setAttributes(['width' => ' 80px','height' => ' 80px'])->image();
			$grid->column('status')->display(function () {
				return ($this->status==1)?'Active':'In Active';
			});

			$grid->column('createddatetime', 'Created Date')->display(function ($createddatetime) {

				return date_format(date_create($createddatetime),"d F Y");;
				
			});

			$grid->filter(function($filter){

				$filter->disableIdFilter();

				$filter->like('catname', 'Category Name');
				$filter->like('catdescription', 'Category Description');
				$filter->equal('status','Status')->select(['0' => 'In Active','1'=>'Active']);

			});

            
$grid->exporter(new CategoryExport());

        //    $grid->createddatetime();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Category::class, function (Form $form) {

          //  $form->display('id', 'ID');
            
            $form->text('catname', 'Category Name')->rules('required');
            $form->text('link', 'Link')->rules('required');
            $form->textarea('catdescription','Description')->rules('required');            
			$form->image('cat_image','Category Image')->uniqueName()->rules('required')->move('public/category_images');
			$form->image('home_cat_image','Home Category Image')->uniqueName()->rules('required')->move('public/category_images');		  
			$headertype = [0=> 'No', 1 => 'Yes'];
			$form->select('headertype','Show Header')->rules('required')->options($headertype);
			$status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('status','Status')->rules('required')->options($status);
                        
           // $form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
            	$form->disableReset();
        });
    }
}
