<?php

namespace App\Admin\Controllers;

use App\Models\Users;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Hash;
use App\Admin\Extensions\UserExpoter;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Quotation;

class UsersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('User Details');
            $content->description('');

            $content->body($this->grid());
        });
    }                 

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('User Details');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {
			
			$content->header('User Details');
            $content->description('');
                // $content->body(view('users'));
              $content->body($this->form());
           
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Users::class, function (Grid $grid) {

          //$grid->id('ID')->sortable();
            $grid->user_first_name('First Name');
            $grid->user_email('Email');
            $grid->user_phone_number('Phone Number');      
                
            $grid->filter(function($filter)
            {
            $filter->disableIdFilter();          // Remove the default id filter
            $filter->like('user_first_name', 'First Name');           // Add a column filter
            $filter->like('user_email', 'Email'); 
            $filter->equal('user_phone_number', 'Phone Number');
            $community = [ 1 => 'Muslim', 0=> 'Hindu',2=>'Christian'];
            $filter->equal('user_community', 'Community')->select($community);
            $filter->equal('user_status','Status')->select(['0' => 'InActive','1'=>'Active']);
            }); 
            $grid->column('created_at')->display(function () {
                  $created_at = $this->created_at->toDateString();
                return $created_at;                
            });   
            $grid->column('user_status','Status')->display(function () {
                return ($this->user_status==1)?'Active':'In Active';                
            }); 
              $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->append('<a href="'.url('/').'/admin/user/view/'.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
            });
            Admin::script('$( document ).ready(function() {  $(":reset").css("display", "none"); });');     
            //$grid->updated_at();
            $grid->exporter(new UserExpoter());
        });
    } 

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Users::class, function (Form $form) {

            $form->text('user_first_name', 'First Name')->rules('required|regex:/^[a-zA-Z\s]+$/');
            $form->text('user_last_name', 'Last Name')->rules('required|regex:/^[a-zA-Z\s]+$/');
			$form->text('user_email', 'Email') ->rules(function ($form) {
            if (!$id = $form->model()->id) {
             return 'required|email|unique:users,user_email|min:3|max:40';
            }
            else
             return 'required|email|unique:users,user_email,'.$form->model()->id;
            });
            
            $form->password('password', 'Password')->rules('required|confirmed');
            $form->password('password_confirmation', 'Confirm Password')->rules('required');
			          
			$form->mobile('user_phone_number', 'Phone Number')->options(['mask' => '999 9999 999'])->rules(function ($form) {
            if (!$id = $form->model()->id) {
             return 'required|unique:users,user_phone_number';
            }
            else
             return 'required|unique:users,user_phone_number,'.$form->model()->id;
            });          
            
            $form->image('user_profile_image','Profile Image')->uniqueName()->move('public/user_images')->rules('required');       
            
            $community = [ 0=> 'Hindu',1 => 'Muslim',2=>'Christian'];
            $form->select('user_community', 'Religion')->options($community)->rules('required');
 			$status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('user_status','Status')->options($status)->rules('required');  
			//$form->ignore(['password_confirmation']);
            //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
            
			 $form->saved(function (Form $form) {
					$id = $form->model()->id;
					$users = Users::find($id);
					if($users->old_pwd=='')
					{
						$enc_password=Hash::make($form->password);
						$users->password=$enc_password;
						$users->old_pwd=$enc_password;
						$users->password_confirmation=$enc_password;
						$users->save();
					}
					else if(($users->old_pwd!='') && ($users->old_pwd!=$form->password) && ($users->old_pwd!=Hash::make($form->password)))
					{
						$enc_password=Hash::make($form->password);
						$users->password=$enc_password;
						$users->old_pwd=$enc_password;
						$users->password_confirmation=$enc_password;
						$users->save();						
					}
			 });
			 $form->disableReset();
			 
        });
    }
    
    public function saves(Request $request)
     {


		 $picture_single='';
		if ($request->hasFile('user_profile_image')) {
			$files_single = $request->file('user_profile_image');
			$filename = $files_single->getClientOriginalName();
			$extension = $files_single->getClientOriginalExtension();
			$picture_single = md5(date('Y-m-d H:i:s')).$filename;
			$destinationPath = base_path() . '/public/uploads/public/user_images';
			$files_single->move($destinationPath, $picture_single);
		}
	
		
		 $useraddress = new users;

        $useraddress->user_first_name=$request->input('user_first_name');
        $useraddress->user_last_name=$request->input('user_last_name');
        $useraddress->user_email=$request->input('user_email');
        $useraddress->password=$request->input('password');
        $useraddress->password_confirmation=$request->input('password_confirmation');
        $useraddress->user_phone_number=$request->input('user_phone_number');
        $useraddress->user_profile_image='public/user_images/'.$picture_single;
        $useraddress->user_community=$request->input('user_community');
        $useraddress->user_status=$request->input('user_status');
        $useraddress->save();
         return redirect('admin/users');
	 }

      public function view($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $user_detail = Users::find($id);
            $content->header('Users');
            $content->description('Management'); 
            //dd($user_detail);
           
           $content->body(view('userview',['user_detail'=>$user_detail]));
         
        });
    }   
}
