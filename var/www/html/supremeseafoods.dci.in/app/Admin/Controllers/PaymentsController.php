<?php

namespace App\Admin\Controllers;

use App\Models\Payments;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PaymentsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Payments');
            $content->description('To manage the payments');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Payments');
            $content->description('To manage the payments');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

             $content->header('Payments');
            $content->description('To manage the payments');


            $content->body($this->form()); 
        });
    }
 
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Payments::class, function (Grid $grid) {
			$grid->disableExport();

			$grid->disableCreateButton();
			
			$grid->disableRowSelector();
			 
			$grid->order_id('Order ID')->sortable();
			//$grid->column('username','User Name');
			$grid->column('order.order_type','Order Type');
			$grid->column('order.order_cost','Order Cost(in Rs)');
			$grid->column('paymentmode.mode','Payment Mode');
            //$grid->id('ID')->sortable();
           // $grid->mode('Payment Mode')->sortable();
            $grid->column('status')->display(function () {
				
				if($this->status==1)
					return "Success"; 
				elseif($this->status==2)
					return "Failure";
				elseif($this->status==3)	
					return "Refunded";
				else
					return "";	
			});
						  
			
			$grid->filter(function($filter){

				$filter->disableIdFilter();
				$filter->equal('id', 'Order ID');
				//$filter->like('mode', 'Payment Mode');
				$filter->equal('status','Status')->select(['1' => 'Success','2'=>'Failure','3'=>'Refunded']);

			});
			
			$grid->actions(function ($actions) {
               
                    $actions->disableDelete();
               });
			

            //$grid->created_at();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Payments::class, function (Form $form) {

            //$form->display('id', 'ID');
           // $form->text('mode', 'Payment Mode')->rules('required');
            $status = [ 1 => 'Success', 2=> 'Failure', 3=> 'Refunded'];
            $form->select('status','Status')->rules('required')->options($status);
			$form->disableReset();

           // $form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
