<?php

namespace App\Admin\Controllers;

use App\Models\Whatsavailable;
use App\Models\Hub;
use App\Models\Category;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DB;
use App\Quotation;
use Redirect;


class WhatsavailableController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
			
				$content->header("Whats Available Today");
				$content->description('The whats available today management');
				$content->body($this->grid());
	
        });
    }



	public function saveavailable(Request $request)
    {
    	//dd($request->all());
      $dates=Carbon::now();
      $current_date=$dates->toDateString();
      Whatsavailable::where('date','<',$current_date)->delete();
    	$product_id=$request->input('productid');

    //  $product=new Whatsavailable;

     foreach($product_id as $k =>$v)
      {
        

        if($request->has('cats_'.$k) && $request->filled('cats_'.$k) )
        {
           //dd($v);
           	$datas=Whatsavailable::where('product_id',$v)->where('category_id',$request->category)->where('hub_id',$request->hub)->where('date',$request->StartDate)->first();
           	if(count($datas)>0)
           		$product=Whatsavailable::find($datas->id);
           	else
     			  $product=new Whatsavailable;

           	$product->product_id =$v;
           	$product->category_id =$request->category;
           	$product->hub_id =$request->hub;
           	$product->date =$request->StartDate;
           	if($request->has('cats_'.$k))
           	$product->status =$request->input('cats_'.$k);
            else
            $product->status=0;
           	$product->price =$request->input('price_'.$k);
           	$product->time	 =$request->input('time_'.$k);
           	if(count($datas)>0)
    		    {
              $product->update();
              admin_toastr('Product Updated successfully');
            }
           	else
           	{
              $product->save();
              admin_toastr('Product Created successfully');
           	}
            
        }
      
      }
       return redirect('admin/whatsavailable');
		
	
	}
	
	 /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Whats Available Today');
            $content->description('Edit Whats Available Today');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Create');
            $content->description("Create todays available products");
            $hub=Hub::where('status', 1)->pluck('name','id');
            $category=Category::where('status', 1)->pluck('catname','id');
          
            $counters=count($category);
           
            $currrent_date=Carbon::now();
            $current_dates=$currrent_date->toDateString();
            //dd($currrent_dates);       

            $content->body(view('whatsavailable',['hub'=>$hub,'category'=>$category,'counters'=>$counters,'current_dates'=>$current_dates]));
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Whatsavailable::class, function (Grid $grid) {
        	$dates=Carbon::now();
        	
            $current_date=$dates->toDateString();
           // dd($current_date);
			$grid->model()->where('date',$current_date);
			$grid->actions(function ($actions) {
				$actions->disableDelete();
				$actions->disableEdit();
			});
			 $grid->column('hub.name','Hub');
			 $grid->column('category.catname','category Name'); 
			// $grid->column('hub_id','Hub ID')->sortable();
			$grid->column('product.productname','Product Name');                
            $grid->price('Price')->editable();;
            $grid->time('Delivery Time')->editable();
                $status = [
               'on'  => ['value' => 1, 'text' => 'Active', 'color' => 'primary'],
              'off' => ['value' => 0, 'text' => 'Inactive', 'color' => 'default'],
                  ];
                 $grid->status('Status')->switch($status);
        $grid->filter(function($filter){
                $filter->disableIdFilter();
                // $category=Category::where('status', 1)->pluck('catname','id');

				$filter->equal('hub_id', 'HUB ID');
				 $filter->equal('category.catname','category Name');
				 $filter->equal('product.productname','Product Name');
				 $filter->equal('status','status')->select(['0' => 'InActive','1'=>'Active']);
			});  
      $grid->disableActions();   
		    $grid->disableRowSelector();     
		    $grid->disableExport();      
  			Admin::script('$( document ).ready(function() {  $(":reset").css("display", "none"); });');            
        });
    }
    
    protected function whatsavailbletoday_grid($hub_id,$createddatetime)
    {
        return Admin::grid(Whatsavailable::class, function (Grid $grid) use($hub_id,$createddatetime) {

						
			//$grid->column('hub_id','Hub ID')->sortable();
			//$grid->column('hub.name','Hub');
            $grid->column('product.productname','Product Name');
            $grid->column('product.product_sku','Product SKU');
            $grid->quantity('Quantity')->editable();        
            $grid->price('Price')->editable();        
           	//~ $grid->column('createddatetime', 'Created Date')->display(function ($createddatetime) {
				//~ return date_format(date_create($createddatetime),"d F Y");;
			//~ });
			
           	 $grid->actions(function ($actions) {
				//$actions->disableDelete();
				$actions->disableEdit();
				
			});
           	$grid->disableCreateButton();			
			$grid->filter(function($filter){
                $filter->disableIdFilter();
				$filter->equal('hub_id', 'HUB ID');
				$filter->equal('product.product_sku','Product SKU');
			});  
			
			 $grid->model()->where(['hub_id'=>$hub_id,'createddatetime'=>$createddatetime]);   
			            
        });
    }
   

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Whatsavailable::class, function (Form $form) {

            $form->text('price');
            $form->text('time');
            $form->switch('status');
                                   // $form->file('file');

            //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
      public function getproducts(Request $request)
    {

    	$product_id=$request->get('products'); /*category*/
      $dates=$request->get('dates');
      $hub=$request->get('hub');

      $today_data=Whatsavailable::where('date',$dates)->where('category_id',$product_id)->where('hub_id',$hub)->pluck('product_id');
//dd( $today_data);

    	$catd=DB::table('productcategories')->where('productcategories.status',1)->where('productcategories.cat_id',$product_id)->whereNotIn('productcategories.product_id', $today_data)->join('product', 'product.id', '=', 'productcategories.product_id')->select('productname','product_id')->get();
    $count_catd=count($catd);
    	
    	return view('productdata', ['catd' => $catd,'count_catd' => $count_catd]);
    
    	//return $data;
    	
    }
   
}
//productname
