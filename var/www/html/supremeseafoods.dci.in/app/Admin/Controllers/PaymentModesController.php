<?php

namespace App\Admin\Controllers;

use App\Models\PaymentModes;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class PaymentModesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Payment Modes');
            $content->description('To manage the payment modes like credit/debit card, net banking, COD and Wallet payment');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Payment Modes');
            $content->description('To manage the payment modes like credit/debit card, net banking, COD and Wallet payment');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

             $content->header('Payment Modes');
            $content->description('To manage the payment modes like credit/debit card, net banking, COD and Wallet payment');


            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(PaymentModes::class, function (Grid $grid) {
			$grid->disableExport();

            //$grid->id('ID')->sortable();
            $grid->mode('Payment Mode')->sortable();
            $grid->column('status')->display(function () {
				return ($this->status==1)?'Active':'In Active';
			});
			
			$grid->filter(function($filter){

				$filter->disableIdFilter();
				$filter->like('mode', 'Payment Mode');
				$filter->equal('status','Status')->select(['0' => 'In Active','1'=>'Active']);

			});
			

            //$grid->created_at();
           // $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(PaymentModes::class, function (Form $form) {

            //$form->display('id', 'ID');
            $form->text('mode', 'Payment Mode')->rules('required');
            $status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('status','Status')->rules('required')->options($status);
			$form->disableReset();
           // $form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
