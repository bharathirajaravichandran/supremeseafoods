<?php

namespace App\Admin\Controllers;

use App\Models\Myfavorites;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class MyfavoritesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('WishList');
            $content->description("The management for user's wishlist");
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Myfavorites::class, function (Grid $grid) {

			$grid->actions(function ($actions) {
				$actions->disableEdit();
			});
			$grid->column('user.user_first_name','User Name');
			$grid->column('product.productname','Product Name');
			$grid->disableCreateButton();
           
			$grid->filter(function($filter){
				$filter->disableIdFilter();
				$filter->like('user.user_first_name', 'User Name');
				$filter->like('product.productname', 'Product Name');

			});           
           
           
           
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Myfavorites::class, function (Form $form) {

            //$form->display('id', 'ID');

           // $form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
