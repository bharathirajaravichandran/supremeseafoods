<?php

namespace App\Admin\Controllers;
use App\Models\Order;
use DB;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Admin\Extensions\InvoiceExport;

class InvoicesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Invoices');
            $content->description("Invoices for list of orders Placed");
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Invoices');
            $content->description('Invoices for list of orders Placed');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Order::class, function (Grid $grid) {
            $grid->disableCreateButton();
           
            $grid->id('Order ID')->sortable();
            
           $grid->disableRowSelector();

			 $grid->actions(function ($actions) {
			 	$actions->disableEdit();
			 	$actions->disableDelete();
			 	$actions->append('<a class="btn-xs btn-primary" href="'.url('/').'/admin/invoice/'.$actions->getKey().'">Show Invoice</a>');
			 });
            $grid->order_type('Order Type');
			$grid->column('user.user_first_name','User Name');

       
            $grid->order_cost('Order Cost(in Rs)')->sortable();
                         
            $grid->filter(function($filter){

                $filter->disableIdFilter();

                $filter->equal('id', 'Order ID');
                $filter->like('order_type', 'Order Type');
                $filter->like('order_cost', 'Order Cost');
                
			}); 
			            
$grid->exporter(new InvoiceExport());          
        });
    }

        public function invoice($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $order = DB::table('order')
							->join('orderdetail', 'order.id', '=', 'orderdetail.order_id')
							->join('users', 'order.user_id', '=', 'users.id')
							->join('product', 'orderdetail.product_id', '=', 'product.id')
							->join('paymentmodes', 'order.payment_mode', '=', 'paymentmodes.id')
							->where('order.id', $id)->get();
			$subTotal = 0;
			foreach($order as $order_item)
				$subTotal += $order_item->quantity * $order_item->product_cost;
			$netTotal = $subTotal;
				
			$content->header('Invoice Details');
            $content->description('The detailed description of the invoice details');
            $content->body(view('invoice',['order'=>$order,'subTotal'=>$subTotal,'netTotal'=>$netTotal]));
        });
    }

   
}
