<?php

namespace App\Admin\Controllers;

use App\Models\Report;
use App\Models\Category;
use App\Models\Users;
use Illuminate\Http\Request;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;


class ReportController extends Controller
{
    
    public function index()
    {
		  return Admin::content(function (Content $content) {
		$content->body(view('adminform'));
    });
    }
    public function reports(Request $request)
    {
		  $this->validate($request,[
         'fromdate'=>'required',
         'todate' => 'required',
         'report' => 'required',
      ]);
	       $report = $request->report;
	        $fromdate =$request->fromdate; 
	    $from = date("Y-m-d H:i:s",strtotime($fromdate));
	        $todate = $request->todate;
       $to = date("Y-m-d H:i:s",strtotime($todate));
	       
		 		  
				switch($report)
				{
				case "order":
				            $users = Report::whereBetween('created_at',  array($from, $to))->where('order_cost', '>', '3000')->get() ;
						     $csvExporter = new \Laracsv\Export();
						     $csvExporter->build($users, ['order_type', 'user_id','order_cost','address','city','state','country','phone'])->download();
						     break;
			    case "Category":
							 $users = Category::whereBetween('created_at',  array($from, $to))->get() ;
							  $csvExporter = new \Laracsv\Export();
							   $csvExporter->build($users, ['id','catname', 'catdescription'])->download();
							   break;
				case "Customer report":
				     
							     $users = Users::whereBetween('created_at',  array($from, $to))->get() ;
							    $csvExporter = new \Laracsv\Export();
							     $csvExporter->build($users, ['user_first_name','user_last_name', 'user_email','user_phone_number'])->download();
							     break;
				}
				
	}
				
	
}
