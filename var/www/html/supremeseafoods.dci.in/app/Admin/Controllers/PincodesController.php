<?php

namespace App\Admin\Controllers;

use App\Models\Pincodes;
use DB;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;

class PincodesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Pincodes');
            $content->description("List of pincodes added in Hub");
            $hub_id = Input::get('hub_id', false);
            //dd($hub_id);
            $content->row('<a href="/admin/pincodes/create/'.$hub_id.'" class="btn btn-sm btn-success"><i class="fa fa-save"></i>&nbsp;Upload Pincodes</a><a href="/admin/hub" class="btn btn-sm btn-default form-history-back pull-right"><i class="fa fa-arrow-left"></i>&nbsp;Back to Hub List</a>');
            $content->row('&nbsp;');
            $content->body($this->grid());
               
           
        });
    }
    
   
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Pincodes');
            $content->description('Edit Pincodes');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create($id)
    {
		return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $content->body(view('pincodeupload',['hub_id'=>$id]));
        });
    }
    
    public function showUploadFile(Request $request){
	  		  
	  $this->validate($request,[
         'pincode' => 'required'
      ]);
          
	   
      $file = $request->file('pincode');
      if($file->getMimeType()=='text/plain' || $file->getMimeType()=='text/csv' || $file->getMimeType()=='application/vnd.ms-excel')
      {
		  
		$hub_id = $request->input('hub_id');
		$destinationPath = 'uploads';
        $file->move($destinationPath,$file->getClientOriginalName());
        $file_content = fopen("uploads/".$file->getClientOriginalName(),"r");
		while($line = fgetcsv($file_content))
		{
		   
		   if(!empty($line[0]) && !empty($line[1]))
		   {
				$count_pincode = DB::table('pincodes')->where(['hub_id' => $hub_id,'location' => $line[0],'pincode' => $line[1]])->count();
				if($count_pincode==0)
				DB::table('pincodes')->insert(['hub_id' => $hub_id,'location' => $line[0],'pincode' => $line[1], 'status' => 1]);
		   }
	    }
	    fclose($file_content);
		
		if(file_exists("uploads/".$file->getClientOriginalName()))
			unlink("uploads/".$file->getClientOriginalName());	  
		
		return redirect('admin/pincodes?hub_id='.$hub_id); 
		    
      }
      else
		return Redirect::back()->withErrors(['Please upload a valid CSV file.']);
   
   }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Pincodes::class, function (Grid $grid) {
            $grid->disableCreateButton();

            $grid->id('ID')->sortable();
             $grid->disableExport(); 
             $grid->disableRowSelector();
               $grid->actions(function ($actions) {
                 $actions->disableDelete();
            });
            //$grid->column('hub.name','Hub Name');
               $grid->location('Route');
            $grid->location('Location')->sortable();
            $grid->pincode('Pincode')->sortable();
            $grid->filter(function($filter){
                $filter->disableIdFilter();
				$filter->equal('hub_id', 'HUB ID');
				$filter->like('location', 'Location');
				$filter->like('pincode', 'Pincode');
                $filter->equal('status','Status')->select(['0' => 'In Active','1'=>'Active']);
			});  
             Admin::script('$( document ).ready(function() { $(".Status").select2({"allowClear":true,"placeholder":"Please Select Status"}); $(":reset").css("display", "none"); $("td .icheckbox_minimal-blue").hide(); });');
                    
        });
    }
    
    public function pincodeview($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $pincodes = DB::table('pincodes')
							->select('pincodes.*','hub.name')
							->join('hub', 'pincodes.hub_id', '=', 'hub.id')
							->where('pincodes.hub_id',$id)->paginate(15);
            $content->header('Pincodes');
            $content->description('List of pincodes');
            $content->body(view('pincodeview',['pincodes'=>$pincodes]));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form() {
            return Admin::form(Pincodes::class, function (Form $form) {
               
                    $form->text('pincode', 'Pincode')->rules('required');
                    $status = [ 1 => 'Active', 0 => 'In Active'];
                    $form->select('status', 'Status')->rules('required')->options($status);
                   $form->disableReset(); 
                });
    }

}
