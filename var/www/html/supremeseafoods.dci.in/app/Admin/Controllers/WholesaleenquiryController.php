<?php

namespace App\Admin\Controllers;

use App\Models\Wholesaleenquiry;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Quotation;
use App\Models\Product;

class WholesaleenquiryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Wholesale Enquiry');
            $content->description('The management of wholesale enquiries');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }
    
    /**
     * View interface.
     *
     * @return Content
     */
    public function wholesaleview($id)
    {
        return Admin::content(function (Content $content) use ($id) {
			
			$detail_array=array();
            $enquiry = DB::table('wholesaleenquiry')->where('id', $id)->first();
            $product_ids=$enquiry->product_id;
            $product_array=explode(",",$product_ids);

            $product_qty=$enquiry->quantity;
            $product_qty_array=explode(",",$product_qty);

            $products = DB::table('product')->whereIn('product.id',$product_array )->where('productimages.main_image',1)->join('productimages', 'product.id', '=', 'productimages.product_id')->get();
            //$products = Product::whereIn('product.id',$product_array)->join('productimages', 'product.id', '=', 'productimages.product_id')->get();

            $i=0;
            foreach($products as $k=>$v)
            {
				$detail_array[$i]['productname']=$v->productname;
				$detail_array[$i]['productimage']=$v->product_image;
				$detail_array[$i]['product_whole_price']=$v->whole_price;
				$detail_array[$i]['product_cleaned_price']=$v->cleaned_price;
				$detail_array[$i]['quantity']=$product_qty_array[$i];
				$i++;
			}


            $content->header('Wholesale Enquiry Details');
            $content->description('The detailed description of the wholesale enquiry');
            $content->body(view('wholesaleview',['proddetails'=>$detail_array]));
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Wholesaleenquiry::class, function (Grid $grid) {

			$grid->disableExport();

			$grid->actions(function ($actions) {
				$actions->disableDelete();
				$actions->append('<a href="'.url('/').'/admin/wholesaleview/'.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
			});
			$grid->disableCreateButton();
			$grid->column('user.user_first_name','User Name');
            
            $grid->description('Description')->sortable();

			$grid->column('deliverydatetime', 'Delivery Date')->display(function ($deliverydatetime) {

				return date_format(date_create($deliverydatetime),"d F Y");;
				
			});

			$grid->column('user_status','Status')->display(function () {
				return ($this->status==1)?'Completed':'Not Attended';
			});

            //$grid->created_at();
            
            
            //$grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Wholesaleenquiry::class, function (Form $form) {

            //$form->display('id', 'ID');
 			$status = [ 1 => 'Completed', 0=> 'Not Attended'];
            $form->select('status','Status')->rules('required')->options($status);  
            //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
            	$form->disableReset();
        });
    }
}
