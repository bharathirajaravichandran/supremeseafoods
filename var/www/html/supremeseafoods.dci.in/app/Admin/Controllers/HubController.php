<?php
namespace App\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

use Illuminate\Support\Facades\Redirect;
use DB;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Hub;

class HubController extends Controller {
	
	use ModelForm;
   
   public function index()
   {
        return Admin::content(function (Content $content) {
            $content->header('Hub List ');
            $content->description("List of Hubs");
            $content->body($this->grid());
        });
   }
    	
   public function create(){
	   return Admin::content(function (Content $content) {
            $content->header('Hub');
            $content->description('management');
            $content->body(view('hub'));
        }); 
	 
   }
   
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Hub List');
            $content->description('Edit Hub');

            $content->body($this->form()->edit($id));
        });
    }
    
    protected function grid()
    {
        return Admin::grid(Hub::class, function (Grid $grid) {
            //$grid->disableCreateButton();
            $grid->disableExport();
            $grid->disableRowSelector();
            
            $grid->actions(function ($actions) {
                 $actions->disableDelete();
            });

            $grid->id('Hub ID')->sortable();
			
            $grid->name('Hub Name');
			            
            $grid->column('Hub status')->display(function () {
                return ($this->status==1)?'Active':'In Active';
            });
                        
            $grid->column("")->display(function () {
                return '<a class="btn-xs btn-primary" href="'.url('/').'/admin/pincodes?hub_id='.$this->id.'">Route</a>';
            });
                        
            $grid->filter(function($filter){

               $filter->disableIdFilter();
           
                $filter->like('name', 'Hub Name');
                $filter->equal('status','Hub Status')->select(['0' => 'In Active','1'=>'Active']);
			      });  
            Admin::script('$( document ).ready(function() { $(".Status").select2({"allowClear":true,"placeholder":"Please Select Status"}); $(":reset").css("display", "none"); });');         
        });
    }
    
     protected function form()
    {
            return Admin::form(Hub::class, function (Form $form) {
                //$form->currency('order_cost', 'Order Cost')->symbol('￥')->rules('required');
                $form->text('name', 'Hub Name')->rules('required');
                $status = [ 1 => 'Active', 0=> 'In Active'];
                $form->select('status','Hub Status')->rules('required')->options($status);
                	$form->disableReset(); 
        });
    }
    
   public function showUploadFile(Request $request){
	  		  
	  $this->validate($request,[
         'hubname'=>'required',
         'pincode' => 'required'
      ]);
          
	   
      $file = $request->file('pincode');
      if($file->getMimeType()=='text/plain' || $file->getMimeType()=='text/csv' || $file->getMimeType()=='application/vnd.ms-excel')
      {
		  
		$hub_id = DB::table('hub')->insertGetId(['name' => $request->hubname, 'status' => 1]);
		$destinationPath = 'uploads';
        $file->move($destinationPath,$file->getClientOriginalName());
        $file_content = fopen("uploads/".$file->getClientOriginalName(),"r");
		while($line = fgetcsv($file_content))
		{
		   if(!empty($line[0]) && !empty($line[1]))
				DB::table('pincodes')->insert(['hub_id' => $hub_id,'location' => $line[0],'pincode' => $line[1], 'status' => 1]);
	    }
	    fclose($file_content);
		
		if(file_exists("uploads/".$file->getClientOriginalName()))
			unlink("uploads/".$file->getClientOriginalName());	  
		
		return redirect('admin/hub'); 
		    
      }
      else
		return Redirect::back()->withErrors(['Please upload a valid CSV file.']);
   
   }
}
