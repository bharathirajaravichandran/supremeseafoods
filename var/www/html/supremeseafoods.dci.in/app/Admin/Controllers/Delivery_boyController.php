<?php

namespace App\Admin\Controllers;

use App\Models\Delivery_boy;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class Delivery_boyController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Delivery Boy');
            $content->description('Management');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Delivery Boy');
            $content->description('Management');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Delivery Boy');
            $content->description('Management');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Delivery_boy::class, function (Grid $grid) {

            //$grid->id('ID')->sortable();
             $grid->disableExport(); 
             $grid->name('Delivery Boy Name');
            $grid->mobile('Phone Number');
            $grid->joint_date('Joint Date'); 
             $grid->column('status','Status')->display(function () {
                return ($this->status==1)?'Active':'InActive';                
            });      
            $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->append('<a href="'.url('/').'/admin/deliveryboy/view/'.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
            });
            $grid->filter(function($filter)
            {
            $filter->disableIdFilter();          // Remove the default id filter
            $filter->like('name', 'Delivery Boy Name');           // Add a column filter
            $filter->equal('mobile', 'Phone Number'); 
            $filter->equal('joint_date', 'Joint Date');
            $filter->equal('status', 'Status')->select(['0' => 'InActive','1'=>'Active']);
          
            }); 
            Admin::script('$( document ).ready(function() {  $(":reset").css("display", "none"); });');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Delivery_boy::class, function (Form $form) {

            $form->hidden('id', 'ID');
            $form->text('name', 'Delivery Boy Name') ->rules('required|regex:/^[a-zA-Z\s]+$/');
            $form->mobile('mobile', 'Phone Number')->options(['mask' => '999 9999 999'])->rules(function ($form) {
            if (!$id = $form->model()->id) {
             return 'required|unique:deliveryboy_details,mobile';
            }
            else
             return 'required|unique:deliveryboy_details,mobile,'.$form->model()->id;
            });   
            $form->textarea('address', 'Address') ->rules('required');
            $form->date('joint_date', 'Joining Date') ->rules('required');
             
            $status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('status','Status')->options($status)->rules('required'); 

            $form->hidden('created_at', 'Created At');
            $form->hidden('updated_at', 'Updated At');
            $form->disableReset();
        });
    }
}
