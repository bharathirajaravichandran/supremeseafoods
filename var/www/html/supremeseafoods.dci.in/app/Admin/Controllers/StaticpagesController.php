<?php

namespace App\Admin\Controllers;

use App\Models\Staticpages;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class StaticpagesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
		
//~ echo "sdfsdf";
//~ exit;
        return Admin::content(function (Content $content) {

            $content->header('CMS Pages');
            $content->description('The grid of CMS pages');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('CMS Pages');
            $content->description('The grid of CMS pages');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('CMS Pages');
            $content->description('The grid of CMS pages');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Staticpages::class, function (Grid $grid) {



            //$grid->id('ID')->sortable();
            $grid->pagename('Page Name')->sortable();
            $grid->pagelink('Link')->sortable();
            $grid->pagecontent('Page Content')->sortable();
            $grid->column('status')->display(function () {
                return ($this->status==1)?'Active':'In Active';
            });            
			$grid->column('footer_status')->display(function () {
				return ($this->footer_status==2)?'Others':($this->footer_status==1)?'Footer Right':'Footer Left';
			});            
			$grid->column('createddatetime', 'Created Date')->display(function ($createddatetime) {

				return date_format(date_create($createddatetime),"d F Y");
				
			});

			$grid->filter(function($filter){

				$filter->disableIdFilter();

				$filter->like('pagename', 'Page Name');
				$filter->like('pagecontent', 'Page Content');
				$filter->equal('status','Status')->select(['0' => 'In Active','1'=>'Active']);

			});



            //$grid->created_at();
            //$grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Staticpages::class, function (Form $form) {

           // $form->display('id', 'ID');
           // $form->text('pagename', 'Page Name')->rules('required');
            
            
			$form->text('pagename', 'Page Name')->rules(function ($form) {
				$id = $form->model()->id;
				// If it is not an edit state, add field unique verification
				//if (!$id = $form->model()->id) {
				return 'required|unique:staticpages,pagename'. ($id ? ",$id" : '');
				//}
				//else
				//{
					//return 'required';					
				//}

			});
            $form->text('pagelink', 'Page Link')->rules(function ($form) {
                $id = $form->model()->id;
                // If it is not an edit state, add field unique verification
                //if (!$id = $form->model()->id) {
                return 'required|without_spaces|unique:staticpages,pagelink'. ($id ? ",$id" : '');
                //}
                //else
                //{
                    //return 'required';                    
                //}

            });
            //$form->html('<span id="tt">');
            $form->textarea('pagecontent','Page Content')->rules('required');            
            //$form->html('</span>');footer_status
            $status1 = [2=>'Other', 1 => 'Footer Right', 0=> 'Footer Left'];
            $form->select('footer_status','Footer Show')->rules('required')->options($status1); 
			$status = [ 1 => 'Active', 0=> 'In Active'];
            
            $form->select('status','Status')->rules('required')->options($status);            
            //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
            	$form->disableReset();
        });
    }
}
