<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class InvoiceExport extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();
		
        Excel::create('category', function($excel) {
               $data = $this->getData();
              
                  foreach($data as $i=> $dat) {
                $data_[$i]['id'] = $dat['id'];
                $data_[$i]['order type'] = $dat['order_type'];
                $data_[$i]['user name'] = $dat['user']['user_first_name'];
             $data_[$i]['order cost'] = $dat['order_cost'];
                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('xls');
    }
}
