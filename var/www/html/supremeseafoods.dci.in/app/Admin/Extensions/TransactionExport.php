<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class TransactionExport extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();

		        Excel::create('Transaction', function($excel) {
               $data = $this->getData();
            
                  foreach($data as $i=> $dat) {
                $data_[$i]['id'] = $dat['id'];
                $data_[$i]['order id'] = $dat['order_id'];
                $data_[$i]['transaction type'] = $dat['tranx_type'];
                $data_[$i]['transaction details'] = $dat['tranx_details'];
                $data_[$i]['order type'] = $dat['order']['order_type'];
                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('xls');
    }
}
