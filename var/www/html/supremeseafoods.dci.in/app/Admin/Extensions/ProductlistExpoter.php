<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class ProductlistExpoter extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();
        Excel::create('product list', function($excel) {
               $data = $this->getData();
                  foreach($data as $i=> $dat) {
                $data_[$i]['id'] = $dat['id'];
                $data_[$i]['productname'] = $dat['productname'];
                $data_[$i]['product_sku'] = $dat['product_sku'];
                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('xls');
    }
}
