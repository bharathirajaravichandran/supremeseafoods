<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class UserExpoter extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();
        Excel::create('users', function($excel) {
               $data = $this->getData();
                  foreach($data as $i=> $dat) {
                $data_[$i]['First Name '] = $dat['user_first_name'];
                $data_[$i]['Last Name '] = $dat['user_last_name'];
                $data_[$i]['Email '] = $dat['user_email'];
                $data_[$i]['Phone Number '] = $dat['user_phone_number'];
                if($dat['user_community']==0 && $dat['user_community'] !="")
                $data_[$i]['Community'] = 'Hindu';  
                if($dat['user_community']==1 && $dat['user_community'] !="")
                $data_[$i]['Community'] = 'Musilim'; 
                if($dat['user_community']==2 && $dat['user_community'] !="")
                $data_[$i]['Community'] = 'Christian';

                if($dat['user_status']==1)
                $data_[$i]['Status'] = 'Active';
                else
                $data_[$i]['Status'] = 'InActive';

                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('csv');
    }
}
