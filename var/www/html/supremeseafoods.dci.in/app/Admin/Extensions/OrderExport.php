<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class OrderExport extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();
     
		        Excel::create('orders', function($excel) {
               $data = $this->getData();
             
                  foreach($data as $i=> $dat) {
                $data_[$i]['id'] = $dat['id'];
                $data_[$i]['order type'] = $dat['order_type'];
                $data_[$i]['user name'] = $dat['user']['user_first_name'];
                $data_[$i]['order cost'] = $dat['order_cost'];
                $data_[$i]['address'] = $dat['address'];
                $data_[$i]['city'] = $dat['city'];
                $data_[$i]['state'] = $dat['state'];
                $data_[$i]['mobilenumber'] = $dat['phone'];
                $data_[$i]['delivery date time'] = $dat['delivery_datetime'];
                $data_[$i]['created date time'] = $dat['createddatetime'];
                
                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('xls');
    }
}
