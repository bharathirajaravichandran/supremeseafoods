<?php

namespace App\Admin\Extensions;

use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;



class CategoryExport extends AbstractExporter
{
	
	
    public function export()
    {
		$filename = $this->getTable();
        Excel::create('category', function($excel) {
               $data = $this->getData();
                  foreach($data as $i=> $dat) {
                $data_[$i]['id'] = $dat['id'];
                $data_[$i]['category name'] = $dat['catname'];
                $data_[$i]['category description'] = $dat['catdescription'];
             $data_[$i]['category image'] = url('/').'/upload/'.$dat['cat_image'];
                
        }
            $excel->sheet('Sheetname', function($sheet) use($data_){

                // This logic get the columns that need to be exported from the table data
                  $sheet->fromArray($data_);
              

            });

        })->export('xls');
    }
}
