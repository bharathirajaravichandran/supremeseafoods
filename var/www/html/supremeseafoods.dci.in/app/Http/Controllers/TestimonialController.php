<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Testimonial;
use App\Models\Users;
use DB;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonial = Testimonial::all();
        //~ $users = Users::all();
        $provinceId = $testimonial->get('user_id');
		$values=Users::where('id',$provinceId)->get(['id', DB::raw('name as text')]);
		return view('welcome')->with('values', $values);
    }
    

}
