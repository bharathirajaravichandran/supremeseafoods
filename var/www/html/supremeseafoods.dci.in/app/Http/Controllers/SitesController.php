<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Guard;
use App\Http\Requests;
use App\Category;
use App\Models\Users;
use App\Models\Product;
use App\Models\Cuts;
use App\Models\Productimages;
use App\Models\Testimonials;
use App\Models\State;
use App\Models\Myfavorites;
use App\CartAdd;
//~ use App\Models\Wishlist;
use DB;
use Session;
use Common;
use Auth;
use Validator;
use App\Profil;
use App\User;
use Illuminate\Support\Collection;
use Bhavinjr\Wishlist\Facades\Wishlist;
use Gloudemans\Shoppingcart\Facades\Cart;
use Socialite;
use App\Order;
use Exception;

class SitesController extends Controller
{
	public $instance;
   public function index()
    {
        $categories = Category::all();
        $states = State::all();        
        
        $testimonials = Testimonials::with('user')->join('users', 'users.id', '=', 'testimonials.user_id')->get();
         $crabs = Product::all();
        return view('welcome')->with('categories', $categories)->with('testimonials',$testimonials)->with('crabs',$crabs)->with('states', $states);       
    }
    
public function autocomplete(Request $request)
       {
			
			$term = $request->term; 			
			$data = DB::table('product')
				->join('productcategories','productcategories.product_id','=','product.id')    	
				->join('category','category.id','=','productcategories.cat_id')				
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',1)
				->where('productname','LIKE', strtolower("%$term%"))->get();
			$result = array();
         

		if(!$data->isEmpty())
		{
         foreach ($data as $key => $value) 
            {
				$result['id'] = $value->id;
				$result['value'] = $value->productname; 
				$result['image']= '/uploads/public/product_images/'.$value->product_image;
                $result['url']= url('category/'.preg_replace('/[^A-Za-z0-9\-]/','', $value->catname).'/'.preg_replace('/[^A-Za-z0-9\-]/','', $value->productname).'/'.$value->product_id);
				$results[] = $result; 
			}
		}
		else
		{
			
			$result['value']="No result Found";
			$results[] = $result; 
			
			}
             echo json_encode($results); 
		
		}




    public function login(Request $request)
	{
		
        $request->session()->put('url.intended',url()->previous()); /*getting previous url*/
        return view('auth/login');
	}
    
   
	public function signin(Request $request)
	{
		
		
		//validator
		 $validator = Validator::make($request->all(), [
		 
            'email' => 'required|email',
            'user_password' => 'required',
            
        ]);
		
        if ($validator->fails()) 
        {
			
            return redirect('/signup')
                        ->withErrors($validator)
                        ->withInput();
        }
          $user_email=DB::table('users')->pluck('user_email');
		if($user_email==$request->email)
        {
			return redirect($request->session()->get('url.intended'));
			
		}
		
		//login
		if(Auth::attempt([		/*validate the user */	
				'user_email' => trim($request->email),
				'password' => $request->user_password
		
		]))
			
		if (Auth::user() != null)
		{			
			return redirect($request->session()->get('url.intended'));
		}	
		Session::flash('adminusername', 'Email Or Password Does Not Match');
		return redirect('/signup')->withInput();
		
	}
	
		
    public function logout(Request $request)
	{
        $request->session()->invalidate(); /*logout from everywhere*/
		$categories = Category::all();
	    $testimonials = Testimonials::with('user')->join('users', 'users.id', '=', 'testimonials.user_id')->get();
		$request->session()->put('url.intended',url()->previous());
        return redirect('login'); 
	}
	
	public function product(Request $request)
    {
        
       
        $testimonials = Testimonials::with('user')->join('users', 'users.id', '=', 'testimonials.user_id')->get();
        
		$crabsId = $request->id;	
								    
		
		$crabs =  DB::table('category')
				->join('productcategories','productcategories.cat_id','=','category.id')
				->join('product','product.id','=','productcategories.product_id')
				->join('productimages','productimages.product_id','=','productcategories.product_id')
				->where('productimages.main_image',1)
				->where('category.id',$crabsId)
				->get();
				
		$catname =  $request->name;
								    
		
      
        			    
        return view('site/product')->with('testimonials',$testimonials)->with('crabs',$crabs)->with('catname',$catname);       
    }
    
	public function state(Request $request)
    {
        $categories = Category::where('headertype',1)->get();
         
		$crabsId = $request->id;	
		
		$crabs =  DB::table('state')
				->join('productstate','productstate.state_id','=','state.id')
				->join('product','product.id','=','productstate.product_id')				
				->join('productimages','productimages.product_id','=','productstate.product_id')
				->where('productimages.main_image',1)
				->where('state.id',$crabsId)
				->get();
				//~ dd($crabs);
		$catname =  $request->name;
		return view('site/stateproduct')->with('crabs',$crabs)->with('categories',$categories)->with('catname',$catname);       
    }
    
	public function productdetails(Request $request)
    {
		
		$productId = $request->id;
		//~ dd($productId);
		
		$catIds =  DB::table('productcategories')				
				->where('productcategories.product_id',$productId)
				->first();
				
		$catId = $catIds->cat_id;
	
		$products =  DB::table('product')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',1)
				->where('product.id',$productId)
				->get();
		
		$productimages =  DB::table('product')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',0)
				->where('product.id',$productId)
				->get();
				
				
				
		$relatedproducts =  DB::table('relatedproduct')
				->join('product','product.id','=','relatedproduct.related_id')
				->join('productcategories','productcategories.product_id','=','relatedproduct.product_id')    	
				->join('category','category.id','=','productcategories.cat_id')				
				->join('productimages','productimages.product_id','=','relatedproduct.related_id')
				->where('productimages.main_image',1)										
				->where('relatedproduct.product_id',$productId)
				->get();
				
			
		
		$slices=DB::table('cuts')
				->join('productcuts','productcuts.cut_id','=','cuts.id')
				->where('productcuts.product_id',$productId)
				->get();
				
				
		$users=Auth::user()['id'];	
		
		$categories = Category::all();
							    
	
        return view('site/product_details')->with('users',$users)
        ->with('catId',$catId)->with('categories',$categories)
        ->with('products',$products)->with('productimages',$productimages)
        ->with('slices',$slices)->with('relatedproducts',$relatedproducts);
    }
    
	public function productdetail(Request $request)
    {

		$productId = $request->id;	
		
		$catIds =  DB::table('productstate')				
				->where('productstate.product_id',$productId)
				->first();
			
			$catId = $catIds->state_id;	
	
		$products =  DB::table('product')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',1)
				->where('product.id',$productId)
				->get();
		
		$productimages =  DB::table('product')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',0)
				->where('product.id',$productId)
				->get();
				
		$relatedproducts =  DB::table('relatedproduct')
				->join('product','product.id','=','relatedproduct.related_id')
				->join('productcategories','productcategories.product_id','=','relatedproduct.product_id')    	
				->join('category','category.id','=','productcategories.cat_id')				
				->join('productimages','productimages.product_id','=','relatedproduct.related_id')
				->where('productimages.main_image',1)										
				->where('relatedproduct.product_id',$productId)
				->get();
				
	
		
		$slices=DB::table('cuts')
				->join('productcuts','productcuts.cut_id','=','cuts.id')
				->where('productcuts.product_id',$productId)
				->get();
				
		$users=Auth::user()['id'];	
		
		$categories = Category::all();
		
		
        return view('site/product_details')->with('users',$users)
        ->with('catId',$catId)->with('categories',$categories)
        ->with('products',$products)->with('productimages',$productimages)
        ->with('slices',$slices)->with('relatedproducts',$relatedproducts);
    }
 
     public function removes(Request $request)	
    {	
	$id = $request->id;	
	$cutname = $request->cutname;
	$price = $request->price;	
	$userid = $request->userid;	
	Myfavorites::where('user_id', $userid)->where('product_id', $id)->where('cutname',$cutname)->where('price',$price)->delete();
	return redirect()->back();
	
	}
    
    
     public function wishlist(Request $request) 
    {
		
		if($request->input('user_id') != '')
		    { 
		        $requests = Myfavorites::where('product_id',$request->pro_id)->where('user_id',$request->user_id)->where('cutname',$request->cutname)->where('price',$request->price)->first();
		        if(count($requests) == 0)
		        {  
					$Myfavorites = new Myfavorites;
		            $Myfavorites->user_id    = $request->input('user_id');
		            $Myfavorites->product_id =$request->input('pro_id');
		            $Myfavorites->price =$request->input('price');		         
		            $Myfavorites->price_type =$request->input('price_type');		         
		            $Myfavorites->cutname =$request->input('cutname');
		            $Myfavorites->save();
		
		             $responses = array(
		                        'pro_id' => '0',
		                        'message' =>'Add to wishlist',
		                        
		                                    );
		             return response()->json($responses);
		        }
		        else
		        {            
		            $requests->user_id    = $request->input('user_id');
		            $requests->product_id = $request->input('pro_id'); 
		            $requests->price = $request->input('price'); 		           
		            $requests->price_type = $request->input('price_type'); 		           
		            $requests->cutname = $request->input('cutname'); 
		            $requests->price_type = $request->input('price_type'); 
		            $requests->save();     
		
		
		             $responses = array(
		                        'pro_id' => '1',
		                        'message' =>'Already Added to wishlist'
		                                    );
		             return response()->json($responses);
		        }       
		           
		         
		  }
		  else
		  {
		        $responses = array(
		                        'user_id' => '0',
		                        $request->session()->put('url.intended',url()->previous()),
		                                    );
		        return response()->json($responses);
		 }
		
		$productId=$request->pro_id;
		$userid=$request->user_id; 
		$productById=DB::table('product')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',1)
				->where('product.id',$productId)
				->first();	
		$cart =Cart::instance('wishlist')->add(['id' => $productId, 'name' => $productById->productname, 'qty' => $request->quantity, 'price' => $request->price,'options' => ['image' => $productById->product_image,'totals' => $request->quantity*($request->price),'user_id' => $userid]]);    
			return redirect()->back();
	}
    
    public function wishlistitems() 
    {
		//~ $wishlists=Cart::instance('wishlist')->content();
	
		if(count(Auth::user()) !=0)
		{
		 $UserId=Auth::user()->id;
    	$wishlists = DB::table('myfavorites')
    	->join('product','product.id','=','myfavorites.product_id')    	
    	->join('productcategories','productcategories.product_id','=','myfavorites.product_id')    	
    	->join('category','category.id','=','productcategories.cat_id')
    	->join('productimages','productimages.product_id','=','product.id')
		->where('productimages.main_image',1)
    	->where('myfavorites.user_id',$UserId)
    	->get();
    	
    		//~ dd($wishlists);

		return view('site/wishlist')->with('wishlists',$wishlists);
		}
		else
		{
			
			return redirect('/');
		}
	
	}
    
    public function removeall() 
    {
		//~ Cart::instance('wishlist')->destroy();
		DB::table('myfavorites')->truncate();
		return redirect()->back();
	}
    


    
  
    public function add(Request $request) 
    {
		
		$cartadd=CartAdd::where('product_id',$request->pro_id)->where('total_price',$request->price)->where('cuts_type',$request->type)->first();
		
		if($cartadd == null)
		{	
			
			$cart = new CartAdd;
			$cart->product_id = $request->input('pro_id');
	        $cart->quantity = $request->input('quantity');
	        $cart->total_price = $request->input('price');
	        $cart->cuts_type = $request->input('type');
	       
	        if(Auth::user()!="")
	        {
				$cart->user_id = Auth::user()->id;
			}
			else
			{
				$users_id=$request->input('users_id');	
				Session::put('comman_user_id',rand(0,9999));
				$userssss_id =Session::get('comman_user_id');
				$cart->user_id = $userssss_id;			
			}
			if(Auth::user()!="")
			{
				$cart->login_type = "Login";
				
			}
			else
			{
				$cart->login_type = "Guest";
			}
			$cart->save();
		}
		else
		{
			
			$cartadd->product_id = $request->input('pro_id');
	        $cartadd->quantity = $request->input('quantity')+$cartadd->quantity;
	        $cartadd->total_price = $request->input('price');
	        $cartadd->cuts_type = $request->input('type');
	        $cartadd->type = $request->input('price_type');
	        if(Auth::user()!="")
	        {
				$cartadd->user_id = Auth::user()->id;
			}
			else
			{
				$users_id=$request->input('users_id');	
				Session::put('comman_user_id',rand(0,9999));
				$userssss_id =Session::get('comman_user_id');
				$cartadd->user_id = $userssss_id;			
			}
			if(Auth::user()!="")
			{
				$cartadd->login_type = "Login";
			}
			else
			{
				$cartadd->login_type = "Guest";
			}
			$cartadd->save();
		}
			$productId=$request->pro_id;
			$productById=DB::table('product')
					->join('productimages','productimages.product_id','=','product.id')
					->where('productimages.main_image',1)
					->where('product.id',$productId)
					->first();	
					
	    $cartItems =Cart::add(['id' => $productId, 'name' => $productById->productname, 'qty' => $request->quantity, 'price' => $request->price,'options' => ['image' => $productById->product_image,'type' => $request->type,'price_type' => $request->price_type]]);    

		$c_count =Cart::count();
		$response = array(
                'status' => 'success',
                'count' => $c_count,
            );
       
	return response()->json($response);
	}
	
	public function cartview() 
    {
		 if(Auth::user()!="")
	     {
				$UserId = Auth::user()->id;
		 }
		 else
		 {
				$UserId = Session::get('comman_user_id');		
		 }
		
		$cartlists = DB::table('cart')
	    ->join('productcategories','productcategories.product_id','=','cart.product_id')   
	    ->join('category','category.id','=','productcategories.cat_id')
		->where('cart.user_id',$UserId)
	    ->get();
	   
	   
		$cartItems=Cart::content();
		return view('site/cart')->with('cartItems',$cartItems)->with('cartlists',$cartlists);
	}
	
	public function wishlistadd(Request $request) 
    {
		
		$productId=$request->pro_id;
		$products=DB::table('myfavorites')
				->join('product','product.id','=','myfavorites.product_id')
				->join('productimages','productimages.product_id','=','product.id')
				->where('productimages.main_image',1)
				->where('myfavorites.product_id',$productId)
				->first();	
        

				$cartItems =Cart::add(['id' => $productId, 'name' => $products->productname, 'qty' => '1', 'price' => $products->price,'options' => ['image' => $products->product_image,'totals' => $request->quantity*($request->price)]]);   			
		return redirect()->back();
	}
	
	public function remove(Request $request) 
    {
		$rowId=$request->id;
		$pr = Cart::remove($rowId);
             return redirect()->back();  
	}
	public function update(Request $request)
	{
		  if(@count(Cart::content()) !=0)
          {           
              foreach($request->quantity as $key=>$value)
              {   
	                foreach($value as $keys=>$val)
	                { 
	                    Cart::update($key,$val);
	                }
              }  
                return redirect()->back();  
          }
	}

	public function checkout() 
    {
		if(Auth::user()!="")
	    {
			$user_id = Auth::user()->id;
	    }
	    else
	    {
			$user_id = '0';
		}
		$productItem=Cart::content();
		$users=DB::table('users')->where('users.id',$user_id)->first();
		$userDetail=DB::table('users')
					->join('useraddress','useraddress.user_id','=','users.id')
					->where('users.id',$user_id)->first();
		foreach($productItem as $product)
		{
			$cartId = DB::table('cart')->where('product_id',$product->id)->pluck('user_id');
		}	
		return view('site/checkout')->with('productItem',$productItem)->with('userDetail',$userDetail)->with('users',$users)->with('cartId',$cartId);
	}
    
	public function profile()
	{
       if(count(Auth::user()) !=0)
        {
	  		 $userId=Auth::user()->id;
   
			if(Auth::user()!="")
		    {
				$user_id = Auth::user()->id;
			}
		    else
		    {
				$user_id = '0';
			}
		 	$users = DB::table('users')->where('users.id',$user_id)->first();
		
         	$userDetail = DB::table('users')
               ->join('useraddress','useraddress.user_id','=','users.id')
               ->where('users.id',$user_id)           
               ->first(); 
            
		 return view('profile')->with('users',$users)->with('userDetail',$userDetail)->with('users',$users);     
		}
		else
		{
	
			return redirect('/');
       	}  
    }

    public function updates(Request $request)
    {	 
		if(count(Auth::user()) !=0)
         {
		        
		$data= User::find(Auth::user()->id);
	
  
		$id=Auth::user()->id;	
		$fname= ($request->input('fname')) ? $request->input('fname') : $data->user_first_name;
		
		
		$lname=$request->input('lname');
	    $optradio1=$request->input('optradio1');
	   
	   
		$dob=$request->input('dob');
		$coname=$request->input('coname');
		
		$email=$request->input('email');
	   $password=$request->input('password');
	
	  $cpassword=$request->input('cpassword');
		$image = $request->file('file-input'); 
		
$picture_single='';
	if($request->hasFile('file-input'))
	{
	   
			$filename = $image->getClientOriginalName();
			$fileExtension = $image->getClientOriginalExtension();
			$picture_single = date('His').$filename;
			$destinationPath = 'public/uploads/user_image';
            $image->move($destinationPath, $picture_single);
      }
      else
      {
		  $picture_single=$data->user_profile_image;
	  }
   	  if($password !='')
	  {
		  $var = bcrypt($cpassword);
	  }
	  else
	  {
		  $var = $data->password;
	  }
	  
	$data= User::where('id', $id)->update(['user_first_name' => $fname,'user_last_name' =>$lname,'gender' =>$optradio1,'companyname' =>$coname,'user_email' => $email,'password' => ($var),'password_confirmation' =>  ($var),
        'user_profile_image' => $picture_single,'user_dob' => $dob]);

		$userId=Profil::where('user_id',$id)->delete();
		$useraddress = new Profil;
		$useraddress->user_id=Auth::user()->id;
                $useraddress->door=$request->input('door');
		$useraddress->address=$request->input('street');
		$useraddress->city=$request->input('city');
		$useraddress->state=$request->input('state');
		$useraddress->country=$request->input('country');
		$useraddress->pincode=$request->input('pincode');
		$useraddress->phone=$request->input('phone');
		$useraddress->save();
		
		$user_id=Auth::user()->id;
		$userDetail = DB::table('users')
              ->join('useraddress', 'useraddress.user_id', '=', 'users.id')             
              ->where('users.id',$user_id )             
              ->first(); 
				$userId=Auth::user()->id;
			   $users =  DB::table('users')
			  ->where('users.id',$userId)
			  ->get();
	
		 return view('profile')->with('users',$users)->with('userDetail',$userDetail);	
	}
else
	{
	
	return redirect('/');
       }
       }
	
	public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function redirectToCallback()
    {
		try 
		{
            $fb_user = Socialite::driver('facebook')->user();
        }  
        
        catch (Exception $e) 
        {
            return redirect('auth/facebook');
		}
        //~ dd($fb_user);
        $authuser=$this->FbcreateUser($fb_user);
        Auth::login($authuser,true);
        return redirect('/');
    }
      
	public function FbcreateUser($fb_user) 
	{
		//~ dd($fb_user);
		$authuser = User::where('user_fb_id',$fb_user->id)->first();
		if($authuser)
		{
			return $authuser;
		}
		return User::create([
		'user_first_name' => $fb_user->name,
		'user_fb_id' => $fb_user->id,
		'user_email' => $fb_user->email,
		'user_profile_image' => $fb_user->avatar,
		'remember_token' => $fb_user->token,
		]);
	}
	public function redirectToGoogle()
    {
		//~ dd($request->all());
        return Socialite::driver('google')->redirect();
    }
    
    public function callback()
    {
		try 
		{
            $user = Socialite::driver('google')->user();
        }  
        catch (Exception $e) 
        {
            return redirect('auth/google');
		}
        
        $authuser=$this->createUser($user);
        Auth::login($authuser,true);
        return redirect('/');
      }
      
	public function createUser($user)
	{
		//~ dd($user->token);
		$authuser = User::where('user_gplus_id',$user->id)->first();
		if($authuser)
		{
			return $authuser;
		}
		return User::create([
		'user_first_name' => $user->name,
		'user_gplus_id' => $user->id,
		'user_email' => $user->email,
		'user_profile_image' => $user->avatar,
		'remember_token' => $user->token,
		]);
	}
	public function thankyou(Request $request)
	{
		 //dd($request->all());
		if(Auth::user()==""){
	       $users=new Users;
	       $users->user_first_name = $request->input('firstname');
	        $users->user_last_name = $request->input('lastname');
			$users->user_email = $request->input('email');
			$users->password = "Null";
			$users->user_salt = "Null";
			$users->password_confirmation = "Null";
			$users->user_phone_number = $request->input('phone');
			$users->user_community = "Null";
			$users->user_profile_image = "Null";
			$users->user_referal_code = "Null";
			$users->user_refered_by = "Null";
			$users->user_fb_id = "Null";
			$users->user_gplus_id = "Null";
			$users->user_twitter_id = "Null";	
			$users->companyname = $request->input('companyname');
			$users->gender = "Null";
			$users->user_location = "Null";
			$users->user_lastlogin = "Null";
			$users->user_createddatetime = "Null";
			$users->user_updatedatetime = "Null";
			$users->user_status = "Null";
			$users->user_type = "Null";
			$users->user_devices_id = "Null";
			$users->remember_token = "Null";
		    $users->save();

		    $lastuserid=$users->id;

			$checkout=new Order;
			$checkout->order_type = "WEB";	    	
			$checkout->user_id = $lastuserid;
			$checkout->order_cost = str_replace(',', '', $request->input('total'));
			$checkout->address = $request->input('address');
			$checkout->city = $request->input('city');
			$checkout->country = $request->input('country');
			$checkout->phone = $request->input('phone');
			$checkout->payment_mode = $request->input('payment');
			$checkout->save();
			$lastorderid=$checkout->id;
		}
		else { 
			$checkout=new Order;
			$checkout->order_type = "WEB";
			$checkout->user_id = Auth::user()->id;	    	
			$checkout->order_cost = str_replace(',', '', $request->input('total'));
			$checkout->address = $request->input('address');
			$checkout->city = $request->input('city');
			$checkout->country = $request->input('country');
			$checkout->phone = $request->input('phone');
			$checkout->payment_mode = $request->input('payment');
			
			$checkout->save();
			$lastorderid=$checkout->id;

		}
		 return view('site/thankyou')->with('orederid',$lastorderid);
	}
}
