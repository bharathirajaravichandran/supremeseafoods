<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['catname','catdescription','cat_image'];
    
    protected $table='category';

    

}
