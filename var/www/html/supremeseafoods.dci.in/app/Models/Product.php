<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use AdminBuilder;

    protected $table = 'product';

	public function productimages()
    {
        return $this->hasMany(Productimages::class);
    }


    
}
