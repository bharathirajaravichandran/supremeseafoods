<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Pincodes extends Model
{
    use AdminBuilder; 

    protected $table = 'pincodes';
     public $timestamps = false;

    public function hub()
    {
        return $this->belongsTo(Hub::class,"id");
    }
}
