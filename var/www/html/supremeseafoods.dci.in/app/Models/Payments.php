<?php
namespace App\Models;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    use AdminBuilder;

    protected $table = 'payments';
     public $timestamps = false;
     
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function paymentmode()
    {
        return $this->belongsTo(PaymentModes::class,"payment_mode");
    }
    
        
}
