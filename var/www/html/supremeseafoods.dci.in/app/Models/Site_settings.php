<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Site_settings extends Model
{
    use AdminBuilder;  
    protected $table = 'site_settings'; 
}
