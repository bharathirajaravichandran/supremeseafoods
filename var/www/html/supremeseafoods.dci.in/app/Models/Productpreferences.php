<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Productpreferences extends Model
{
    use AdminBuilder;

    protected $table = 'productpreference';
}
