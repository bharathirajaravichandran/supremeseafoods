<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Productimages extends Model
{
    use AdminBuilder;

    protected $table = 'productimages';

	public function product()
    {
        return $this->belongsTo(Product::class);
    }    
    
 
    
}
