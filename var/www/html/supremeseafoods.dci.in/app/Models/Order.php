<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use AdminBuilder;
    public $timestamps = false;
    
    

    protected $table = 'order';

    public function user()
    {
       return $this->belongsTo(Users::class);
    }
    public function orderdetail()
    {
        return $this->belongsTo(Orderdetail::class);
    }
   
}
