<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
class Notification extends Model
{

    use AdminBuilder;

    protected $table = 'notifications';
    public $timestamps = false;
	
	public function user()
    {
        return $this->belongsTo(Users::class);
    }
    
   public function setUsersAttribute($users)
   {
	   if (is_array($users)) 
	   {
			$this->attributes['users'] = implode(',', $users);
		}
	}       
	
}
