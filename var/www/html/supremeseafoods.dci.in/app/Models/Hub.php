<?php
namespace App\Models;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Hub extends Model
{
    use AdminBuilder;

    protected $table = 'hub';
     public $timestamps = false;
     
    public function pincode()
    {
        return $this->belongsTo(Pincode::class);
    }
    
}
