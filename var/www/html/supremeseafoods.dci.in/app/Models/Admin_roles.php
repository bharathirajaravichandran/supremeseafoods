<?php

namespace App\Models;


use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Admin_roles extends Model
{
    public $timestamps = false;
    protected $table = 'admin_roles';
}
