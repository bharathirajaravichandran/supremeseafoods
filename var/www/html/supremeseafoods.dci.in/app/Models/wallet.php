<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class wallet extends Model
{
     use AdminBuilder; 

    protected $table = 'wallet';
     public $timestamps = false;
}
