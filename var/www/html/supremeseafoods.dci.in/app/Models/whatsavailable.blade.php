 <section class="content">

                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
       <h3 class="box-title">Report</h3>

        <div class="box-tools">
     <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
 </div>
        </div>
    </div>
    
       <form id="prod_from" action="{{ url('/') }}/admin/ReportGen" method="post"  class="form-horizontal" >
 <div class="box-body">
 <div class="fields-group">
	                <div id="start_date" class="form-group">
                       <label for="StartDate" class="col-sm-2 control-label">Date</label>
                      <div id="startdate_inner" class="col-sm-8">
                        <div class="input-group fromdate">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input style="width: 110px" type="text" id="FromDate" class="FromDate" name="StartDate" value="" class="form-control StartDate" placeholder="Input Start Date" />
                        </div>
                     </div>
                    </div>

                    	<div id="district" class="form-group  ">
			<label for="DistrictID" class="col-sm-2 control-label">Hub</label>
			<div id="district_inner" class="col-sm-8">
				<input type="hidden" name="DistrictID"/>
				<select class="form-control DistrictID" style="width: 100%;" name="DistrictID"  id="DistrictID">
				<option value="">Please select district</option>
					@foreach ($hub as $k=>$v)
								<option value="{{ $k }}">{{ $v }}</option>
					@endforeach
				</select>
			</div>
		</div>

                  
            
                  
                 
         <div class="box-footer">
				 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
            <div class="col-md-8">
					<div class="btn-group pull-right" >
                   		<button type="submit" name="submit" onclick="validation();" id="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
             		 </div>

                <div class="btn-group pull-left">
                    <button type="reset" onclick="location.reload();" class="btn btn-warning">Reset</button>
                 </div>

            </div>
         </div>
     </div>
 </div>
</form>


	</section>
	<script>
		$(function () {
		$('.FromDate').datetimepicker({"format":"YYYY-MM-DD","locale":"en"});
	});
	</script>

 

