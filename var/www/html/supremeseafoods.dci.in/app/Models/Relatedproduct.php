<?php

namespace App\Models;



use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Relatedproduct extends Model
{
  public $timestamps = false;
    protected $table = 'relatedproduct';
}
