<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	
 public $timestamps = false;
    protected $table = 'state';
      public function getPicturesAttribute($pictures)
    {
        if (is_string($pictures)) {
            return json_decode($pictures, true);
        }
        return $pictures;
    }
    public function setPicturesAttribute($pictures)
    {
        if (is_array($pictures)) {
            $this->attributes['pictures'] = json_encode($pictures);
        }
    }
}
