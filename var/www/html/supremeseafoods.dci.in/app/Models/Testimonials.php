<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    use AdminBuilder;

    protected $table = 'testimonials';

    public function user()
    {
        return $this->belongsTo(Users::class);
    }
}
