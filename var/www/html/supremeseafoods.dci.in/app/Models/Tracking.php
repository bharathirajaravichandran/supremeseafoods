<?php
namespace App\Models;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    use AdminBuilder;

    protected $table = 'ordertracking';
     public $timestamps = false;
     
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    
}
