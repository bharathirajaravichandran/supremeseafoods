<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Wholesaleenquiry extends Model
{
    use AdminBuilder;

    protected $table = 'wholesaleenquiry';
	public function user()
    {
        return $this->belongsTo(Users::class);
    }       
	//~ public function product()
    //~ {
        //~ return $this->belongsTo(Product::class);
    //~ }       
}
