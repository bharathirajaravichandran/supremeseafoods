<?php
namespace App\Models;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class PaymentModes extends Model
{
    use AdminBuilder;

    protected $table = 'paymentmodes';
     public $timestamps = false;
     
     public function payments()
    {
        return $this->belongsTo(Payments::class);
    }
}
