<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Preferences extends Model
{
    use AdminBuilder;

    protected $table = 'preferences';
}
