<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;


class Whatsavailable extends Model
{
    use AdminBuilder;

    protected $table = 'whatsavailable';
	public function user()
    {
        return $this->belongsTo(Users::class);
    }       
	public function product()
    {
        return $this->belongsTo(Product::class);
    }       
	public function hub()
    {
        return $this->belongsTo(Hub::class);
    }
      public function category()
    {
        return $this->belongsTo(Category::class);
    }       
}
