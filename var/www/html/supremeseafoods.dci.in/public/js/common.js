function productdetails_page() {
$("#zoom_03").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: ''}); 
//pass the images to Fancybox
$("#zoom_03").bind("click", function(e) {  
  var ez =   $('#zoom_03').data('elevateZoom');
$.fancybox(ez.getGalleryList());
  return false;
});

$("#zoom_03").bind('touchstart', function(){
$("#zoom_03").unbind('touchmove');
});



}

function product_page() {


// May 8

var windowsize = $(window).width();

if (windowsize < 767) {
//alert("hai");
jQuery('.bxslider').bxSlider({
mode: 'horizontal',
auto: false,
autoControls: false,
controls: true,
moveSlides: 1,
slideMargin: 40,
infiniteLoop: true,
slideWidth: 110,
minSlides: 3,
maxSlides: 3,
adaptiveHeight: false,
speed: 800,
nextText: '<span class="fa fa-angle-right"></span>',
pager: false,
prevText: '<span class="fa fa-angle-left"></span>',
});

} else {
//alert("The paragraph was clicked.");
jQuery('.bxslider').bxSlider({
mode: 'vertical',
auto: false,
autoControls: false,
controls: true,
moveSlides: 1,
slideMargin: 40,
infiniteLoop: true,
slideWidth: 110,
adaptiveHeight: false,
minSlides: 3,
maxSlides: 3,
speed: 800,
nextText: '<span class="fa fa-angle-right"></span>',
pager: false,
prevText: '<span class="fa fa-angle-left"></span>',
});
}
}

jQuery(document).ready(function(){


 //$('nav a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');


//may .17
jQuery(".carbs_whole").click(function(){
    jQuery(".carbs_whole").addClass("add");
    jQuery(".carbs_cleaned").removeClass("add1");
});
jQuery(".carbs_cleaned").click(function(){
    jQuery(".carbs_cleaned").addClass("add1");
    jQuery(".carbs_whole").removeClass("add");
});

////Add this jQuery also 25.apr//
jQuery(".incr-btn").on("click", function (e) {
var $button = $(this);
var oldValue = $button.parent().find('.quantity').val();
$button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
if ($button.data('action') == "increase") {
var newVal = parseFloat(oldValue) + 1;
} else {
// Don't allow decrementing below 1
if (oldValue > 1) {
var newVal = parseFloat(oldValue) - 1;
} else {
newVal = 1;
$button.addClass('inactive');
}
}
$button.parent().find('.quantity').val(newVal);
e.preventDefault();
});
//
jQuery('#horizontalTab').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion           
width: 'auto', //auto or any width like 600px
fit: true,   // 100% fit in a container
closed: 'accordion', // Start closed if in accordion view
activate: function(event) { // Callback function if tab is switched
var $tab = $(this);
var $info = $('#tabInfo');
var $name = $('span', $info);
$name.text($tab.text());
$info.show();
}
});
//

////
jQuery('#owl_carousel_product').owlCarousel({
loop:true,
margin:10,
nav:true,
mouseDrag: true,
touchDrag: false,
responsive:{
0:{
items:1,
autoHeight: true,
mouseDrag: false,
touchDrag: true
},
600:{
items:3,
autoHeight: true,
mouseDrag: false,
touchDrag: true
},
1000:{
items:4,
autoHeight: true,
mouseDrag: false,
touchDrag: true
}
}
});
//
jQuery('#owl_carousel_homepage').owlCarousel({
loop:true,
margin:10,
nav:true,
mouseDrag: true,
touchDrag: false,
responsive:{
0:{
items:1,
autoHeight: true,
mouseDrag: false,
touchDrag: true
},
600:{
items:3,
autoHeight: true,
mouseDrag: false,
touchDrag: true
},
1000:{
items:5,
autoHeight: true,
mouseDrag: false,
touchDrag: true
}
}
});




if($("#zoom_03").length > 0) {
productdetails_page();
}

if($(".bxslider").length > 0) {
product_page();
}



});
