   
    
    <section class="content">
                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">&nbsp;</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/wholesaleenquiry" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->



	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<tr>
				<th>Product Image</th>
				<th>Product Name</th>
				<th>Whole Price</th>
				<th>Cleaned Price</th>
				<th>Quantity</th>
			</tr>
			@foreach ($proddetails as $k=>$v)
			<tr >
				<td >
						<img src="{{ url('/') }}/uploads/public/product_images/{{$v['productimage']}}" style="max-width:200px;max-height:200px" class="img img-thumbnail">	
				</td>
				<td>
						<p>{{$v['productname']}}</p>
				</td>
				<td>
						<p>&#8377;{{$v['product_whole_price']}}</p>
				</td>
				<td>
						<p>&#8377;{{$v['product_cleaned_price']}}</p>
				</td>
				<td>
						<p>{{$v['quantity']}} Kg(s)</p>
				</td>
			</tr>
			@endforeach


		</table>
	</div>

        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

            </div>

        </div>

</div>

</div></div>

    </section>

<script>

function validation(cnt)
{
	var error='';
	for(i=0;i<=cnt;i++)
	{
		var prod_qty=$('#productname'+cnt).val();
		prod_qty=prod_qty.trim();
		if(prod_qty=='')
		{
			$('#prod_name'+i).addClass('has-error');
			$('#prod_name_inner'+i).prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Quantity is required.</label>');
			error='1';
		}
	}
	if(error=='')
	{
		$('#prod_from').submit();
	}
	else
	{
		return false;
	}
}


</script>
