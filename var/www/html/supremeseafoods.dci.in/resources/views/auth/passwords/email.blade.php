@include('header')
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Account</h2>
						<h5>Home > Reset Password</h5>
					</div>
				</div>		
			</div>
		</div>

<div class="Login_section">
<div class="container">
<div class="row">
<div class="Login_content clearfix">
<h3>Reset Password</h3>

<!--Error-->

@if (count($errors) > 0)
   <div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
   </div>
@endif
 @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
<!-- login -->

	{!! Form::open(['url' => 'password.email','method' => 'post']) !!}
<div class="form-group">

  {!!  Form::text('user_email',null , array('class' => 'form-control','placeholder'=>'Email','id'=>'user_email')) !!}

</div>

<div class="form-group pull_left">
<button type="submit" class="btn login_btn pull-left">Send Password Reset Link</button>
</div>
{!! Form::close() !!}	
</div>
</div>	
</div>
</div>
@include('footer')
