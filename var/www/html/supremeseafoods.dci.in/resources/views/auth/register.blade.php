@include('header')

<!-- head top bg -->
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Account</h2>
						<h5>Home > Register</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Login -->
		<div class="Login_section">
			<div class="container">
				<div class="row">
					<div class="Login_content clearfix">
						<h3>Create Account</h3>
						<h5>Sign in with a simple click:</h5>
						<ul class="social_icons">
							<li><a href="facebook.com"><span class="fa fa-facebook"></span></a></li>
							<li><a href="twitter.com"><span class="fa fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
							
						</ul>
						<p class="or">(OR)</p>
						<form method="POST" action="{{ route('register') }}" name="myform" id="myform">
							{{ csrf_field() }}
						<div class="form-inline">
							
							<div class="form-group">							
							  <input type="text" class="form-control" name="user_first_name" placeholder="First Name" >
							   @if ($errors->has('user_first_name'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('user_first_name') }}</strong>
                                    </span>
                                @endif
							</div>
							<div class="form-group form-group-second">							 
							  <input type="text" class="form-control" id="user_last_name" name="user_last_name"  placeholder="Last Name">
							  @if ($errors->has('user_last_name'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('user_last_name') }}</strong>
                                    </span>
                                @endif
							</div>
						</div>
						<div class="form-group">
						  <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Email">
						  @if ($errors->has('email'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
						</div>
						<div class="form-group">
						  <input type="password" class="form-control" id="password" name="password" placeholder="Password">
						  @if ($errors->has('password'))
                                    <span class="alert-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
						</div>
						 <input type="hidden" class="form-control" id="status" name="status" value="1">
						<div class="form-group">
						  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Conform Password"  >
						 
						</div>
						<div class="form-group">
							<button type="submit" class="btn login_btn">Create</button>							
						</div>
						
					</div>
					</form>
				</div>		
			</div>
		</div>
<!-- //Login -->	 
	<script src="/js/validate.js"></script>

<script type="text/javascript">

    $('#myform').validate({ // initialize the plugin
        rules: {

            user_first_name: {
                required: true,
               
            }, 
            user_last_name: {
                required: true,
                
            }, 
            user_email: {
                required: true,
                email: true
            },
            
            password: {
                required: true,               
            },
            password_confirmation: {
                required: true,              
            }
        }
    });
</script>
@include('footer')
