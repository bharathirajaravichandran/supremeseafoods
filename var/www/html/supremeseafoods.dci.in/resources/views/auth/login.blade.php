@include('header')

<!--
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
-->
<!-- head top bg -->
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Account</h2>
						<h5>Home > Account</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Header -->

<!-- Login -->
<div class="Login_section">
<div class="container">
<div class="row">
<div class="Login_content clearfix">
<h3>Login</h3>
<h5>Sign in with a simple click:</h5>
<ul class="social_icons">
<li><a href="{{url('auth/facebook/callback')}}"><span class="fa fa-facebook"></span></a></li>
<li><a href="twitter.com"><span class="fa fa-twitter"></span></a></li>
<li><a href="{{url('auth/google')}}"><span class="fa fa-google-plus"></span></a></li>
</ul>
<p class="or">(OR)	</p>

<!--Error-->

<!-- login -->
	{!! Form::open(['url' => 'custom','id' => 'myform','method' => 'post','name'=>'myform']) !!}
<div class="form-group">

  {!!  Form::text('email',null , array('class' => 'form-control','placeholder'=>'Email')) !!}
  
</div>
<div class="form-group">

{!!  Form::password('user_password',['class' => 'form-control','placeholder'=>'Password']) !!}
@if(Session::has('adminusername'))
	<span style="color:red;">
			<strong>{{ Session::get('adminusername') }}</strong>
		</span>
@endif
</div>	

<div class="form-group pull_left">
<button type="submit" class="btn login_btn pull-left">Login</button>
<a href="{{ route('password.request') }}" class="forget_btn pull-right">Forget your password?</a>
</div>
{!! Form::close() !!}	
<div class="form-group ">
<a href="{{ url('register') }}" class="btn creat_btn">Create Account</a>
</div>
</div>
</div>	
</div>
</div>
<!-- //Login -->	 

<!-- Footer -->
				<script src="/js/validate.js"></script>
<script type="text/javascript">

    $('#myform').validate({ // initialize the plugin
        rules: {

            email: {
                required: true,
                email: true
            },
            
            user_password: {
                required: true,               
            },
        }
    });
</script>

@include('footer')
