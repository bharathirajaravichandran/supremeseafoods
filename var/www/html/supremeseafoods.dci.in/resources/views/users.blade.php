<section class="content">

                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/users"  class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
            <form action="{{ url('/') }}/admin/save" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data"  id="myforms" pjax-container>
    
 <div class="box-body">
      <div class="fields-group">
         <div class="form-group  " id="users_name">
            <label for="user_first_name" class="col-sm-2 control-label">First Name</label>
          <div class="col-sm-8" id="user_name_inner">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                 <input type="text" id="first_name" name="user_first_name" value="" class="form-control user_first_name" placeholder="Input First Name" />  <span id="nam" style="color:red"></span>
             </div>
          </div>
       </div>
     <div class="form-group  " id="last_name">
        <label for="user_last_name" class="col-sm-2 control-label">Last Name</label>
       <div class="col-sm-8"  id="last_name_inner">
         <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
             <input type="text" id="last_names" name="user_last_name" value="" class="form-control user_last_name" placeholder="Input Last Name" />  <span id="lam" style="color:red"></span>
         </div>
      </div>
     </div>
    <div class="form-group  " id="email_id">
        <label for="user_email" class="col-sm-2 control-label">Email</label>
       <div class="col-sm-8" id="email_id_inner">
         <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
           <input type="text" id="email" name="user_email" value="" class="form-control user_email" placeholder="Input Email" />
             <span id="eml" style="color:red"></span> </td>
         </div>
        </div>
      </div>
     <div class="form-group  " id="password_id">
        <label for="password" class="col-sm-2 control-label">Password</label>
       <div class="col-sm-8" id="password_id_inner">
          <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-eye-slash fa-fw"></i></span>
             <input type="password" id="pass" name="password" value="" class="form-control password" placeholder="Input Password" />
             <span id="pass" style="color:red"></span></td>
         </div>
       </div>
      </div>
     <div class="form-group  " id="comfirm_password">
        <label for="password_confirmation" class="col-sm-2 control-label">Confirm Password</label>
       <div class="col-sm-8" id="confirm_pasword_inner">
          <div class="input-group">
             <span class="input-group-addon"><i class="fa fa-eye-slash fa-fw"></i></span>
             <input type="password" id="cfpassword" name="password_confirmation" value="" class="form-control password_confirmation" placeholder="Input Confirm Password" />
             <span id="cass" style="color:red"></span></td>
          </div>
        </div>
     </div>
    <div class="form-group  " id="mob_num">
         <label for="user_phone_number" class="col-sm-2 control-label" id>Phone Number</label>

    <div class="col-sm-8" id="mob_num_inner">
	  <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
              <input type="text" id="phone_number" name="user_phone_number" value="" class="form-control user_phone_number" placeholder="Input Phone Number" />
            <span id="phon" style="color:red"></span>
       </div>
      </div>
    </div>
    <div class="form-group" id="image_id">
        <label for="user_profile_image" class="col-sm-2 control-label">Profile Image</label>
       <div class="col-sm-8" id="image_id_inner">
         <input type="file" class="user_profile_image" name="user_profile_image" id="proimage"  />
         <span id="pict" style="color:red"></span></td>
         </div>
     </div>
    <div class="form-group  ">
       <label for="user_community" class="col-sm-2 control-label">Religion</label>
      <div class="col-sm-8">
	       <input type="hidden" name="user_community"/>
	       <select class="form-control user_community" style="width: 100%;" name="user_community"  >
	                            <option value=""></option>
	                                    <option value="1" >Muslim</option>
	                                    <option value="0" selected>Hindu</option>
	                                    <option value="2" >Christian</option>
	                                    </select>
      </div>
   </div>
   <div class="form-group  ">
      <label for="user_status" class="col-sm-2 control-label">Status</label>
    <div class="col-sm-8">
       <input type="hidden" name="user_status"/>
       <select class="form-control user_status" style="width: 100%;" name="user_status"  >
                            <option value=""></option>
                                    <option value="1" >Active</option>
                                    <option value="0" selected>In Active</option>
                                    </select>
      </div>
    </div>
</div>
 </div>
        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <div class="btn-group pull-right">
    <button type="button" name="submit" class="btn btn-info pull-right"  onclick="validation();" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
    </div>
              <div class="btn-group pull-left">
    <button type="reset" class="btn btn-warning">Reset</button>
</div>
                

            </div>

        </div>

        
        <!-- /.box-footer -->
    </form>
</div>

</div></div>
 </div>
    </section>
    
    <script>
		function validation()
		{
	
	var user_name=$('#first_name').val().trim();

    //user_name=user_name.trim();

	var err='';
	if(user_name=='')
	{
		$('#users_name').addClass('has-error');
		$('#user_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The user first Name field is required.</label>');
		if(err=='')
		{
			$('#first_name').focus();
			err='set';
		}
	}
		//////////////////////last name////////////////////////////////////	
	var lastname=$('#last_names').val();
	lastname=lastname.trim();
	//var err='';
	if(lastname=='')
	{
		$('#last_name').addClass('has-error');
		$('#last_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The last name field is required.</label>');
		if(err=='')
		{
			$('#last_names').focus();
			err='set';
		}
	}
		//////////////////////email////////////////////////////////////	
	var email_ids=$('#email').val();
	email=email_ids.trim();
	//var err='';
	if(email=='')
	{
		$('#email_id').addClass('has-error');
		$('#email_id_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The email field is required.</label>');
		if(err=='')
		{
			$('#email').focus();
			err='set';
		}
	}
		//////////////////////password////////////////////////////////////	
	var password=$('#pass').val();

	//var err='';
	if(password=='')
	{
		$('#password_id').addClass('has-error');
		$('#password_id_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The password field is required.</label>');
		if(err=='')
		{
			$('#pass').focus();
			err='set';
		}
	}
		if(password!='')
	{
		if(((password.length) < 4) || ((password.length)>10))
		{
			$('#password_id').addClass('has-error');
			$('#password_id_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The password  shoule be minimum Of 4 characters and a maximum Of 10 characters.</label>');
			if(err=='')
			{
				$('#pass').focus();
				err='set';
			}
		}
	}
			//////////////////////confirm password////////////////////////////////////	
	var cpassword=$('#cfpassword').val();

	//var err='';
	if(cpassword=='')
	{
		$('#comfirm_password').addClass('has-error');
		$('#confirm_pasword_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The confirm password field is required.</label>');
		if(err=='')
		{
			$('#cfpassword').focus();
			err='set';
		}
	}
		if(password!=cpassword)
	{
			$('#comfirm_password').addClass('has-error');
			$('#confirm_pasword_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The password and confirm password  shoule be same</label>');
			if(err=='')
			{
				$('#cfpassword').focus();
				err='set';
			}
		}
			//////////////////////phone number////////////////////////////////////	
	var mnum=$('#phone_number').val();

	//var err='';
	if(mnum=='')
	{
		$('#mob_num').addClass('has-error');
		$('#mob_num_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The phone number field is required.</label>');
		if(err=='')
		{
			$('#phone_number').focus();
			err='set';
		}
	}
			if(mnum!='')
	{
		if(((mnum.length)!=10)  )
		{
			$('#mob_num').addClass('has-error');
			$('#mob_num_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The phone number  shoule be 10 numbers.</label>');
			if(err=='')
			{
				$('#phone_number').focus();
				err='set';
			}
		}
	}
			////////////////////image////////////////////////////////////	
	var iamge=$('#proimage').val();
	image=iamge.trim();		
	//var err='';
	if(iamge=='')
	{
		$('#image_id').addClass('has-error');
		$('#image_id_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The user image is required.</label>');
		if(err=='')
		{
			$('#proimage').focus();
			err='set';
		}
	}
	
		
		if(err!='')
	{
		return false;
	}
	else
	{
		$('#myforms').submit();
		
	}
}
</script>
        <script data-exec-on-popstate>

    $(function () {
                    $('.form-history-back').on('click', function (event) {
    event.preventDefault();
    history.back(1);
});
                    
$("input.user_profile_image").fileinput({"overwriteInitial":true,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":false,"showUpload":false,"deleteExtraData":{"user_profile_image":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},"deleteUrl":"http:\/\/http://127.0.0.1:8000\/admin\/","allowedFileTypes":["image"]});

                    $(".user_community").select2({"allowClear":true,"placeholder":"Religion"});
                    $(".user_status").select2({"allowClear":true,"placeholder":"Status"});
            });
</script>

   
