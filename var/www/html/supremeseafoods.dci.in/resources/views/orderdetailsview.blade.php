<?php 
// $ordersView = DB::table('orderdetail')->get();
// $ordersView = DB::table('orderdetail')->join('order','order.id','=','orderdetail.id')->get();
// echo "<pre>";
// $idArray=array();
// foreach ($ordersView as $key) {
// 	$idArray[]=$key;
// }
// print_r($key);
// exit();
?>
    <section class="content">
                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">&nbsp;</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/orders" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/orders" class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->



	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<tr>
				<th>Order ID</th>
				<th>Product Name</th>
				<th>Quantity</th>
				<th>Product Cost</th>
				<th>Type Of Cuts</th>
				<th> Created Date</th>
				<th>Action</th>
			</tr>

			@foreach ($orderdetails as $ordersViews)
			<tr >
				<td>
						<p>{{$ordersViews->order_id}}</p>
				</td>
				<td>
						<p>{{$ordersViews->productname}}</p>
				</td>
				<td>
						<p>{{$ordersViews->quantity}} Kg(s)</p>
				</td>
				<td>
						<p>&#8377;{{$ordersViews->product_cost}}</p>
				</td>
				<td>
						<p>{{$ordersViews->type_cuts}}</p>
				</td>
				<td>
						<p>{{$ordersViews->createddatetime}}</p>
				</td>
				<!-- <td>
						<a href="{{ url('/') }}/admin/orders" ><i class="fa fa-edit"></i></a>
				</td> -->
				<td>
				    <a href="/admin/orderdetails/{{$ordersViews->id}}/edit"><i class="fa fa-edit"></i></a>
				    <!-- <a href="" class="grid-row-delete"><i class="fa fa-trash"></i></a> -->
                </td>
				
			</tr>
			@endforeach


		</table>
	</div>

        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

            </div>

        </div>

</div>

</div></div>


    </section>

<script>

</script>
