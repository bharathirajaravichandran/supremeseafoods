
<!-- //footer -->
<div class="footer">
	<div class="footer-inner">
		<div class="container">
			<div class="_footer_grids">
				
				<div class="col-md-5 _footer_grid col-xs-12">
						<h1 class="footer-logo"><a href="{{url('/')}}"><img src="/images/supreme-logo.png" alt="Supreme Seafoods"></a></h1>
						<div class="about-para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
				</div>
<div class="col-md-4 _footer_grid col-xs-6">
					<h3>Information</h3>
					<div class="row">
					<ul class="info col-sm-6"> 
						<li><a href="#">About Us</a></li>
						<li><a href="#">Feedback</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">Press &amp; Media</a></li>
						<li><a href="#">Return Policy</a></li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
					<ul class="info col-sm-6"> 
							<li><a href="#">Terms &amp; Conditions</a></li>	
							<li><a href="#">Cookies Policy</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Wholesale Fish Supply</a></li>
							<li><a href="#">Payment Methods</a></li>
						</ul>
						</div>
				</div>
				<div class="col-md-3 _footer_grid col-xs-6">
						<h3>ADDRESS</h3>
						<ul class="address">
							<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Old No 123, New No. 255,Linghi Chetty Street, Parrys,
Chennai 600 001, Tamil Nadu, India.</span></li>
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone    : +91-44-66442222<br>Mobile    : +91-9171072896
</li>
<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:bill.supremeseafood@gmail.com">bill.supremeseafood@gmail.com</a></li>
						</ul>
					</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
		

	</div>	
	<div class="footer-botm">
			<div class="container">
				<div class="footer-copy col-sm-3">
					<p>Copyright 2018 © Supreme Seafood.</p>
				</div>
				<div class="layouts-foot col-sm-6 text-center">
					<ul>
						<li><a href="#" class="_agile_facebook"><img src="/images/fb.png"/></a></li>
						<li><a href="#" class="agile_twitter"><img src="/images/twitter.png"/></a></li>
						<li><a href="#" class="_agile_goo"><img src="/images/google.png"/></a></li>
					</ul>
				</div>
				<div class="payment-ls col-sm-3  text-right">	
					<img src="/images/payment-icons.png" alt=" " class="img-responsive">
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
<!-- //footer -->	
<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>

<!-- top-header and slider -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
<script src="/js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>

<!-- main slider-banner -->
<script src="/js/skdslider.min.js"></script>
<link href="/css/skdslider.css" rel="stylesheet">
<script src="http://kenwheeler.github.io/slick/slick/slick.js"></script>




<script src="/js/easy-responsive-tabs.js"></script>
<script src="/js/owl.carousel.js"></script>
<script src="/js/jquery.elevatezoom.js"></script>
<script src="/js/jquery.bxslider.js"></script>
<script src="/js/common.js"></script>





<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
						
			jQuery('#responsive').change(function(){
			  $('#responsive_wrapper').width(jQuery(this).val());
			});
			 jQuery("#search-button img").click(function(){
				jQuery(".l_search").fadeToggle();
			});
			jQuery('.owl-carousel').owlCarousel({
                items: 1,
                loop: true,
                margin: 10,
				autoplay: true,
                autoplayTimeout: 3000,
				nav: true,
                autoplayHoverPause: true
              });
		});
</script>	
<!-- //main slider-banner --> 
</body>
</html>

