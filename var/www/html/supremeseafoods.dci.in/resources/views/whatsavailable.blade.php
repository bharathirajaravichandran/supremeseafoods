
<style type="text/css">
.main-footer{
  display:none;
}
label.checkbox-label{
	position: relative;
	top: 8px;
	margin-right: 15px;
	padding: 0 !important
}
label.checkbox-label1{
	position: relative;
	top: -3px;
	margin-right: 5px;
	padding: 0 !important;
}

</style>

 <section class="content">

    <div class="row">
    	<div class="col-md-12">
    		<div class="box box-info">
    	<div class="box-header with-border">
       <h3 class="box-title">Whats available today</h3>

        <div class="box-tools">
     <div class="btn-group pull-right" style="margin-right: 10px">
                  <a class="btn btn-sm btn-default form-history-back" href="{{ url('/') }}/admin/whatsavailable"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
            </div>
        </div>
    </div>
    
    <form id="prod_from" action="{{ url('/') }}/admin/ReportGen" method="post"  accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container >
        <div class="box-body">
           <div class="fields-group">
	            <div id="start_date" class="form-group">
                    <label for="StartDate" class="col-sm-2 control-label">Date</label>
                    <div id="dates_inner" class="col-sm-8">
                        <div class="input-group fromdate">
                           <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                           <input style="width: 110px;height:30px;" type="text" id="FromDate" class="FromDate" name="StartDate" value="" class="form-control StartDate" placeholder="Input Start Date" />
                        </div>
                    </div>
                </div>

                <div id="hubs" class="form-group">
					<label for="hub" class="col-sm-2 control-label">Hub</label>
					<div id="hub_inner" class="col-sm-8">
						<select class="form-control hub" style="width: 100%;" name="hub"  id="hub" onchange="changes(this.value)">
						     <option value="">Please select Hub</option>
							@foreach ($hub as $k=>$v)
								<option value="{{ $k }}">{{ $v }}</option>
							@endforeach
						</select>
			        </div>
		        </div>
		        <div id="categorys" class="form-group ">
					<label for="category" class="col-sm-2 control-label">Category</label>
					<div id="category_inner" class="col-sm-8">
						<select class="form-control category" style="width: 100%;" name="category"  id="category" onchange="product(this.value)">
						     <option value="0">Please select category</option>
							@foreach ($category as $k=>$v)
								<option value="{{ $k }}">{{ $v }}</option>
							@endforeach
						</select>
			        </div>
		        </div>
		        <div id='TextBoxesGroup' class="form-group" style="display:none">
	            	   
						
			  	</div>


 				<div class="foot">
				    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="col-md-10" style="padding: 0">
					     <div class="btn-group pull-left">
					     	<button type="reset" onclick="location.reload();" class="btn btn-warning">Reset</button>
					     </div>
             		    <div class="btn-group pull-right">
                            
                            <button type="button" name="submit" onclick=" validation();"  class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
 </section>
	<script>
		$(function () {
		$('.FromDate').datetimepicker({"format":"YYYY-MM-DD","locale":"en"});
	});
    function changes(vals)
    {
      $('#category').val(0).trigger('change')
    }
		  function product(vals)
      {    	
          var hub=$('#hub').val();
          var dates=$('#FromDate').val();
        	var products=$('#category').val();
          err="";
        if(products != 0 && products != null)
        {
          if(dates=='')
          {
            $( "#dates_label" ).remove();
            
            $('#dates_inner').prepend('<label id="dates_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i> Date is required.</span></label>');
            if(err=='')
            {
              $('#FromDate').focus();
              err='set1';
            }
          }
          else
          {
            $( "#dates_label" ).remove();
          }     
          if(hub=='')
          {
            $( "#hub_label" ).remove();
            
            $('#hub_inner').prepend('<label id="hub_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i>select any one of the hub.</span></label>');
            if(err=='')
            {
              $('#hub').focus();
              err='set2';
            }
          }
          else
          {
            $( "#hub_label" ).remove();
          } 
          if(err!="")
          {
            return false;
          }
        	
   		   url = "{{ url('/') }}/admin/getproducts?dates="+dates+"&hub="+hub+"&products="+products; 

      // url = "{{ url('/') }}/admin/getpanchayat?dist_id="+districtid+"&const_id="+const_id+"&typeid="+typeid+"&punions="+vals; 
       
        	$.ajax({
   			type : "GET",
   			url : url,
   			beforeSend : function() {
   
   			},
   			success : function(data) {
   				$('#TextBoxesGroup').show();
   				$('#TextBoxesGroup').html(data);
   				
   				  
                }
           });
      }
    } 
         </script>
         <script>
    function validation()
   {
   	
   		var err='';
   		var dates=$('#FromDate').val();
   		var hub=$('#hub').val();
   		var category=$('#category').val();
   		var d_val=$('#d_value').val();
   		var i="", check="";
      var cur_date="{{$current_dates}}";

   	if(dates=='')
   	{
   		$( "#dates_label" ).remove();
   		
   		$('#dates_inner').prepend('<label id="dates_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i> Date is required.</span></label>');
   		if(err=='')
   		{
   			$('#FromDate').focus();
   			err='set1';
   		}
   	}
   	else
   	{
   		$( "#dates_label" ).remove();
      if(dates<cur_date)
      {
        $( "#dater_label" ).remove();
        $('#dates_inner').prepend('<label id="dater_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i> Select valid Date.</span></label>');

      }
      else
      {
         $( "#dater_label" ).remove();
      }
   	}  		
   	if(hub=='')
   	{
   		$( "#hub_label" ).remove();
   		
   		$('#hub_inner').prepend('<label id="hub_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i>select any one of the hub.</span></label>');
   		if(err=='')
   		{
   			$('#hub').focus();
   			err='set2';
   		}
   	}
   	else
   	{
   		$( "#hub_label" ).remove();
   	}  		

   	if(category==''|| category==0)
   	{
   		$( "#category_label" ).remove();
   		
   		$('#category_inner').prepend('<label id="category_label" class="control-label" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i> select any one of the Category.</span></label>');
   		if(err=='')
   		{
   			$('#FromDate').focus();
   			err='set3';
   		}
   	}
   	else
   	{
   		$( "#category_label" ).remove();
   	}
   	for(i=0;i<d_val;i++)
   	{      
   		$( "#price_div_label"+i ).remove(); 
   		$( "#time_label"+i ).remove();


   		if($('#cats_'+i).prop("checked") !="")
   		{
   			$( "#dummy_lab" ).remove();
             check=1;

   			if($('#price_'+i).val()=="")
   			{
   				$( "#price_div_label"+i ).remove();

   				var error="<label id=price_div_label"+i+" class='control-label' style='margin-left:30px;' for='inputError'><span style='color:red!important'><i class='fa fa-times-circle-o'></i> Price is required.</span></label>";

   		        $('#price_error_'+i).prepend(error);
   				if(err=='')
   				{
   					$('#price_'+i).focus();
   					err='set3';
   				}
   			}
   			else
   			{
   				$( "#price_div_label"+i).remove();
   			}
   			if($('#time_'+i).val()=="")
   			{
   				$( "#time_label"+i ).remove();
   				var error1="<label id=time_label"+i+" class='control-label' style='margin-left:30px;' for='inputError'><span style='color:red!important'><i class='fa fa-times-circle-o'></i> Delivery Time required.</span></label>";
   				$('#time_error_'+i).prepend(error1);
   				if(err=='')
   				{
   					$('#price_'+i).focus();
   					err='set3';
   				}

   			}
   			else
   			{
   				$( "#time_label"+i).remove();
   			}
   		}
   		
   	}
   	if(check=="")
   		{
   			//$("#prod_div_"+i)
   			$( "#dummy_lab" ).remove();
   		      err=1;
   				$('#dummy').prepend('<label id="dummy_lab" class="control-label" style="margin-left:30px;" for="inputError"><span style="color:red!important"><i class="fa fa-times-circle-o"></i> Select Atleast one product.</span></label>');
   		}

   	if(err!='')
   	{
   		//alert("hi");
   		
   	    	return false;


   	}
   	
   else if(err=='')
   	{
   		//alert(err);

   	    $('#prod_from').submit();
   	}
   
  
   }
  
	
    </script>
    <script>
    	
    		
    function checking()	
   {
   
   	
   	if ($('#checks').is(':checked')) {   		
   		var d_val=$('#d_value').val();
   		for(i=0;i<d_val;i++)
   		{      
   		
        $('#cats_'+i).prop('checked', true);
  		 }
    }
    else
    {
    	var d_val=$('#d_value').val();
   		for(i=0;i<d_val;i++)
   		{      
   		
        $('#cats_'+i).prop('checked', false);
  		 }
    }


   };
    	    </script>