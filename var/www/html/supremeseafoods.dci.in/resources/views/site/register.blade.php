<!-- Header -->

 @include('header')



     
<!-- Login -->
		<div class="Login_section">
			<div class="container">
				<div class="row">
					<div class="Login_content clearfix">
						<h3>Create Account</h3>
						<h5>Sign in with a simple click:</h5>
						<ul class="social_icons">
							<li><a href="facebook.com"><span class="fa fa-facebook"></span></a></li>
							<li><a href="twitter.com"><span class="fa fa-twitter"></span></a></li>
							<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
							<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
							<li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
						</ul>
						<p class="or">(OR)	</p>
						
						
					<!--Error-->
						
						 @if (count($errors) > 0)
							   <div class="alert alert-danger">
								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
								  <ul>
									 @foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									 @endforeach
								  </ul>
							   </div>
							@endif
						
						
						
						<div class="form-inline">
								{!! Form::open(['url' => 'registered','method' => 'post','name' => 'myform']) !!}
							<div class="form-group">
								{!!  Form::text('first_name', null, array('class' => 'form-control','placeholder'=>'First Name')) !!}

							</div>
							<div class="form-group form-group-second">
								
								{!!  Form::text('last_name', null, array('class' => 'form-control','placeholder'=>'Last Name')) !!}

							</div>
						</div>
					
						<div class="form-group">
							 {!!  Form::email('email',null,  array('class' => 'form-control','placeholder'=>'Email')) !!}

						</div>
						<div class="form-group">
							 {!!  Form::password('user_password', ['class' => 'form-control','placeholder'=>'Password']) !!}

						</div>
						<div class="form-group">

						<button class="btn login_btn">Craete</button>
						</div>
						
						
						   
							
						{!! Form::close() !!}		
					</div>
				</div>		
			</div>
		</div>
<!-- //Login -->	 
	

<!-- Footer -->

 @include('footer')

