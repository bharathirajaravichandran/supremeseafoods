
<!-- header -->

 @include('header')
<script>
function incrementValue()
{
    var value1 = document.getElementById('number').value;
    var value2 = document.getElementById('qty').value;
    value3 = value2*(value1);
    document.getElementById('total').value = value3;
  
}
</script>
<!-- head top bg -->
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Cart</h2>
						<h5><a id="banner" href="{{url('/')}}">Home</a> > Cart</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Login -->
<div class="Cart_content clearfix">
<div class="col-md-12 padding_none">
<div class="col-md-8 padding_xs">
<div class="col-md-6 padding_xs padding_none">
<h3 class="cart_title">Product</h3>
</div>
<div class="price_cart_all col-md-6 padding_xs padding_none md_block xs_none">
<div class="price_cart">
<h3 class="cart_title common_cart_hegt">Price</h3>
</div>
<div class="quantity_cart">
<h3 class="cart_title common_cart_hegt">Quantity</h3>
</div>
<div class="total_cart">
<h3 class="cart_title common_cart_hegt">Total</h3>
</div>

</div>
@if(count($cartItems)!=0)

@foreach($cartItems as $keys=>$carts)
<form name="vi" id="vi" action="{{url('update',['id' => $carts->rowId])}}" method="post">
{{ csrf_field() }}

<div class="col-md-12 col-sm-12 col-xs-12  padding_none cart_bottom">
<div class="col-md-6 padding_xs padding_none">
<div class="cart_product">
@if(count($cartlists)!=0)
@foreach($cartlists as $cartlist)
<a href="{{url('category',[$cartlist->catname,$carts->name,$carts->id])}}">
@endforeach
@endif

<img style="max-width:150px; max-height:100px;" src="/uploads/public/product_images/{{$carts->options->image}}"  class="cart_1img"/>
<p>{{$carts->name}}  ({{$carts->options->type}}) </p></a>
</div>
</div>

<div class="price_cart_all price_cart_all_bot col-md-6 padding_xs padding_none">
<div class="price_cart">
<h3 class="cart_title common_cart_hegt md_none xs_block">Price</h3>

<h3 class="cart_rup">&#8377; {{$carts->price}}<input class="price" type="hidden" name="price" id="price_{{$keys}}" value="{{$carts->price}}"/>
</h3>
</div>
<div class="quantity_cart">
<h3 class="cart_title common_cart_hegt md_none xs_block">Quantity</h3>
<div class="count-input space-bottom">
<a class="incr-btn" data-action="decrease" href="#"><span class="fa fa-minus"></span></a>
<input class="quantity" type="text"  name="quantity[{{$carts->rowId}}][]" id="quantity" value="{{$carts->qty}}"/>
<a class="incr-btn decr-btn" data-action="increase" href="" ><span class="fa fa-plus"></span></a>
</div>
<p>(in kgs)</p>
</div>
<div class="total_cart">
<h3 class="cart_title common_cart_hegt md_none xs_block">Total</h3>
<h3 class="cart_rup">&#8377;{{$carts->price*$carts->qty}}
 <a href="{{url('remove',['id' => $carts->rowId])}}" ><img src="/images/trash.png" class="trash_img" /></a></h3>
</div>
</div>
</div>

@endforeach
@else
<div class="head_content">
	<h2 align="center">Empty Cart</h2>
</div>
@endif

</div>

<div class="cart_Total_all col-md-4 padding_xs">
<h3 class="cart_title cart_title1">Cart Totals</h3>
<div class="cart_totals">
<h3 class="subtotal_cont">Subtotal <span class="cart_rup">&#8377; {{Cart::subtotal()}}</span></h3>

<h3 class="subtotal_cont subtotal_cont1">Shipping <span class="cart_free">Free</span></h3>
<h3 class="cart_Total">Total <span class="cart_rup">&#8377; {{Cart::subtotal()}}</span></h3>
<div class="form-group pull_left">
<a href="{{url('checkout')}}" class="btn login_btn proceed_btn pull-right">Proceed to Checkout</a>
<button type="submit" class="btn login_btn proceed_btn pull-right">Update Cart</button>


</div>
</div>
</div>
</form>
<!-- <div class="form-group ">
<a href="#" class="btn creat_btn">Creat Account</a>
</div> -->
</div>
</div>
<!-- //Login -->	 
	
   
<!-- //footer -->

 @include('footer')
