@include('header')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- head top bg -->
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Account</h2>
						<h5>Home > Login</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Header -->

<!-- Login -->
<div class="Login_section">
<div class="container">
<div class="row">
<div class="Login_content clearfix">
<h3>Login</h3>
<h5>Sign in with a simple click:</h5>
<ul class="social_icons">
<li><a href="facebook.com"><span class="fa fa-facebook"></span></a></li>
<li><a href="twitter.com"><span class="fa fa-twitter"></span></a></li>
<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
<li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
</ul>
<p class="or">(OR)	</p>

<!--Error-->

@if (count($errors) > 0)
   <div class="alert alert-danger">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
  <ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
  </ul>
   </div>
@endif

<!-- login -->

	{!! Form::open(['url' => 'login.custom','method' => 'post']) !!}
<div class="form-group">

  {!!  Form::text('user_email',null , array('class' => 'form-control','placeholder'=>'Email','id'=>'user_email')) !!}

</div>
<div class="form-group">

{!!  Form::password('user_password',['class' => 'form-control','placeholder'=>'Password','id'=>'user_password']) !!}

</div>	

<div class="form-group pull_left">
<button type="submit" class="btn login_btn pull-left">Login</button>
<a href="#" class="forget_btn pull-right">Forget your password?</a>
</div>
{!! Form::close() !!}	
<div class="form-group ">
<a href="{{ url('register') }}" class="btn creat_btn">Creat Account</a>
</div>
</div>
</div>	
</div>
</div>
<!-- //Login -->	 

<!-- Footer -->


@include('footer');
