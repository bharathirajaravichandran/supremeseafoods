<!-- //Header -->

 @include('header')
		
<!-- head top bg -->
		<div class="head_top_section head_top_section1">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>{{Request::segment(1)}}</h2>
						<h5><a id="banner" href="{{url('/')}}">Home </a> > <a id="banner">{{Request::segment(2)}}</a></h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Product Details -->
		<div class="product_details_section Product_page">
			<div class="container">
				<div class="row">
					<div class="product_details_content clearfix">
						<div class="col-md-12 padding_none">
														
							<div class="col-md-12 crabs_right padding_none padding_xs">
									
								
								@if(count(@$crabs) !=0)	
								
									@foreach($crabs as $crab)	
									
										<div class="col-md-3 col-sm-4 col-xs-6">
											
											
											<div class="related_prod">
												<a href="{{ url( 'state/' .Request::segment(2).'/'.preg_replace('/[^A-Za-z0-9\-]/','', $crab->productname).'/'.$crab->product_id) }}" style="background: transparent; border: 0px; outline: none	">
													<div class="owl_prod_img">
														<div class="owl_prod_img_table">
															<img src="/uploads/public/product_images/{{$crab->product_image}}" />
														</div>	
													</div>	
													<h4>{{$crab->productname}}</h4>
													<h5>Rs{{$crab->cleaned_price}} extra for cleaning</h5>
													<h2>&#8377;{{$crab->whole_price}}</h2>
												</a>
												
												
											</div>
											
										</div>
										
										@endforeach
										
									@endif
							
									
								
							</div>
							
						</div>
					</div>
				</div>		
			</div>
		</div>
<!-- //Product Details -->	 
	
   
<!-- //footer -->

 @include('footer')
