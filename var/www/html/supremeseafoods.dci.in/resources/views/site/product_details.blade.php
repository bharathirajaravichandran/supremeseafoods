<script src="/js/jquery-min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">

<script type="text/javascript">
	//add to cart
$(document).ready(function() {
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
        $('#add').click( function(e) 
        {   
			if(!$('input[name=yesno]:checked').val())
			{   
				var msg = '<span style="color:red;">You must select Whole Or Cleaned Price!</span>';
				document.getElementById('msg').innerHTML = msg;
				return false;
				
			}
			else if($('#sel1').val() == 0)
			{   
				document.getElementById('msg').innerHTML = " ";
				var msgs = '<span class="col-md-offset-6" style="color:red;"><small>You must select Cut Type</small></span><br /><br />';
				document.getElementById('msgs').innerHTML = msgs;
				return false;
			}
			 
			var qty=$('#quantity').val();
			var product_id=$('#pro_id').val();
			var user_id=$('#user_id').val();
			var wprice=$('input[id=price]:checked').val();
			var type=$('#sel1').val();
			var url = "{{url('cartadd')}}";
			     e.preventDefault();
         
            $.ajax({
                type:"POST",
                url: url,
                dataType: "json",
                data: {pro_id: product_id, quantity: qty,price: wprice,type: type,user_id: user_id, _token: '{{csrf_token()}}'}, 
                success: function( msg ) {
				$('#head_cart_count').html(msg.count);
				}
              });
              return false;
              
          });
           
          
        $('#wishlist').click( function(e) 
        {   
			
			document.getElementById('message').innerHTML = '';
			if($('#user_id').val() == 0 )
			{
				
				 $("#wishlist").click(function () {
					 document.getElementById('messages').innerHTML = "Please Login";
				      $( "#messages" ).toggle();
				  });  
								
				return false;
				
			}
			else
			{
			
			if(!$('input[name=yesno]:checked').val())
			{   
				var msg = '<span style="color:red;">You must select Whole Or Cleaned Price!</span>';
				document.getElementById('msg').innerHTML = msg;
				return false;
				
			}
			else if($('#sel1').val() == 0)
			{   
				document.getElementById('msg').innerHTML = " ";
				document.getElementById('message').innerHTML = " ";
				var msgs = '<span class="col-md-offset-6" style="color:red;"><small>You must select Cut Type</small></span><br /><br />';
				document.getElementById('msgs').innerHTML = msgs;
				return false;
			}
			document.getElementById('msgs').innerHTML = "";
			}

			
			document.getElementById('message').innerHTML = "";
			$('#spinner').show();
			var qty=$('#quantity').val();
			var product_id=$('#pro_id').val();
			var user_id=$('#user_id').val();
			var cutname=$('#sel1').val();
			//~ var price_type=$('#price_type').val();
			var wprice=$('input[id=price]:checked').val();
			if($('#price[1]') == true)
			{
				var price_type='Whole';
			}
			else
			{
				var price_type='Cleaned';
			}
		
			var url = "{{url('wishlists')}}";
			     e.preventDefault();
         
            $.ajax({
                type:"POST",
                url: url,
                dataType: "json",
                data: {pro_id: product_id, quantity: qty,price: wprice,cutname: cutname,price_type: price_type,user_id: user_id, _token: '{{csrf_token()}}'}, 
                
                success: function( responses ) {
						
					 if(responses.pro_id == 0)
					{	
						
					$('#message').html(responses.message);
				
					
					}
					else if(responses.pro_id == 1)
					{		
					
					
					$('#message').html(responses.message);
					
					}
					
					else if(responses.user_id == 0)
					{
						
					  window.location = '/login';
					}
				
				},
					complete: function(){
						$('#spinner').hide();
					}
				});
			
              return false;
              
          });
          
          

      });
      

</script>


    @include('header')
<!-- head top bg -->
		<div class="head_top_section head_top_section1">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>{{Request::segment(2)}}</h2>
						<h5> <a id="banner" href="{{url('/')}}"> Home </a> > 
						
						<a href="{{url(Request::segment(1).'/'.Request::segment(2).'/'.$catId) }}" id="banner">
							{{Request::segment(2)}}
						</a>
						> {{Request::segment(3)}}</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
		
<!-- Product Details -->
		<div class="product_details_section">
			<div class="container">
				<div class="row">
					
					@foreach($products as $product)
							
					<div class="product_details_content clearfix">
						<div class="col-md-12 padding_none">	
							<div class="col-md-5 padding_none clearfix">
							<form name="{{url('cartadd')}}" method="post"> 
								<div class="checkout_left">									
									<div class="pull-right col-md-9 col-sm-9 col-xs-12">
										<div class="prod_table">
											<div class="prod_table_cell" >
											<img id="zoom_03" class="zoom_img_view" src="/uploads/public/product_images/{{$product->product_image}}" data-zoom-image="/uploads/public/product_images/{{$product->product_image}}"/>
											</div>
										</div>
									</div>	
								 	
									<div id="gal1" class="col-md-3 col-sm-12 col-xs-12 padding_none">
										<div class="bxslider">		
									@foreach($productimages as $productimage)							 					 
											<a href="#" data-image="/uploads/public/product_images/{{$productimage->product_image}}" data-zoom-image="/uploads/public/product_images/{{$productimage->product_image}}">
											<img id="zoom_03" src="/uploads/public/product_images/{{$productimage->product_image}}"  />
									  </a>	
									@endforeach	
									  </div>								 
									</div>	
													
								</div>
							</div>
							
							<div class="crabs_right col-md-7 padding_xs" id="form-container">
								
								<h3 class="crabs_title">{{$product->productname}}</h3>
								<div class="carbs_totals carbs_totals1">
									
									<h3 class="carbs_whole"><input type="radio" name="yesno" id="price" value="{{$product->whole_price}}"> 
									Whole <br/><input type="hidden" name="price_type" id="price_type" value="Whole"><span class="cart_rup">&#8377;{{$product->whole_price}} </span></h3>
									
									<h3 class="carbs_cleaned"><input type="radio" name="yesno" id="price" value="{{$product->whole_price+$product->cleaned_price}}"> 
									Cleaned <br/><input type="hidden" name="price_type" id="price_type" value="Cleaned"><span class="cart_rup">&#8377; {{$product->whole_price+$product->cleaned_price}}</span></h3>
											<div id="msg" ></div>													
								</div>
								<div class="details_cont">
									<h5>* Whole: The product will be weighed before cleaning,</h5>
									<h5># Steaks/HGT: The product will be weighed after cleaning (with skin and bone)</h5>
								</div>
								<div class="details_skucont">
									<h5><span>SKU: </span>{{$product->product_sku}}</h5>
									<h5><span>Categories:</span> Andhra, Blue Crabs, Crabs, Expat, Fresh Sea Food, Kerala, Tamil Nadu</h5>
									<h5><span>Tags:</span>{{$product->product_tags}}</h5>
								</div>
							
								<div class="details_quantity details_quantity1">
									<div class="details_quant"><span>Quantity</span>
										<div class="count-input space-bottom">
										<a class="incr-btn" data-action="decrease" href="#"><span class="fa fa-minus"></span></a>
										<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

											<input class="quantity" type="text" name="quantity" id="quantity" value="1"/>
											<input type="hidden" name="pro_id" id="pro_id" value="{{$product->product_id }}">
										<a class="incr-btn decr-btn" data-action="increase" href="#"><span class="fa fa-plus"></span></a>										
									</div>
									<p>(in kgs)</p>
									</div>
									<div class="details_type">
										<span>Type of Cut:</span>
											<div class="dropdown">											 
												<select class="dropdown-toggle form-control" name="sel1" id="sel1">
													<option value='0'>Select Cuts</option>		
												@foreach($slices as $slice)
													<option>{{$slice->cut_name}}</option>													
												@endforeach												
												</select>
											</div>												
									</div>
									<div id="msgs"></div>
									</div>	
									
									<div class="form-group pull_left view_cart_btn view_cart_btn1">						
	

										<button class="btn login_btn add_tocart_btn btn-submit" id="add">Add To Cart</button>
										
										
										
										<button	 class="btn login_btn add_tocart_btn"  name="wishlist" id="wishlist" value="wishlist" >Wishlist</button>
										
										<img src="/images/bg-spinner.gif" id="spinner" style="display:none;width:35px;height:35px;"/>
										
										<input type="hidden" name="product_id" id="product_id" value="{{$product->product_id}}">
										
										
										<input type="hidden" name="user_id" id="user_id" value="{{$users}}">
										
										<span  style="color:red;" class="message" id="message"></span>
										<span  style="color:red;" class="messages" id="messages" style="display:none"></span>
										
											
											
											@if(Session::has('adminusername'))
											<span  style="color:red;">
											{{ Session::get('adminusername') }}
											</span>
											@endif
										
										<p style="margin-top: 10px;">This item has been added to cart! <a href="{{url('cartdetail')}}" class="view_cart">View Cart</a> or <a href="{{ url('/') }}" class="continue_cart">Continue Shopping</a></p>	
										
										

									
								</div>
							</div>
						</form>
							<div class="col-md-12 padding_none padding_xs">
								<div id="horizontalTab">
									<ul class="resp-tabs-list">
										<li>Description</li>
										<li>Size and Quantity</li>										
									</ul>
									<div class="resp-tabs-container">
										<div>
											<p>Another popular seafood belonging to the shell fish family is crab. It is also available in many varieties such as blue crab, mud crab etc. The white and succulent meat of crab
is preferred by many seafood consumers.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
										</div>
										<div>
											<p>Another popular seafood belonging to the shell fish family is crab. It is also available in many varieties such as blue crab, mud crab etc. The white and succulent meat of crab
is preferred by many seafood consumers.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
										</div>
									</div>
								</div>						
							</div>
							
							@endforeach
							
							
							
							<div class="col-md-12 crabs_right padding_none padding_xs">
								<h3 class="crabs_title crabs_title_related">Related Products</h3>							
								
								<div class="owl-carousel owl-theme" id="owl_carousel_product">
									@if(count(@$relatedproducts) !=0)	
									@foreach($relatedproducts as $relatedproduct)
									
									<div class="item">
										<div class="related_prod">
											<a href="{{ url( 'category/' .preg_replace('/[^A-Za-z0-9\-]/','', $relatedproduct->catname).'/'.preg_replace('/[^A-Za-z0-9\-]/','',$relatedproduct->productname).'/'.$relatedproduct->product_id) }}">
												<div class="owl_prod_img">
												<img src="/uploads/public/product_images/{{$relatedproduct->product_image}}" />
											</div>
											<h4>{{$relatedproduct->productname}}</h4>
											<h5>Rs. {{$relatedproduct->whole_price}} extra for cleaning</h5>
											<h2>&#8377; {{$relatedproduct->cleaned_price}}</h2></a>
										</div>
									</div>
									
									@endforeach
								@endif 
									
								</div>
							</div>
						
						</div>
					</div>
				</div>		
					
			</div>
			
			
		</div>
<!-- //Product Details -->	  
	
@include('footer')
