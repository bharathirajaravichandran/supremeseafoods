
<!-- header -->

 @include('header')
 
 
<!-- head top bg -->
		<div class="head_top_section">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>{{Request::segment(1)}}</h2>
						<h5><a id="banner" href="{{url('/')}}">Home </a> > <a id="banner">{{Request::segment(1)}}</a></h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Login -->

 <div class="Cart_section">
	<div class="container">
		<div class="row">
			<div class="Cart_content Cart_contentNN clearfix">
				<div class="col-md-12 padding_none">
				<div class="col-md-12 padding_xs">
					<div class="col-md-5 padding_xs padding_none">
						<h3 class="cart_title">Product</h3>
					</div>
					<div class="price_cart_all col-md-7 padding_xs padding_none md_block xs_none">	
						<div class="price_cart">
							<h3 class="cart_title common_cart_hegt">Price</h3>
						</div>	
						<div class="quantity_cart">
							<h3 class="cart_title common_cart_hegt">Add To Cart</h3>	
						</div>	
						<div class="total_cart">
						<h3 class="cart_title common_cart_hegt">Remove</h3>
						</div>	

					</div>	
					
					@if(count(@$wishlists) !=0)
					@foreach($wishlists as $wishlist)
  
					<div class="col-md-12 col-sm-12 col-xs-12  padding_none cart_bottom">	
							<div class="col-md-5 padding_xs padding_none">	
								<div class="cart_product cart_productnn item">
									<a href="{{ url( 'category/' .preg_replace('/[^A-Za-z0-9\-]/','', $wishlist->catname).'/'.preg_replace('/[^A-Za-z0-9\-]/','', $wishlist->productname).'/'.$wishlist->product_id) }}" style="color:black;font-weight:bolder">
									<img style="max-width:150px; max-height:100px;" src="/uploads/public/product_images/{{$wishlist->product_image}}"  class="cart_1img"/>
									<p>{{$wishlist->productname}} ({{$wishlist->cutname}})</a></p>
								</div>
							</div>
							<div class="price_cart_all price_cart_all_newn price_cart_all_bot col-md-7 padding_xs padding_none">	
								<div class="price_cart">	
									<h3 class="cart_title common_cart_hegt md_none xs_block">Price</h3>
									<div class="carbs_totals">

										<h3 class="cart_rup">&#8377; {{$wishlist->price}}</h3>
										
									</div>
								</div>

								<div class="quantity_cart">

									<h3 class="cart_title common_cart_hegt md_none xs_block">Add to Cart</h3>
									<div class="space_bottom">
										<a class="add_to_cart" id="add" href="{{url('cartadd').'/'.$wishlist->product_id.'/'.$wishlist->cutname}}"><img src="images/cart1.png"  class="cart_imgg"/></a>	
										<input class="quantity" type="hidden" name="quantity"  id="quantity"  value="1"/>
										<input class="pro_id" type="hidden" name="pro_id" id="pro_id"  value="{{$wishlist->id}}"/>
										<input class="user_id	" type="hidden" name="user_id"  id="user_id"  value="{{$wishlist->user_id}}"/>
										<input class="price" type="hidden" name="price" id="price"  value="{{$wishlist->price}}"/>
										<input class="type" type="hidden" name="type" id="type"  value="{{$wishlist->cutname}}"/>
									</div>	
								</div>
								<div class="total_cart total_cartnn">
									<h3 class="cart_title common_cart_hegt md_none xs_block">Remove</h3>
									<h3 class="cart_rup"> <a href="{{url('removes',['userid' => $wishlist->user_id,'id' => $wishlist->product_id,'price'=>$wishlist->price,'cutname'=>$wishlist->cutname])}}" ><img src="/images/trash.png" class="trash_img" /></a></h3>
								</div>
							</div>
						</div>

						@endforeach
						@else
						<h2 style="text-align:center">Empty Wishlist</h2>
						@endif

						@if(count(@$wishlists) !=0)
						<a href="{{url('removeall')}}" style="margin-top:10px" type="submit" class="btn login_btn proceed_btn pull-right">Remove All</a>
						@endif

					</div>
				</div>
			</div>
		</div>	
	</div>
</div>

<!-- //Login -->	 
	
   
<!-- //footer -->

 @include('footer')
 

<style>
/**25.may**/
.Cart_contentNN .carbs_totals .carbs_whole  .cart_rup { color:#fff !Important; }	
.Cart_contentNN  .carbs_totals .carbs_cleaned  .cart_rup { color:#fff !Important; }	
.Cart_contentNN  .quantity_cart .space_bottom .add_to_cart {  max-width: 35px; display: inline-block; background: transparent; border: 0px; outline: none; }
.Cart_contentNN  .quantity_cart .space_bottom .add_to_cart .cart_imgg {   max-width: 100%; }
.Cart_contentNN  .price_cart_all_newn .carbs_totals .carbs_whole  { width:100px; font-size:16px; }
.Cart_contentNN  .price_cart_all_newn .carbs_totals .carbs_cleaned { width:105px; font-size:16px;  }
.Cart_contentNN  .price_cart_all_newn	.carbs_totals .cart_rup { font-size: 15px;  }
.Cart_contentNN  .cart_product.cart_productnn { display:table; }
.Cart_contentNN  .Cart_content .cart_productnn p {  margin-left: 114px; display: table-cell;  vertical-align: middle; }
.Cart_contentNN	.total_cart .trash_img {  float: none; }
.Cart_content .cart_product p { display: table-cell;  vertical-align: middle; padding:10px 5px }


@media only screen and (min-width : 992px) and (max-width: 1200px){ 

/**25.may**/
.Cart_contentNN .price_cart_all_newn .carbs_totals .carbs_whole { width: 80px; font-size: 14px; margin: 0px 0px 5px 0px; }
.Cart_contentNN .price_cart_all_newn	.carbs_totals .cart_rup {  font-size: 14px; }
.Cart_contentNN .price_cart_all_newn .carbs_totals .carbs_cleaned {  width: 81px; font-size: 13px; padding:12px 8px; }


}

@media only screen and (max-width:767px){ 
/**25.may**/
.Cart_contentNN .price_cart_all_newn .carbs_totals .carbs_whole { width: 80px; font-size: 14px; margin: 0px 0px 5px 0px; }
.Cart_contentNN .price_cart_all_newn	.carbs_totals .cart_rup {  font-size: 14px; }
.Cart_contentNN .price_cart_all_newn .carbs_totals .carbs_cleaned {  width: 81px; font-size: 13px; }
.Cart_content .cart_product p { display: table-cell;  vertical-align: middle; padding:10px 5px }
.Cart_contentNN .carbs_totals { padding-bottom: 15px; }

}

@media only screen and (max-width:480px){ 
/**25.may**/
.Cart_content .cart_title {  font-size: 13px; }

}

</style>
