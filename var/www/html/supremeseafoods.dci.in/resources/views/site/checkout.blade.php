@include('header')
<!-- head top bg -->
		<div class="head_top_section head_top_section1">
			<div class="container">
				<div class="row">
					<div class="head_content">
						<h2>Checkout</h2>
						<h5> <a id="banner" href="{{url('/')}}">Home</a> > <a id="banner" href="{{url('/').'/'.'cartdetail'}}">Cart</a> > Checkout</h5>
					</div>
				</div>		
			</div>
		</div>
<!-- //head top bg -->
     
<!-- Checkout -->
		<div class="Checkout_section">
			<div class="container">
				<div class="row">
				  <form name="checkout" action="{{url('thankyou')}}" method="post" id="myform">
				  	<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
					<div class="Checkout_content clearfix">
						<div class="col-md-12 padding_none">
							<div class="col-md-7 padding_none">
								<div class="checkout_left">
									<h3 class="checkout_title">Billing Details</h3>
									<div class="form-inline">
										<div class="form-group ">
										  <label for="usr">First name <sup>*</sup></label>
										  <input type="text" name="firstname" id="firstname" class="form-control" value="@if(@$userDetail->user_first_name !=''){{ $userDetail->user_first_name }}@elseif($users!=''){{ $users->user_first_name }}@endif" >
										</div>
										<div class="form-group form-group-second">
										  <label for="usr">Last name <sup>*</sup></label>
										  <input type="text" name="lastname" id="lastname" class="form-control" value="@if(@$userDetail->user_last_name !=''){{ $userDetail->user_last_name }}@elseif($users!=''){{ $users->user_last_name }}@endif" >
										  <input type="hidden" name="cartId" value="{{$cartId}}">
										</div>
									</div></br>
									<div class="form-group">
									  <label for="pwd">Company name</label>
									  <input type="text" name="companyname" id="companyname" class="form-control" value="@if(@$userDetail->companyname !=''){{ $userDetail->companyname }}@endif" >
									</div>
									<div class="form-group">
									  <label for="usr">Street Address <sup>*</sup></label>
									  <textarea name="address" name="address" id="adderess" class="form-control form-control-second">@if(@$userDetail->address !=''){{ $userDetail->address }}@endif</textarea>
									</div>
									<div class="form-group">
									  <label for="usr">Town City <sup>*</sup></label>
									  <input type="text" name="city" id="city" class="form-control" value="@if(@$userDetail->city !=''){{ $userDetail->city }}@endif">
									</div>
									<div class="form-group">
									  <label for="pwd">Country</label>
									  <input type="text" name="country" id="country" class="form-control" value="@if(@$userDetail->country !=''){{ $userDetail->country }}@endif" >
									</div>
									<div class="form-group">
									  <label for="usr">Pincode <sup>*</sup></label>
									  <input type="test" name="pincode" id="pincode" class="form-control" value="@if(@$userDetail->pincode !=''){{ $userDetail->pincode }}@endif" >
									</div>
									<div class="form-inline">
										<div class="form-group">
										  <label for="usr">Phone <sup>*</sup></label>
										  <input type="text" name="phone" id="phone" class="form-control" value="@if(@$userDetail->phone !=''){{ $userDetail->phone }}@endif">
										</div>
										<div class="form-group form-group-second">
										  <label for="usr">Email Address <sup>*</sup></label>
										  <input type="email" name="email" id="email" class="form-control" value="@if(@$userDetail->user_email !=''){{ $userDetail->user_email }}@elseif($users!=''){{ $users->user_email }}@endif" >
										</div>
									</div>
										
								</div>
							</div>
							<div class="cart_Total_all checkout_right col-md-5 padding_xs">
								<h3 class="checkout_title common_center">Your Order</h3>
								
								<div class="cart_totals">
									<h3 class="subtotal_cont">Product <span class="prod_total">Total</span></h3>
									<div class="content_details clearfix">
									@if(count($productItem)!=0)
									@foreach($productItem as $keys=>$productItems)	

									<div class="content_details clearfix">
										<h3 class="subtotal_cont subtotal_cont2 pull-left">{{$productItems->name}}<span style="color:#fa5237;"> X {{$productItems->qty}}</span></h3>
										<span class="cart_rup pull-right">&#8377; {{$productItems->price*$productItems->qty}}</span>
									</div>
									
									@endforeach	
									@endif
									<div class="content_details clearfix">
										<h3 class="subtotal_cont subtotal_cont1">Shipping <span class="cart_free">Free</span></h3>
									</div>

									<div class="content_details clearfix">
										<h3 class="subtotal_cont subtotal_cont2 pull-left">Total</h3>
										<div name="subtotal" class="cart_rup pull-right">&#8377; {{Cart::subtotal()}}</div>
										<input type="hidden" name="total" value="{{Cart::subtotal()}}">
									</div>
									
								</div>
							
							<div class="col-md-12 padding_xs">
								<h3 class="checkout_title pay_title common_center">Payments</h3>
									<div id="msg"></div>
								<div class="checkout_right" style="width:100%;display: block;position: relative;">
									<div class="radio">
									  <label><input type="radio" name="payment" value="1">Credit card</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="payment" value="2">Debit card</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="payment" value="3">Internet Banking</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="payment" value="4">Wallet</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="payment" value="5">Cash On Delivery</label>
									</div>
										
									 <div class="form-group pull_left">										
										<input type="submit" name="submit" id="submit" value="Place Order" class="btn login_btn proceed_btn pull-right">										
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>		
			</div>
		</div>
		</div>
		</div>
		</div>
		<style>
			
			.cart_Total_all.checkout_right  label.error {  position: absolute; top:-30px; left: -30px; width:100%; display: block; }
		</style>
<!-- //Checkout -->	 
 



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script type="text/javascript">
				jQuery(document).ready(function($) {
		$.validator.addMethod("valueNotEquals", function(value, element, arg){
			return arg != value;
		}, "Value must not equal arg.");
		
		$.validator.addMethod("alphanumeric", function(value, element) {
			return this.optional(element) || /^[a-z0-9A-Z]+$/i.test(value);
		}, "Zip Code must contain only letters or numbers.");
		
		$.validator.addMethod("url", function(value, element) {
			return this.optional(element) || /^(http:\/\/www\.|http:\/\/www\.|http:\/\/|http:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value); 
		}, "Please enter a valid URL");
		
		$.validator.addMethod("email", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(value); 
		}, "Please enter a valid email");
				
		$.validator.addMethod("phoneval", function(value, element) {
			return this.optional(element) || /^[-+]?[0-9]{9,11}$/i.test(value); 
		}, "Please enter 10 digits number only.");
		
		$.validator.addMethod("url_reg", function(value, element) {
			return this.optional(element) || /^(http:\/\/www\.|http:\/\/www\.|http:\/\/|http:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value); 
		}, "Please enter your valid website");
		
		$.validator.addMethod("extension", function(value, element) {
			return this.optional(element) || /([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf|.txt)$/i.test(value);
		}, "Invalid file type upload (txt,doc,docx and pdf) only");
		
		$.validator.addMethod("lettersonly", function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
		}, "Alpha Numeric characters only allowed."); 
		
				$.validator.addMethod("numbersonly", function(value, element) {
			return this.optional(element) || /^[0-9]+$/.test(value);
		}, "Numbers only allowed."); 
		
		function recaptchaCallback() {
			$('#hiddenRecaptcha').valid();
		};	
				

// Checkout Form Validation
	
	$("#myform").validate({
		ignore: ".ignore",
			rules: {
				firstname: {
					required: true,
					lettersonly: true,
				},
				lastname: {
					required: true,
					lettersonly: true,
				},				
				email: {
					required: true,
					email: true,
				},
				city: {
					required: true,
					lettersonly: true,
				},
				country: {
					required: true,
					lettersonly: true,
				},
				address: {
					required: true,
					alphanumeric: true,
				},
				companyname: "required",
				payment: "required",	
				//skills: "required",
				//gender: "required",
				//code: { valueNotEquals: "Select Country Code" },
				pincode: {
					required: true,
					numbersonly: true,
				},			
				phone: { 
					required: true,
					phoneval: true 
				},									
				hiddenRecaptcha: {
                required: function () {
                    if (grecaptcha.getResponse() == '') {
                        return true;
                    } else {
                        return false;
                    }
                }
            } 
				},
						
			messages: {
				firstname: {					
					required: "Please enter your name",
					lettersonly: "Alphanumeric not allowed",
				},
				lastname: {					
					required: "Please enter your name",
					lettersonly: "Alphanumeric not allowed",
				},
				email: {
					required: "Please enter your email-id",
					email: "Please enter valid email-id"
				},
				address: {
					required: "Please enter your address",
					alphanumeric: "Please enter address"
				},
				city: {					
					required: "Please enter your city",
					lettersonly: "Alphanumeric not allowed",
				},
				country: {					
					required: "Please enter your country",
					lettersonly: "Alphanumeric not allowed",
				},
				payment: "Please select your payment",
				companyname: "Please enter your companyname",
				//skills: "Please enter your skills",
				//gender: "Please select your gender",	
				//code: { valueNotEquals: "Select country code" },
				pincode: {					
					required: "Please enter your pincode",
					numbersonly: "Alphanumeric not allowed",
				},			
				phone: {
					required: "Please enter your mobile number",
					phoneval: "Please enter 10 digits number only."
				},			
				hiddenRecaptcha: {					
					required: "Please verify that you are not a robot",
				},
			}
		});
		$('#myform').submit(function () {
		if ($(this).valid()) {
        $(':submit', this).attr('disabled', 'disabled');
        $(':submit', this).val('Sending...');
    }
});			
});
</script>

@include('footer')
