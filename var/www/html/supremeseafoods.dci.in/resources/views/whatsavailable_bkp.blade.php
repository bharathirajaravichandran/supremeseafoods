    <section class="content">
                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/addproduct" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
            <form id="prod_from" action="{{ url('/') }}/admin/saveavailable" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container>
    
        <div class="box-body">
            <div class="fields-group">

			<?php $i=0;?>
			
			@foreach ($products as $k=>$v)

				<div id="prod_name{{$i}}" class="form-group">
					<label for="productname" class="col-sm-2 control-label">{{$v->productname}}</label>
					<input type="hidden" name="productid[]" value="{{$v->id}}">
					<div id="prod_name_inner{{$i}}" class="col-sm-4">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-pencil"></i></span>
							<input type="text" id="productname{{$i}}" name="productname[]" value="0" class="form-control productname" placeholder="Quantity" />
						</div>
					</div>
				</div>
			<?php $i++;?>
			@endforeach
            <?php $i--;?>
            </div>
        </div>

        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <div class="btn-group pull-right">
    <button type="button" name="submit" onclick="validation({{$i}});" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
</div>
                <div class="btn-group pull-left">
    <button type="reset" class="btn btn-warning">Reset</button>
</div>

            </div>

        </div>

    </form>
</div>

</div></div>

    </section>

<script>

function validation(cnt)
{
	var error='';
	for(i=0;i<=cnt;i++)
	{
		var prod_qty=$('#productname'+cnt).val();
		prod_qty=prod_qty.trim();
		if(prod_qty=='')
		{
			$('#prod_name'+i).addClass('has-error');
			$('#prod_name_inner'+i).prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Quantity is required.</label>');
			error='1';
		}
	}
	if(error=='')
	{
		$('#prod_from').submit();
	}
	else
	{
		return false;
	}
}


</script>
