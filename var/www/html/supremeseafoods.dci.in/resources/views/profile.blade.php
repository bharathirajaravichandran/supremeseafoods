 
<!--
<script src="/js/validate.js"></script>
-->
 @include('header')		
<!-- head top bg -->


		<div class="head_top_section head_top_section1">
			<div class="container">
				<div class="row">
					<div class="head_content">
					<div class="profile_img">
							
							<img src="@if(@$userDetail->user_profile_image =='')/images/images.png @elseif(@$userDetail->user_profile_image !='')public/uploads/user_image/{{ $userDetail->user_profile_image }}@elseif($users!='')public/uploads/user_image/{{ $users->user_profile_image }}  @endif"
								 name="file-input"  class="profile_picture" alt="user_img"/>
						
                         
					</div>	
				
						<h4>@if(@$userDetail->user_first_name !=''){{ $userDetail->user_first_name }}
						@elseif($users!=''){{ $users->user_first_name }}
						@endif</h4>
					
					</div>
				</div>		
			</div>`
		</div>
<!-- //head top bg -->
<!-- Checkout -->
		<div class="Checkout_section">
			<div class="container">
				<div class="row">
					<div class="Checkout_content profile_content clearfix">
						<div class="col-md-12 padding_none">
							<div class="col-md-12 clearfix padding_none">
								<div class="checkout_left">
								<form action="{{url('updates')}}" name="passenger-validation" method="POST" enctype="multipart/form-data" id="myform"  class="passenger-validation">	
									{{csrf_field()}}		
									
									<h3 class="checkout_title">Profile Details</h3>
									<div class="form-inline" >
										<div class="form-group" id="users_name" >
										  <label for="usr">First name <sup>*</sup></label>
										  <input type="text"  class="form-control"   name="fname" id="firsts_name"
										   value="@if(@$userDetail->user_first_name !=''){{ $userDetail->user_first_name }}@elseif($users!=''){{ $users->user_first_name }}@endif">

											</div>
										<div class="form-group form-group-second" id="las_name">
										  <label for="usr">Last name <sup>*</sup></label>
										  <input type="text" class="form-control"  name="lname" id="last_name" 
										  value="@if(@$userDetail->user_last_name !=''){{ $userDetail->user_last_name }}@elseif($users!=''){{ $users->user_last_name }}@endif">
									
										</div>
								    	</div><br><br>
									 <div class="form-inline">
										
										<div class="form-group">
										  <label for="usr">Date of Birth <sup>*</sup></label>
										  <input type="date" class="form-control"  name="dob"   id="dob" 
										  value="@if(@$userDetail->user_dob !=''){{ $userDetail->user_dob }}@elseif($users!=''){{ $users->user_dob }}@endif">
								
										
										</div>	
										
										<div class="form-group form-group-second radio_inline_top">
										  <label for="usr">Gender <sup>*</sup></label>
										  <label class="radio-inline radio_inline"><input type="radio" name="optradio1"  value="male"  checked >Male</label>
										  <label class="radio-inline radio_inline"><input type="radio" name="optradio1"  value="female" >Female</label>									
										
											</div>	
								   	</div>
   
									<div class="form-group form-group-second">
									  <label for="pwd">Company name</label>
									  <input type="text"  name="coname" id="coname"  placeholder="Enter the name" class="form-control"
									   value="@if(@$userDetail->companyname !=''){{ $userDetail->companyname }}@elseif($users!=''){{ $users->companyname }}@endif">
									
									</div>
						           
	                                                   <label for="usr">Street address<sup>*</sup></label>
									<div class="form-group" >
										 <div id="locationField">
											  <input id="autocomplete"  placeholder="Enter your address"
											onFocus="geolocate()" type="text" class="form-control" name="address" id="address" 
											 value="@if(@$userDetail->address !=''){{ @$userDetail->address }}@endif"> </input>										
									 
									     </div>
									</div> 
								 
									<div class="form-group">
									   <input type="text" class="form-control form-control-second"  id="street_number" name="door" 
									   value="@if(@$userDetail->door !=''){{ @$userDetail->door }}@endif">
									  <input type="text" class="form-control" id="route"  name="street" 
									  value="@if(@$userDetail->address !=''){{ @$userDetail->address }}@endif" >
							          </div>
									
									  <div class="form-inline">
										<div class="form-group">
										  <label for="usr">city <sup>*</sup></label>
										  <input type="text" id="locality" class="form-control"  name="city" 
										   value="@if(@$userDetail->city !=''){{ @$userDetail->city }}@endif">
										</div>
										
										
									  
										 <div class="form-group">
									  <label for="usr">state <sup>*</sup></label>
										  <input type="text"   id="administrative_area_level_1" class="form-control"  name="state" 
										  value="@if(@$userDetail->city !=''){{ @$userDetail->state }}@endif">
										 </div>
										</div>
										
									     <div class="form-inline">
								         <div class="form-group">
										  <label for="usr">Pincode <sup>*</sup></label>
										  <input type="text" id="postal_code" class="form-control"  name="pincode"  
										  value="@if(@$userDetail->pincode !=''){{ $userDetail->pincode }}@endif">
										</div>
										
										
										
									  
										<div class="form-group">
										  <label for="usr">country <sup>*</sup></label>
										  <input type="text"   id="country" class="form-control"  name="country"
										   value="@if(@$userDetail->country !=''){{ @$userDetail->country }}@endif">
										
										</div>	
										</div>
										
									   <div class="form-inline">
										<div class="form-group">
										  <label for="usr">Phone <sup>*</sup></label>
										  <input type="text" class="form-control"  name="phone"  id="phone" 
										   value="@if(@$userDetail->phone !=''){{ @$userDetail->phone }}@endif" >
									    </div>
										<div class="form-group" >
										  <label for="usr">Email Address <sup>*</sup></label>
										  <input type="text" class="form-control"  name="email" id="email" 
										  value="@if(@$userDetail->user_email !=''){{ $userDetail->user_email }}@elseif($users!=''){{ $users->user_email }}@endif">
							
									  
										</div>
									   </div>
									 
										 
										<div class="form-group form-group-second">									
										  <label for="usr">Password <sup>*</sup></label>
										  <input type="password" class="form-control" name="password" id="password"  placeholder="********"  >
										   </div>
										
									     <div class="form-group form-group-second">
										  <label for="usr">Confirm Password <sup>*</sup></label>
										  <input type="password" class="form-control" name="cpassword" id="cpassword"   placeholder="********"  >
									
									   </div>
										</div>
									  </div> 
										
										

									<div class="form-group">
										<label for="usr">Image Upload <sup>*</sup></label>										
										<div class="image_upload">
											<label for="file-input">											
											<h4><img src="images/upload_white.png"  alt="user_img" /> </h4>											
											</label>
											 
											 <input   id="file-input" type="file"  onchange="readURL(this);" class="file-input" name="file-input"  />
																
										</div>
							
										<div class="upload_after">
										
											<img src="@if(@$userDetail->user_profile_image =='')/images/images.png @elseif(@$userDetail->user_profile_image !='')public/uploads/user_image/{{ $userDetail->user_profile_image }}@elseif($users!='')public/uploads/user_image/{{ $users->user_profile_image }}  @endif"
											 id="blah"  name="file-input"  class="profile_imgg" alt="image"/>
										  
										
										</div>
									</div>
			
									<div class="form-group common_center">
										<input type="submit" class="btn login_btn proceed_btn"   value="Submit" name="Submit">										
								    </div>
								
								</form>
							</div>
							</div>
							</div>
						</div>
					</div>
				</div>		
			</div>
		</div>
	</div>

<!-- //Checkout -->	 

<script>
   
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
dobs
<script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places&callback=initAutocomplete"
        async defer></script>
 <script>       
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(150)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  </script>  
  


<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script src="{{ asset ('/js/validate.js') }}" type="text/javascript"></script>
 <script type="text/javascript" src="http://www.technicalkeeda.com/js/javascripts/plugin/jquery.js"></script>
    <script type="text/javascript" src="http://www.technicalkeeda.com/js/javascripts/plugin/jquery.validate.js"></script>

<script>
 $(".passenger-validation").validate({
        rules: {
           // simple rule, converted to {required:true}
          
		  
		   
		 
    }
     messages: {
                password:{
					required: 'Enter this!',
					}
                
            }
    
  });
  
</script>


 
    <script>

    $("#myform").validate({
            rules: {
				 fname: "required",
           lname: "required",
           dob: "required",
           optradio1: "required",
           coname: "required",
           address: "required",
           phone: "required",
		   email: "required",
           
             cpassword: "required",
             cpassword: {
                  equalTo: "#password"
              }
            },
            messages: {
                fname: " Enter First name",
                lname: " Enter Last Name",
                dob: " Enter DOB",
                optradio1: " Enter Gender",
                coname: " Enter Company Name",
                address: " Enter Address",
                phone: " Enter Phone number"
              
            }
        });

    
 
    </script>

  
<style>
label.error{
	color:red;
	}
</style>
  
<!-- //footer -->	
@include('footer')
<!-- //main slider-banner --> 

