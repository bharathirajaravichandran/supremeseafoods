<div class="row">
   <div class="box-tools">
 
      <div class="btn-group pull-right"  style="margin-right: 30px">
            <a href="{{ url('/') }}/admin/users/{{$user_detail->id}}/edit" class="btn btn-sm btn-default"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
      </div>
      <div class="btn-group pull-right " style="margin-right: 10px">
         <a href="{{ url('/') }}/admin/users" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
      </div>
   </div>
</div>

<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="box box-primary box-solid" >
            <div class="box-header with-border" style="text-align: center;">
               <h3 class="box-title"> View {{$user_detail->user_first_name}} </h3>
               <div class="box-tools pull-right">
               </div>
               <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                <table class="table">
                  <tbody>

                       <tr>
                         <th> First Name</th>
                         @if($user_detail->user_first_name!="")
                         <td>{{$user_detail->user_first_name}}</td> 
                         @else
                         <td><b>...</b></td>
                         @endif
                       </tr> 
                        <tr>
                         <th> Last Name</th>
                          @if($user_detail->user_last_name!="")
                         <td>{{$user_detail->user_last_name}}</td> 
                         @else
                         <td><b>...</b></td>
                         @endif
                       </tr>
                          <tr>
                         <th> Email</th>
                          @if($user_detail->user_email!="")
                         <td>{{$user_detail->user_email}}</td> 
                         @else
                         <td><b>...</b></td>
                         @endif
                       </tr>
                          <tr>
                         <th>Mobile Number</th>
                          @if($user_detail->user_phone_number!="")
                         <td>{{$user_detail->user_phone_number}}</td>
                          @else
                         <td><b>...</b></td>
                         @endif
                        </tr>
                          <tr>
                        <th>Profile Picture</th>

                        @if($user_detail->user_profile_image!="")
                        <td><img src="http://supremeseafoods.dci.in/uploads/{{$user_detail->user_profile_image}}"width="250px" height="250px"/></td>
                        @else
                        <td><b>No Profile Picture</b></td>
                        @endif
                     </tr>
                        <tr>
                         <th> Community</th>
                        
                         @if($user_detail->user_community==0)
                         <td>Hindu</td> 
                         @elseif($user_detail->user_community==1)
                          <td>Muslim</td> 
                         @elseif($user_detail->user_community==2)
                         <td>christian</td>
                         @endif

                        </tr>
                        <tr>
                         <th>Status</th>
                        <td>{{($user_detail->user_status == 1) ? "Active" : "InActive"}}</td> 
                        </tr>
                        <tr>
                        <th>Created Date</th>
                        <td><?php echo date_format($user_detail->created_at,"M d, Y");?></td>
                     </tr> 
                     <tr>
                        <th>Modified Date</th>
                        <td><?php echo date_format($user_detail->updated_at,"M d, Y");?></td>
                     </tr>

					        
                    
                   
                    
                  </tbody>
               </table>
            </div>
            <!-- /.box-body -->
         </div>
      </div>
    </div>
</section>
