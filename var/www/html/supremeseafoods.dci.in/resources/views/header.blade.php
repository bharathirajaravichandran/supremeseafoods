<!--
author: layouts
author URL: http://layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Supreme Seafoods</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Supreme seafoods" />

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">  

<style>
 .ui-autocomplete-loading {
    background: white url("/images/loader1.gif") right center no-repeat;
  }
.ui-state-active h4,
.ui-state-active h4:visited {
    color: black ;
}

.ui-menu-item{
    height: 80px;
    border: 1px solid #ececf9;
}
.ui-widget-content .ui-state-active {
    background-color: white !important;
    border: none !important;
    color:black;
}
.list_item_container {
    width:498px;
    height: 60px;
    float: left;
    color:black;
    margin-left: 20px;
}
.ui-widget-content .ui-state-active .list_item_container {
    background-color: #f5f5f5;
}

.image {
    width: 15%;
    float: left;
    padding: 10px;
}
.image img{
    width: 60px;
    height : 40px;
}
.label{
    width: 85%;
    white-space: nowrap;
    overflow: hidden;
    color:black;
    text-align: left;
}
input:focus{
    background-color: #f5f5f5;
}
.ui-widget.ui-widget-content {  height: 250px;    overflow: hidden;    overflow-y: auto; }
#ui-id-1 { 	  max-width:535px;  height: 250px auto !important;  overflow: hidden;  overflow-y: auto; }
#ui-id-1 .ui-menu-item {  height:auto;   display: inline-block;  width: 100%;  float: left; }
#ui-id-1 .ui-menu-item .list_item_container .label { text-align:left; display: table; height: 55px; }
#ui-id-1 .ui-menu-item .list_item_container .label h4  { font-size:15px; color:#000;  display: table-cell; vertical-align: middle; }
#ui-id-1 .ui-menu-item .ui-menu-item-wrapper { padding:0px; }
#ui-id-1 .ui-menu-item .list_item_container .label:hover h4 { color:	rgb(0, 82, 204); }  

</style>




<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script class="jsbin" src="/js/jquery-ui1.min.js"></script>
<script class="jsbin" src="/js/jquery-ui1.8.0.min.js"></script>

<!-- //for-mobile-apps -->
<link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/supreme_seafoods.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/seafoods_innerpage.css" rel="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- font-awesome icons -->
<link href="/css/font-awesome.css" rel="stylesheet"> 
<link href="/css/easy-responsive-tabs.css" rel="stylesheet">
<link href="/css/owl.carousel.css" rel="stylesheet">
<link href="/css/jquery.bxslider.css" rel="stylesheet">


<!-- //font-awesome icons -->
<!-- js -->
<script src="/js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="/js/move-top.js"></script>
<script type="text/javascript" src="/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- header -->
	<div class="agileits_header">
		<div class="container">
			<div class="l_offers col-sm-7 cl-xs-12">
				<p>Live Murrel ( Viral meen ) , Original Seabass ( Asal Koduva ) , Emperor , Seer Fish Big & Small</p>
			</div>
			<div class="agile-login col-sm-5 col-xs-12 text-right">
				
				<ul>
					<li><a href="tel:044-66442222"><i class="fa fa-phone" aria-hidden="true"></i> 044-66442222 </a></li>

					@guest
					<li><a href="{{ url('signup') }}"><i class="fa fa-sign-in" aria-hidden="true"></i>LOGIN / REGISTER</a></li>
					@else
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true"></i>{{ Auth::user()->user_first_name }}<b class="caret"></b></a>
						<ul class="dropdown-menu multi-column columns-3">
							<div class="row">
								<div class="multi-gd-img">
									<ul class="multi-column-dropdown">
										<li id="li1">
											<a href="{{ url('detail')}}"><i class="fa fa-user" aria-hidden="true"></i> User Details</a>
										</li>

										<li id="li1">
											<a href="{{url('wishlist')}}"><i class="fa fa-heart" aria-hidden="true"></i> Wishlist</a>
										</li>
										<li id="li1"><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> 
										Logout
										
											{!! Form::open(['url' => 'logout','method' => 'post','id'=>'logout-form','style'=>'display: none;']) !!}

										</a></li>
										</ul>
									</div>	
									
								</div>
							</ul>
						</li>

					@endguest
				</ul>
			</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>

	<div class="logo_products">
		<div class="container">
			<div class="ls_logo_products_left text-left col-sm-4 col-xs-12">
				<h1><a href="{{ url('/') }}"><img src="/images/supreme-logo.png" alt="Supreme Seafoods"/></a></h1>
			</div>
            
            <div class="search-icon col-sm-7 text-right" id="search-button"><img src="/images/search-icons.png" alt="Search" />
		<div class="l_search" style="display: none;">
		
				<input type="search" name="Search" id="searchname" placeholder="Search for a Product..." required="">
				
			
				<div class="clearfix"></div>

		</div>
        </div>
        <div class="product_list_header col-sm-2 text-right">  
					<form action="#" method="post" class="last"> 
						<input type="hidden" name="cmd" value="_cart">
						<input type="hidden" name="display" value="1">
					<a href="{{url('cartdetail')}}"  class="cart-color">MY CART (<span id="head_cart_count">{{Cart::count()}}</span>) <img src="/images/cart-icon.png" alt="mini-cart" /></a>
					</form>  
			</div>
			
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
<!-- navigation -->
	<div class="navigation-agileits">
		<div class="container">
			<nav class="navbar navbar-default">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav">
									<li class="{{ (Request::segment(1)== '')  ?  'active'  : '' }}" ><a href="{{ url('/') }}" class="act">Home</a></li>	
									<!-- Mega Menu -->
									@if(count(@$categories) !=0)
									@foreach($categories as $category)
									<li  class="{{(Request::segment(2) == $category->catname ) ? 'active' : '' }}" ><a href="{{ url('category'.'/'.preg_replace('/[^A-Za-z0-9\-]/','', $category->catname).'/'.$category->id)}}" class="act">{{$category->catname}}</a></li>
									@endforeach   
									@endif 
									<li><a href="#">RECIPES</a></li>
									<li><a href="#">TESTIMONIALS</a></li>
   									<li><a href="#">WHAT’S AVAILABLE TODAY?</a></li>
   									
								</ul>
							</div>
							</nav>
			</div>
		</div>
		
<!-- //navigation -->

<script src="https://code.jquery.com/jquery-1.10.2.js"></script>  
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
<script type="text/javascript">
	(function ($) {
$(document).ready(function() {
    $('#searchname').autocomplete({

      source : '/autocomplete', // <-- this should be sufficient
     minLength:1,
      		
     //~ focus: function( event, ui ) {
		 //~ 
         //~ $( "#searchname" ).val( ui.item.value );               
         //~ },
      select: function(event,ui){
		  if (ui.item.value != 'No result Found') {
        window.location.href = ui.item.url;
			}
      }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		
        if (item.image)
        {
        return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append('<a href="' + item.url + '" ><div class="list_item_container"><div class="image"><img src="' + item.image + '" ></div><div class="label"><h4><b>' + item.value + '</b></h4></div></div></a>')
                .appendTo(ul);
		}
		else
		{
        return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append('<a><div class="list_item_container"><div class="label"><h4><b>' + item.value + '</b></h4></div></div></a>')
                .appendTo(ul);
		}
    };
  });
})(jQuery)

</script>
