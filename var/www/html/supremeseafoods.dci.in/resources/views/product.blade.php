
    <section class="content">

                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/addproduct" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    @if (count($errors) > 0)
	   <div class="alert alert-danger alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		  <ul>
			 @foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			 @endforeach
		  </ul>
	   </div>
	@endif
	
            <form id="prod_from" action="{{ url('/') }}/admin/saveprd" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container>
    
        <div class="box-body">

                            <div class="fields-group">

                                                                        <div class="form-group  ">

<label for="product_type " class="col-sm-2 control-label">Product Section</label>

    <div class="col-sm-8">

        

        <select onchange="showdisplay(this.value);" class="form-control product_type " style="width: 100%;" id="product_type" name="product_type"  >
                            <option value=""></option>
                                    <option value="1" >Combo</option>
                                    <option value="0" selected>Individual</option>
                                    </select>

        
    </div>
</div>










                                                    <div id="prod_name" class="form-group  ">

    <label for="productname" class="col-sm-2 control-label">Product Name</label>

    <div id="prod_name_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="productname" name="productname" value="" class="form-control productname" placeholder="Input Product Name" />

            
        </div>

        
    </div>
</div>


<div id="prod_name" class="form-group  ">

    <label for="productdescription" class="col-sm-2 control-label">Product Description</label>

    <div id="prod_name_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>

                        <textarea id="product_description" name="product_description" value="" class="form-control productname" placeholder="Input Product Description">
                        </textarea>
                        
        </div>

        
    </div>
</div>




	<div id="prod_cat" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Categories</label>
		<div id="prod_cat_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_categories" name="product_categories[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($categories as $k=>$v)

						<option value="{{ $v->id }}">{{ $v->catname }}</option>
			@endforeach
			</select>
		</div>
	</div>
	
	<div id="prod_type" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Types</label>
		<div id="prod_type_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_types" name="product_types[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($types as $k=>$v)
						<option value="{{ $k }}">{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
	
	<div id="prod_reference" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Preference</label>
		<div id="prod_reference_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_preferences" name="product_preferences[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($preference as $k=>$v)
						<option value="{{ $k }}">{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>

	<!-- <div id="prod_reference" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Type of cuts</label>
		<div id="type_of_cut" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="type_of_cut" name="type_of_cut[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@if(@$types_cuts !='')
			@foreach (@$types_cuts as $k=>$vv)
						<option value="{{ @$k }}">{{ @$vv }}</option>
			@endforeach
			@endif
			</select>
		</div>
	</div>
 -->



                                                    <div id="prod_sku" class="form-group  ">

    <label for="product_sku" class="col-sm-2 control-label">Product SKU</label>

    <div id="prod_sku_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="product_sku" name="product_sku" value="" class="form-control product_sku" placeholder="Input Product SKU" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_tags" class="form-group  ">

    <label for="product_tags" class="col-sm-2 control-label">Product Tags</label>

    <div id="prod_tags_inner" class="col-sm-8">

        
        <select  id="product_tags1" class="form-control product_tags" style="width: 100%;" name="product_tags[]" multiple="multiple" data-placeholder="Input Product Tags"  >
                    </select>
        <input type="hidden" id="product_tags" name="product_tags[]" />

        
    </div>
</div>

                                                    <div style="display:none;" id="prod_price" class="form-group  ">

    <label for="combo_price" class="col-sm-2 control-label">Price</label>

    <div id="prod_price_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="combo_price" name="combo_price" value="" class="form-control combo_price" placeholder="Input Price" />

            
        </div>

        
    </div>
</div>



 <div class="form-group  ">
<label for="product_type " class="col-sm-2 control-label">Product Quantity (Kg,pocket) </label>
    <div class="col-sm-8">    

        <select class="form-control product_type " style="width: 100%;" id="product_quantity_type" name="product_quantity_type">                   
                    <option value="kg">Kg</option>
                    <option value="pocket">Pocket</option>
        </select>

        
    </div>
</div>




<span id="close_div">
    <div id="prod_min" class="form-group  ">

    <label for="min_quantity" class="col-sm-2 control-label">Minimum Quantity</label>

    <div id="prod_min_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="min_quantity" name="min_quantity" value="" class="form-control min_quantity" placeholder="Input Minimum Quantity" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_max" class="form-group  ">

    <label for="max_quantity" class="col-sm-2 control-label">Maximum Quantity</label>

    <div id="prod_max_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="max_quantity" name="max_quantity" value="" class="form-control max_quantity" placeholder="Input Maximum Quantity" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_whole" class="form-group  ">

    <label for="whole_price" class="col-sm-2 control-label">Whole Price</label>

    <div id="prod_whole_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="whole_price" name="whole_price" value="" class="form-control whole_price" placeholder="Input Whole Price" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_cleaned" class="form-group  ">

    <label for="cleaned_price" class="col-sm-2 control-label">Cleaned Price</label>

    <div id="prod_cleaned_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="cleaned_price" name="cleaned_price" value="" class="form-control cleaned_price" placeholder="Input Cleaned Price" />

            
        </div>

        
    </div>
</div>
                                                    </span>
                                                    
                                                    
                                                    <div id="prod_wholesale" class="form-group  ">

    <label for="whole_sale" class="col-sm-2 control-label">Whole sale</label>

    <div id="prod_wholesale_inner" class="col-sm-8">

        
        <input type="checkbox" class="whole_sale la_checkbox"   />
        <input type="hidden" class="whole_sale" name="whole_sale" class="" value="off" />

        
    </div>
</div>

              

                    <div id="related product" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">related product</label>
		<div id="prod_reference_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="related_product" name="related_product[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($tab as $k=>$v)
						<option value="{{ $k }}">{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
                    <div id="related product" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label"> productstate</label>
		<div id="prod_reference_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="related_product" name="productstate[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($stat as $k=>$v)
						<option value="{{ $k }}">{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
                    <div id="cuts product" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label"> product cuts</label>
		<div id="prod_reference_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_cuts" name="product_cuts[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($cutt as $k=>$v)
						<option value="{{ $k }}">{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
                                                    <div id="prod_mainimage" class="form-group  ">

    <label for="productimages_product_image" class="col-sm-2 control-label">Main Image</label>

    <div id="prod_mainimage_inner" class="col-sm-8">

        
        <input id="prod_images" type="file"  class="productimages_product_image_" name="product_image"  />

        
    </div>
</div>

                                                    <div id="prod_otherimage" class="form-group  ">

    <label for="productimages_multiple_image" class="col-sm-2 control-label">Other Product Images</label>

    <div id="prod_otherimage_inner" class="col-sm-8">

        
<!--
        <input id="prod_multiple_images" type="file" data-initial-preview="http://devram.com:8000/uploads/public/category_images/538d3f3d5d86839078d273ba17bb769c.jpg" data-initial-caption="538d3f3d5d86839078d273ba17bb769c.jpg" class="productimages_multiple_image_" name="multiple_image[]" multiple="1" />        
-->
        
        <input id="prod_multiple_images" type="file" class="productimages_multiple_image_" name="multiple_image[]" multiple="1" />

        
    </div>
</div>

                                                    <div class="form-group  ">

<label for="status" class="col-sm-2 control-label">Status</label>

    <div class="col-sm-8">

        

        <select class="form-control status" style="width: 100%;" name="status"  >
                            <option value=""></option>
                                    <option value="1" >Active</option>
                                    <option value="0" selected>In Active</option>
                                    </select>

        
    </div>
</div>




                </div>
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <div class="btn-group pull-right">
    <button type="button" name="submit" onclick="validation();" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
</div>

                <div class="btn-group pull-left">
    <button type="reset" class="btn btn-warning">Reset</button>
</div>

            </div>

        </div>

        
        <!-- /.box-footer -->
    </form>
</div>

</div></div>

    </section>
    
    
<script>

function validation()
{
	var product_name=$('#productname').val();

product_name=product_name.trim();

	var err='';
	if(product_name=='')
	{
		$('#prod_name').addClass('has-error');
		$('#prod_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Name field is required.</label>');
		if(err=='')
		{
			$('#productname').focus();
			err='set';
		}
	}
	if(product_name!='')
	{
		if(((product_name.length) < 5) || ((product_name.length)>50))
		{
			$('#prod_name').addClass('has-error');
			$('#prod_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Name should be minimum Of 5 characters and a maximum of 50 characters.</label>');
			if(err=='')
			{
				$('#productname').focus();
				err='set';
			}
		}
	}
	//////////////////////Product SKU////////////////////////////////////	
	var product_sku=$('#product_sku').val();
	product_sku=product_sku.trim();
	//var err='';
	if(product_sku=='')
	{
		$('#prod_sku').addClass('has-error');
		$('#prod_sku_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product SKU field is required.</label>');
		if(err=='')
		{
			$('#product_sku').focus();
			err='set';
		}
	}
	if(product_sku!='')
	{
		if(((product_sku.length) < 2) || ((product_sku.length)>10))
		{
			$('#prod_sku').addClass('has-error');
			$('#prod_sku_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product SKU shoule be minimum Of 2 characters and a maximum Of 10 characters.</label>');
			if(err=='')
			{
				$('#product_sku').focus();
				err='set';
			}
		}
	}
	
	//////////////////////Product Tags////////////////////////////////////	
	var product_tags=$('#product_tags1').val();
		//product_tags=product_tags.trim();
	//var err='';
	if(product_tags=='')
	{
		$('#prod_tags').addClass('has-error');
		$('#prod_tags_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Tag field is required.</label>');
		if(err=='')
		{
			$('#product_tags1').focus();
			err='set';
		}
	}	

	var val_product_type=$('#product_type').val();
	
	
	if(val_product_type==1)
	{
		//////////////////////Combo price////////////////////////////////////	
		var prod_price=$('#combo_price').val();
		prod_price=prod_price.trim();
		//var err='';
		if(prod_price=='')
		{
			$('#prod_price').addClass('has-error');
			$('#prod_price_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product price field is required.</label>');
			if(err=='')
			{
				$('#combo_price').focus();
				err='set';
			}
		}	
		if(prod_price!='')
		{
			if(!$.isNumeric(prod_price))
			{
				$('#prod_price').addClass('has-error');
				$('#prod_price_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product price field shoule be numeric.</label>');
				if(err=='')
				{
					$('#combo_price').focus();
					err='set';
				}
			}
		}	
	
	}


	if(val_product_type==0)
	{
	//////////////////////minimum quantity////////////////////////////////////	
	var min_quantity=$('#min_quantity').val();
	min_quantity=min_quantity.trim();	
	//var err='';
	if(min_quantity=='')
	{
		$('#prod_min').addClass('has-error');
		$('#prod_min_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Minimum Quantity field is required.</label>');
		if(err=='')
		{
			$('#min_quantity').focus();
			err='set';
		}
	}
	if(min_quantity!='')
	{
		if(!$.isNumeric(min_quantity))
		{
			$('#prod_min').addClass('has-error');
			$('#prod_min_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Minimum Quantity field should be numeric.</label>');
			if(err=='')
			{
				$('#min_quantity').focus();
				err='set';
			}
		}
	}	

	//////////////////////maximum quantity////////////////////////////////////	
	var max_quantity=$('#max_quantity').val();
	max_quantity=max_quantity.trim();		
	//var err='';
	if(max_quantity=='')
	{
		$('#prod_max').addClass('has-error');
		$('#prod_max_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Maximum Quantity field is required.</label>');
		if(err=='')
		{
			$('#max_quantity').focus();
			err='set';
		}
	}
	if(max_quantity!='')
	{
		if(!$.isNumeric(max_quantity))
		{
			$('#prod_max').addClass('has-error');
			$('#prod_max_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Maximum Quanity field should be numeric.</label>');
			if(err=='')
			{
				$('#max_quantity').focus();
				err='set';
			}
		}
	}
	
	//////////////////////Whole Price////////////////////////////////////	
	var whole_price=$('#whole_price').val();
	whole_price=whole_price.trim();	
	//var err='';
	if(whole_price=='')
	{
		$('#prod_whole').addClass('has-error');
		$('#prod_whole_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Whole Price field is required.</label>');
		if(err=='')
		{
			$('#whole_price').focus();
			err='set';
		}
	}
	if(whole_price!='')
	{
		if(!$.isNumeric(whole_price))
		{
			$('#prod_whole').addClass('has-error');
			$('#prod_whole_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Whole Price field should be numeric.</label>');
			if(err=='')
			{
				$('#whole_price').focus();
				err='set';
			}
		}
	}
	

	
	//////////////////////Cleaned Price////////////////////////////////////	
	var cleaned_price=$('#cleaned_price').val();
	cleaned_price=cleaned_price.trim();	
	//var err='';
	if(cleaned_price=='')
	{
		$('#prod_cleaned').addClass('has-error');
		$('#prod_cleaned_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Cleaned Price field is required.</label>');
		if(err=='')
		{
			$('#cleaned_price').focus();
			err='set';
		}
	}
	if(cleaned_price!='')
	{
		if(!$.isNumeric(cleaned_price))
		{
			$('#prod_cleaned').addClass('has-error');
			$('#prod_cleaned_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Cleaned Price field should be numeric.</label>');
			if(err=='')
			{
				$('#cleaned_price').focus();
				err='set';
			}
		}
	}


}




	//////////////////////Product Categories////////////////////////////////////	
	var product_categories=$('#product_categories').val();
	//var err='';
	if(product_categories==null)
	{
		$('#prod_cat').addClass('has-error');
		$('#prod_cat_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Categories field is required.</label>');
		if(err=='')
		{
			$('#product_categories').focus();
			err='set';
		}
	}
	
	//////////////////////Product Types////////////////////////////////////	
	var product_types=$('#product_types').val();
	//var err='';
	if(product_types==null)
	{
		$('#prod_type').addClass('has-error');
		$('#prod_type_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Type field is required.</label>');
		if(err=='')
		{
			$('#product_types').focus();
			err='set';
		}
	}
	
	//////////////////////Product Preferences////////////////////////////////////	
	var product_preferences=$('#product_preferences').val();
	//var err='';
	if(product_preferences==null)
	{
		$('#prod_reference').addClass('has-error');
		$('#prod_reference_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Preferences field is required.</label>');
		if(err=='')
		{
			$('#product_preferences').focus();
			err='set';
		}
	}






	//////////////////////Cleaned Price////////////////////////////////////	
	var prod_images=$('#prod_images').val();
	prod_images=prod_images.trim();		
	//var err='';
	if(prod_images=='')
	{
		$('#prod_mainimage').addClass('has-error');
		$('#prod_mainimage_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Image field is required.</label>');
		if(err=='')
		{
			$('#prod_images').focus();
			err='set';
		}
	}

	if(err!='')
	{
		return false;
	}
	else
	{
		$('#prod_from').submit();
	}
}




</script>    
    
    
    
        <script data-exec-on-popstate>

    $(function () {
                    $('.form-history-back').on('click', function (event) {
    event.preventDefault();
    history.back(1);
});
                    //$(".product_type ").select2({"allowClear":true,"placeholder":"Product Type"});
                    $(".product_tags").select2({
            tags: true,
            tokenSeparators: [',']
        });
                    
$('.whole_sale.la_checkbox').bootstrapSwitch({
    size:'small',
    onText: 'ON',
    offText: 'OFF',
    onColor: 'primary',
    offColor: 'default',
    onSwitchChange: function(event, state) {
        $(event.target).closest('.bootstrap-switch').next().val(state ? 'on' : 'off').change();
    }
});

                    
$("input.productimages_product_image_").fileinput({"overwriteInitial":false,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":false,"showUpload":false,"deleteExtraData":{"productimages[product_image]":"_file_del_","_file_del_":"","_token":"APPpllRt6toGIcWrWjJvYLGUSzim24djnSaEN9Yw","_method":"PUT"},"deleteUrl":"http:\/\/devram.com:8000\/admin\/","allowedFileTypes":["image"],"allowedFileExtensions": ["jpg", "png", "gif"]});

                    $("input.productimages_multiple_image_").fileinput({"overwriteInitial":false,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":false,"showUpload":false,"deleteExtraData":{"productimages[multiple_image]":"_file_del_","_file_del_":"","_token":"APPpllRt6toGIcWrWjJvYLGUSzim24djnSaEN9Yw","_method":"PUT"},"deleteUrl":"http:\/\/devram.com:8000\/admin\/","allowedFileTypes":["image"],"allowedFileExtensions": ["jpg", "png", "gif"]});
                    //$(".status").select2({"allowClear":true,"placeholder":"Status"});
            });
            
                               // $("#product_categories").select2({"allowClear":true,"placeholder":"Product Categories"});
                               // $("#product_types").select2({"allowClear":true,"placeholder":"Product Types"});
                               // $("#product_preferences").select2({"allowClear":true,"placeholder":"Product Preferences"});
                               
                               

function showdisplay(vals)
{
	if(vals==0)
	{
		$('#prod_price').hide();
		$('#close_div').show();
	}
	if(vals==1)
	{
		$('#close_div').hide();
		$('#prod_price').show();
	}
}

</script>

