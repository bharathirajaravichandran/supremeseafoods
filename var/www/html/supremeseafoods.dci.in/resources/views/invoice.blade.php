 <div class="row">
	 <div class="col-md-12">
		 <div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">&nbsp;</h3>

				<div class="box-tools">
					<div class="btn-group pull-right" style="margin-right: 10px">
						<a href="{{ url('/') }}/admin/invoices" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
					</div> 
					<div class="btn-group pull-right" style="margin-right: 10px">
						<a href="{{ url('/') }}/admin/invoices" class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
					</div>
				</div>


<section class="invoice" id="printableArea">
   @if(count($order) !=0)
      <!-- title row --> 
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> Supreme Sea Foods
            <small class="pull-right">Date: {{ \Carbon\Carbon::parse($order[0]->createddatetime)->format('d/m/Y')}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>Supreme Sea Foods.</strong><br>
            Address Line 1,<br>
            Address Line 2,<br>
            Phone: 9894425571<br>
            Email: info@supremeseafoods.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong>{{ $order[0]->user_first_name }}</strong><br>
            {{ $order[0]->address }}<br>
            {{ $order[0]->city }}, {{ $order[0]->state }} {{ $order[0]->country }}<br>
            Phone: {{ $order[0]->phone }}<br>
            
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <br>
          <br>
          <b>Order ID:</b> {{ $order[0]->order_id }}<br>
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Product Price (Rs)</th>
              <th>Cuts</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
			@foreach ($order as $order_item)	
            <tr>
              <td>{{$order_item->quantity}} Kg(s)</td>
              <td>{{$order_item->productname}}</td>
              <td>&#8377;{{$order_item->product_cost}}</td>
              <td>{{$order_item->type_cuts}}</td>
              <td>&#8377;{{$order_item->quantity * $order_item->product_cost}}</td>
            </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Method:</p>
          
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           {{ $order[0]->mode }}
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
         
          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td>&#8377;{{ $subTotal }}</td>
              </tr>
              <tr>
                <th>CGST (0%)</th>
                <td>&#8377;0</td>
              </tr>
              <tr>
                <th>SGST (0%)</th>
                <td>&#8377;0</td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>&#8377;{{ $netTotal }}</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#1" onclick="printDiv('printableArea')" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
        
        </div>
      </div>
      @else
              Some values Missing Inside Invoice Data  
      @endif
    </section>

			</div>
		</div>
   	</div>
</div>		
<script>
function printDiv(divName) {
    
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
}
</script>
