<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Admin::title() }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/dist/css/skins/" . config('admin.skin') .".min.css") }}">

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/laravel-admin/laravel-admin.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/nprogress/nprogress.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/sweetalert/dist/sweetalert.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/nestable/nestable.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/google-fonts/fonts.css") }}">
    <link rel="stylesheet" href="{{ admin_asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}">

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/AdminLTE/dist/js/app.min.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/jquery-pjax/jquery.pjax.js") }}"></script>
    <script src="{{ admin_asset ("/vendor/laravel-admin/nprogress/nprogress.js") }}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">
<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        @yield('content')
        {!! Admin::script() !!}
    </div>

    @include('admin::partials.footer')

</div>

<!-- ./wrapper -->

<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="{{ admin_asset ("/vendor/laravel-admin/nestable/jquery.nestable.js") }}"></script>
<script src="{{ admin_asset ("/vendor/laravel-admin/toastr/build/toastr.min.js") }}"></script>
<script src="{{ admin_asset ("/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js") }}"></script>
<script src="{{ admin_asset ("/vendor/laravel-admin/sweetalert/dist/sweetalert.min.js") }}"></script>
{!! Admin::js() !!}
<script src="{{ admin_asset ("/vendor/laravel-admin/laravel-admin/laravel-admin.js") }}"></script>

<script>
					  $(function () {
						"use strict";
						// AREA CHART
						var area = new Morris.Area({
						  element: 'revenue-chart',
						  resize: true,
						  data: [
							{y: '2011 Q1', item1: 2666, item2: 2666},
							{y: '2011 Q2', item1: 2778, item2: 2294},
							{y: '2011 Q3', item1: 4912, item2: 1969},
							{y: '2011 Q4', item1: 3767, item2: 3597},
							{y: '2012 Q1', item1: 6810, item2: 1914},
							{y: '2012 Q2', item1: 5670, item2: 4293},
							{y: '2012 Q3', item1: 4820, item2: 3795},
							{y: '2012 Q4', item1: 15073, item2: 5967},
							{y: '2013 Q1', item1: 10687, item2: 4460},
							{y: '2013 Q2', item1: 8432, item2: 5713}
						  ],
						  xkey: 'y',
						  ykeys: ['item1', 'item2'],
						  labels: ['Item 1', 'Item 2'],
						  lineColors: ['#a0d0e0', '#3c8dbc'],
						  hideHover: 'auto'
						});
						// LINE CHART
						var line = new Morris.Line({
						  element: 'line-chart',
						  resize: true,
						  data: [
							{y: '2011 Q1', item1: 2666},
							{y: '2011 Q2', item1: 2778},
							{y: '2011 Q3', item1: 4912},
							{y: '2011 Q4', item1: 3767},
							{y: '2012 Q1', item1: 6810},
							{y: '2012 Q2', item1: 5670},
							{y: '2012 Q3', item1: 4820},
							{y: '2012 Q4', item1: 15073},
							{y: '2013 Q1', item1: 10687},
							{y: '2013 Q2', item1: 8432}
						  ],
						  xkey: 'y',
						  ykeys: ['item1'],
						  labels: ['Item 1'],
						  lineColors: ['#3c8dbc'],
						  hideHover: 'auto'
						});
					 });
              </script>

</body>
</html>
