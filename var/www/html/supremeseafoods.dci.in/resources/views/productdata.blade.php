<style type="text/css">
#agent_div_inner { text-align: center;font-weight: bold;font-size: 23px; color: red;border: 2px solid red;border-radius: 10px;padding: 10px 0px; }
</style>

<div id="dummy">

@if($count_catd!=0)
	
	<div class="form-group col-sm-12">
		<div class="col-sm-10" >
		<div class="text-left"  style="margin-left: 30px;">
		<label class="checkbox-label"> <strong>Select All</strong> </label>
		<input style="position: relative;top: 10px;" type="checkbox" id="checks" class="checks" name="checks" value="1" onclick="checking();"class="form-control"  />
	</div>
	</div>
	</div>

@foreach($catd as $k=>$v)
 				<div class="form-inline" style="display: inline-block;width: 100%;height: 70px;">
						<div id="prod_div_{{$k}}" class="col-sm-12" style="padding: 0px;">
							<div class="col-sm-12">
							<div class="col-sm-4" style="text-align: right;display: inline-block;">
							</div>
					 		<div class="col-sm-3" style="display: inline-block;">
					 			<div id="price_error_{{$k}}"></div>
							</div>
							<div class="col-sm-3" style="display: inline-block;">
					 			<div id="time_error_{{$k}}"></div>
							</div>
						</div>

							<div class="col-sm-12">
							<div class="col-sm-4" style="text-align: right;display: inline-block;min-height:28px;">
								<label class="checkbox-label1 control-label">{{$v->productname}}</label>
					 			<input  type="checkbox" id="cats_{{$k}}" class="cats_{{$k}}" name="cats_{{$k}}" value="1" class="form-control"  />
					 			
					 			<input type="hidden" id="productid_{{$k}}" class="productid_{{$k}}" name="productid[{{$k}}]" value="{{$v->product_id}}" class="form-control"  />
					 		</div>
					 				
					 		<div class="col-sm-3" style="display: inline-block;">
					 			
					 			<label  >Price</label>
								<input  type="text" id="price_{{$k}}" class="price_{{$k}}" name="price_{{$k}}" value="" class="form-control"  />
								
							</div>
							<div class="col-sm-4" style="display: inline-block;">
					 		<label style="margin-left:0px">Delivery Time</label>
					 	 	<input  type="text" id="time_{{$k}}" class="time_{{$k}}" name="time_{{$k}}" value="" class="form-control"  />
							</div>

						</div>
						 </div>
						 <br>
			    </div>

@endforeach
@else
   <div class="col-md-12">
                  <div id="agent_div_inner" class="col-sm-8">No Products</div>
               </div>
@endif
</div>
@if($count_catd!=0)

<input style="width: 110px" type="hidden" id="d_value" class="d_value" name="d_value" value="{{$k+1}}" class="form-control"  />
@endif