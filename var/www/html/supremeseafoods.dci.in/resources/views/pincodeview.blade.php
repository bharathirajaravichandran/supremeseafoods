 <section class="content">
                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">&nbsp;</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/hub" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/hub" class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->



	<div class="box-body table-responsive no-padding">
		<table class="table table-hover">
			<tr>
				<th>Hub ID</th>
				<th>Pincode</th>
				<th>Status</th>
				<th>Action</th>
			</tr>

			@foreach ($pincodes as $pincode)
			<tr >
				<td>
						<p>{{$pincode->name}}</p>
				</td>
				<td>
						<p>{{$pincode->pincode}}</p>
				</td>
				<td>
					<p>
						@if ($pincode->status === 1)
							Active
						@else
							In Active
						@endif
					</p>
				</td>
				
				<td>
				    <a href="/admin/pincodes/{{$pincode->id}}/edit"><i class="fa fa-edit"></i></a>
				    <a href="javascript:void(0);" data-id="{{$pincode->id}}" class="grid-row-delete"><i class="fa fa-trash"></i></a>
				   
                </td>
				
			</tr>
			@endforeach


		</table>
		
		{{ $pincodes->links() }}
	</div>

        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

            </div>

        </div>

</div>

</div></div>

    </section>

<script>

</script>
