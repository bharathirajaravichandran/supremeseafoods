<section class="content">
	<div class="row">
		<div class="col-md-12">
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Upload Pincodes</h3>

        <div class="box-tools">
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    
    @if (count($errors) > 0)
	   <div class="alert alert-danger alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		  <ul>
			 @foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			 @endforeach
		  </ul>
	   </div>
	@endif
	           
    
    {!! Form::open(array('url' => '/admin/pincodes/upload','files'=>'true','class'=>'form-horizontal')) !!}
    
        <div class="box-body">

    <div class="fields-group">

   <input type="hidden" value="{{ $hub_id}}" name="hub_id"/>
   
   <div class="form-group">
     <label for="name" class="col-sm-2 control-label">Pincode Upload</label>
    <div class="col-sm-8">
		 <div class="input-group">
		   	 {!! Form::file('pincode') !!}
		</div>
     </div>
	</div>
   </div>
  </div>
 <!-- /.box-body -->
 
        <div class="box-footer">

              <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <div class="btn-group pull-left">
				  
					{!! Form::submit('Submit',['class' => 'btn btn-info pull-right']) !!}
				</div>

            </div>

        </div>
                  
{!! Form::close() !!}
</div>

</div></div>

    </section>
