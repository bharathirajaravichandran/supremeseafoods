<html>
	  <head>
	  </head>
	<body>
		
<div class="row">
 <div class="col-md-12">
    <div class="box box-info">
			                    <!--box header--> 
						           <div class="box-header with-border">
						                 <h3 class="box-title">Create</h3>
						                   <div class="box-tools">
						                   </div>
						            </div>
					
				                    {!! Form::open (['url' => 'admin/ad'])!!}

			

	<div class="box-body">
	  <div class="fields-group">
		        @if (count($errors) > 0)
	   <div class="alert alert-danger alert-dismissible">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
		  <ul>
			 @foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			 @endforeach
		  </ul>
	   </div>
	@endif
		       <!--from date input-->
		       <div class="form-group clearfix">
						 <label class="col-md-3 control-label">Fromdate</label>
				       <div class="col-md-8">
							   <div class="input-group">
								 <span class="input-group-addon glyphicon glyphicon-calendar"></span> 
								  {!! Form::date('fromdate',null,['class' => 'form-control'])!!}
							    </div>
						
					  	</div>	   
	
			    </div>
		         <!--to date input-->
		         <br>
		       <div class="form-group clearfix" >
				     <label class="col-sm-3 control-label">Todate</label>
			       <div class="col-sm-8">
						<div class="input-group">


							 <span class="input-group-addon glyphicon glyphicon-calendar"></span> 
						    {!! Form::date('todate',null,['class' => 'form-control'])!!}
						</div>
					
				   </div>
		       </div>
	 	          <!--dropdowns input-->
	 	          <br>
	 	       <div class="form-group clearfix" >
			           <label  class="col-sm-3 control-label">Type of reports</label>
				   <div class="col-sm-8">
                     	<div class="input-group-md">
					       {!!Form::select('report', ['' => 'please select the type of report' , 'order ' => 'Sales reports of order more than 3000rs',  'Category' => ' Category wise sales report', 'Customer report' => ' Customer report for particular duration'],null,['class' => 'form-control '])!!}
					    </div>
				   </div>	
			 </div>
	   </div>
	</div>	    
	
	
	
			
							 <div class="box-footer">
							
									    <div class="col-md-3">
										 </div>
							            <div class="col-md-8">
							                 <div class="btn-group pull-left">
											  {!! Form::submit('Submit',['class' => 'btn btn-info pull-left']) !!}
										     </div>
										</div>
							</div>
	               {!! Form::close() !!}
	          
  

</div>
</div>
</div>

	</body>
</html>
