
    <section class="content">

                                
        <div class="row"><div class="col-md-12"><div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 10px">
    <a href="{{ url('/') }}/admin/addproduct" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
</div> <div class="btn-group pull-right" style="margin-right: 10px">
    <a class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
</div>
        </div>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
            <form id="prod_from" action="{{ url('/') }}/admin/editprd" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" pjax-container>
    
        <div class="box-body">

                            <div class="fields-group">

                                                                        <div class="form-group  ">

<label for="product_type " class="col-sm-2 control-label">Product Section</label>

    <div class="col-sm-8">

        

        <select onchange="showdisplay(this.value);" class="form-control product_type " style="width: 100%;" id="product_type" name="product_type"  >
                            <option value=""></option>
                                    <option {{ ($product->product_type==1)  ? 'selected': '' }} value="1" >Combo</option>
                                    <option {{ ($product->product_type==0)  ? 'selected': '' }} value="0" selected>Individual</option>
                                    </select>

        
    </div>
</div>










                                                    <div id="prod_name" class="form-group  ">

    <label for="productname" class="col-sm-2 control-label">Product Name</label>

    <div id="prod_name_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="productname" name="productname" value="{{$product->productname}}" class="form-control productname" placeholder="Input Product Name" />

            
        </div>

        
    </div>
</div>


<div id="prod_name" class="form-group  ">

    <label for="productdescription" class="col-sm-2 control-label">Product Description</label>

    <div id="prod_name_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>

                        <textarea id="product_description" name="product_description" value="" class="form-control productname" placeholder="Input Product Description">{{@$product->product_description}}
                        </textarea>
                        
        </div>

        
    </div>
</div>



	<div id="prod_cat" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Categories</label>
		<div id="prod_cat_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_categories" name="product_categories[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($categories as $k=>$v)
			
						<option value="{{ $k }}"  {{ (in_array($k, $cat_array)) ? 'selected': '' }} >{{ $v->catname }}</option>
			@endforeach
			</select>
		</div>
	</div>

	
	
	
	<div id="prod_type" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Types</label>
		<div id="prod_type_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_types" name="product_types[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($types as $k=>$v)
						<option value="{{ $k }}" {{ (in_array($k, $type_array)) ? 'selected': '' }}>{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
	
	<div id="prod_reference" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Product Preference</label>
		<div id="prod_reference_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="product_preferences" name="product_preferences[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($preference as $k=>$v)
						<option value="{{ $k }}" {{ (in_array($k, $pref_array)) ? 'selected': '' }}>{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>



                                                    <div id="prod_sku" class="form-group  ">

    <label for="product_sku" class="col-sm-2 control-label">Product SKU</label>

    <div id="prod_sku_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="product_sku" name="product_sku" value="{{$product->product_sku}}" class="form-control product_sku" placeholder="Input Product SKU" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_tags" class="form-group  ">

    <label for="product_tags" class="col-sm-2 control-label">Product Tags</label>

    <div id="prod_tags_inner" class="col-sm-8">

        
        <select  id="product_tags1" class="form-control product_tags" style="width: 100%;" name="product_tags[]" multiple="multiple" data-placeholder="Input Product Tags"  >
			@if (count($tags_array) > 0)
				@foreach ($tags_array as $tags_arrays)
				@if (count($tags_arrays) != '')
					<option value="{{$tags_arrays}}" selected>{{$tags_arrays}}</option>
					@endif
				@endforeach
			@endif

                    </select>
        <input type="hidden" id="product_tags" value="" name="product_tags[]" />

        
    </div>
</div>

                                                    <div style="display:none;" id="prod_price" class="form-group  ">

    <label for="combo_price" class="col-sm-2 control-label">Price</label>

    <div id="prod_price_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="combo_price" name="combo_price" value="{{$product->combo_price}}" class="form-control combo_price" placeholder="Input Price" />

            
        </div>

        
    </div>
</div>


<div class="form-group  ">
<label for="product_type " class="col-sm-2 control-label">Product Quantity (Kg,pocket) </label>
    <div class="col-sm-8">    

        <select class="form-control product_type " style="width: 100%;" id="product_quantity_type" name="product_quantity_type">                   
           <option value="kg" @if(@$product->product_quantity_type == 'kg') selected @endif> Kg</option>
           <option value="pocket" @if(@$product->product_quantity_type == 'pocket') selected @endif> Pocket</option>
        </select>

        
    </div>
</div>




<span id="close_div">
                                                    <div id="prod_min" class="form-group  ">

    <label for="min_quantity" class="col-sm-2 control-label">Minimum Quantity</label>

    <div id="prod_min_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="min_quantity" name="min_quantity" value="{{$product->min_quantity}}" class="form-control min_quantity" placeholder="Input Minimum Quantity" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_max" class="form-group  ">

    <label for="max_quantity" class="col-sm-2 control-label">Maximum Quantity</label>

    <div id="prod_max_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="max_quantity" name="max_quantity" value="{{$product->max_quantity}}" class="form-control max_quantity" placeholder="Input Maximum Quantity" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_whole" class="form-group  ">

    <label for="whole_price" class="col-sm-2 control-label">Whole Price</label>

    <div id="prod_whole_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="whole_price" name="whole_price" value="{{$product->whole_price}}" class="form-control whole_price" placeholder="Input Whole Price" />

            
        </div>

        
    </div>
</div>
                                                    <div id="prod_cleaned" class="form-group  ">

    <label for="cleaned_price" class="col-sm-2 control-label">Cleaned Price</label>

    <div id="prod_cleaned_inner" class="col-sm-8">

        
        <div class="input-group">

                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            
            <input type="text" id="cleaned_price" name="cleaned_price" value="{{$product->cleaned_price}}" class="form-control cleaned_price" placeholder="Input Cleaned Price" />

            
        </div>

        
    </div>
</div>
                                                    </span>
                                                    
                                                    
                                                    <div id="prod_wholesale" class="form-group  ">

    <label for="whole_sale" class="col-sm-2 control-label">Whole sale</label>

    <div id="prod_wholesale_inner" class="col-sm-8">

        
        <input type="checkbox"  class="whole_sale la_checkbox"  {{($product->whole_sale=='on')?'checked':''}} />
        <input type="hidden" class="whole_sale" name="whole_sale" class="" value="{{($product->whole_sale=='on')?'on':'off'}}" />

        
    </div>
</div>
          
      <div id="related product" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label">Related Product</label>
		<div id="prod_cat_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="related_product" name="related_product[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($tab as $k=>$v)
			
						<option value="{{ $k }}"  {{ (in_array($k, $rel_array)) ? 'selected': '' }} >{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
	<div id=" product state" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label"> Product state</label>
		<div id="prod_cat_inner" class="col-sm-8">
			<select class="form-control productname" style="width: 100%;" id="related_product" name="related_product[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($stat as $k=>$v)
			
						<option value="{{ $k }}"  {{ (in_array($k, $stat_array)) ? 'selected': '' }} >{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>
    <div id="cuts product" class="form-group  ">
		<label for="productname" class="col-sm-2 control-label"> product cuts</label>
		<div id="prod_reference_inner" class="col-sm-8">
			
			<select class="form-control productname" style="width: 100%;" id="related_product" name="related_product[]" multiple="multiple" data-placeholder="Input Product Categories"  >
			@foreach ($cutt as $k=>$v)
			
						<option value="{{ $k }}"  {{ (in_array($k, $cutt_array)) ? 'selected': '' }} >{{ $v }}</option>
			@endforeach
			</select>
		</div>
	</div>



                                                    <div id="prod_mainimage" class="form-group  ">

    <label for="productimages_product_image" class="col-sm-2 control-label">Main Image</label>

    <div id="prod_mainimage_inner" class="col-sm-8">

        

@if (($main_image) != '')
        <input id="prod_images" data-initial-preview="{{ url('/') }}/uploads/public/product_images/{{$main_image}}" data-initial-caption="{{$main_image}}" type="file"  class="productimages_product_image_" value="{{$main_image}}" name="product_image" /> 
@else
        <input id="prod_images" type="file"  class="productimages_product_image_" name="product_image" /> 
@endif 

       



        
    </div>
</div>

                                                    <div id="prod_otherimage" class="form-group  ">

    <label for="productimages_multiple_image" class="col-sm-2 control-label">Other Product Images</label>

    <div id="prod_otherimage_inner" class="col-sm-8">

        
<!--
        <input id="prod_multiple_images" type="file" data-initial-preview="http://devram.com:8000/uploads/public/category_images/538d3f3d5d86839078d273ba17bb769c.jpg" data-initial-caption="538d3f3d5d86839078d273ba17bb769c.jpg" class="productimages_multiple_image_" name="multiple_image[]" multiple="1" />        
-->
        
        <input id="prod_multiple_images" type="file" class="productimages_multiple_image_" name="multiple_image[]" multiple="1" />

        
    </div>
</div>

                                                    <div class="form-group  ">

<label for="status" class="col-sm-2 control-label">Status</label>

    <div class="col-sm-8">

        

        <select class="form-control status" style="width: 100%;" name="status"  >
                            <option value=""></option>
                                    <option {{($product->status=='1')?'selected':''}} value="1" >Active</option>
                                    <option {{($product->status=='0')?'selected':''}} value="0" selected>In Active</option>
                                    </select>

        
    </div>
</div>




                </div>
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{$pid}}">
                        <div class="col-md-2">

            </div>
            <div class="col-md-8">

                <div class="btn-group pull-right">
    <button type="button" name="submit" onclick="validation();" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
</div>
                <div class="btn-group pull-left">
    <button type="reset" class="btn btn-warning">Reset</button>
</div>

            </div>

        </div>

        
        <!-- /.box-footer -->
    </form>
</div>

</div></div>

    </section>
    
    
<script>

function validation()
{
	var product_name=$('#productname').val();

product_name=product_name.trim();

	var err='';
	if(product_name=='')
	{
		$('#prod_name').addClass('has-error');
		$('#prod_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Name field is required.</label>');
		if(err=='')
		{
			$('#productname').focus();
			err='set';
		}
	}
	if(product_name!='')
	{
		if(((product_name.length) < 5) || ((product_name.length)>50))
		{
			$('#prod_name').addClass('has-error');
			$('#prod_name_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Name should be minimum Of 5 characters and a maximum of 50 characters.</label>');
			if(err=='')
			{
				$('#productname').focus();
				err='set';
			}
		}
	}
	//////////////////////Product SKU////////////////////////////////////	
	var product_sku=$('#product_sku').val();
	product_sku=product_sku.trim();
	//var err='';
	if(product_sku=='')
	{
		$('#prod_sku').addClass('has-error');
		$('#prod_sku_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product SKU field is required.</label>');
		if(err=='')
		{
			$('#product_sku').focus();
			err='set';
		}
	}
	if(product_sku!='')
	{
		if(((product_sku.length) < 2) || ((product_sku.length)>10))
		{
			$('#prod_sku').addClass('has-error');
			$('#prod_sku_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product SKU shoule be minimum Of 2 characters and a maximum Of 10 characters.</label>');
			if(err=='')
			{
				$('#product_sku').focus();
				err='set';
			}
		}
	}
	
	//////////////////////Product Tags////////////////////////////////////	
	var product_tags=$('#product_tags1').val();
		//product_tags=product_tags.trim();
	//var err='';
	if(product_tags=='')
	{
		$('#prod_tags').addClass('has-error');
		$('#prod_tags_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Tag field is required.</label>');
		if(err=='')
		{
			$('#product_tags1').focus();
			err='set';
		}
	}	

	var val_product_type=$('#product_type').val();
	
	
	if(val_product_type==1)
	{
		//////////////////////Combo price////////////////////////////////////	
		var prod_price=$('#combo_price').val();
		prod_price=prod_price.trim();
		//var err='';
		if(prod_price=='')
		{
			$('#prod_price').addClass('has-error');
			$('#prod_price_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product price field is required.</label>');
			if(err=='')
			{
				$('#combo_price').focus();
				err='set';
			}
		}	
		if(prod_price!='')
		{
			if(!$.isNumeric(prod_price))
			{
				$('#prod_price').addClass('has-error');
				$('#prod_price_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product price field shoule be numeric.</label>');
				if(err=='')
				{
					$('#combo_price').focus();
					err='set';
				}
			}
		}	
	
	}


	if(val_product_type==0)
	{
	//////////////////////minimum quantity////////////////////////////////////	
	var min_quantity=$('#min_quantity').val();
	min_quantity=min_quantity.trim();	
	//var err='';
	if(min_quantity=='')
	{
		$('#prod_min').addClass('has-error');
		$('#prod_min_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Minimum Quantity field is required.</label>');
		if(err=='')
		{
			$('#min_quantity').focus();
			err='set';
		}
	}
	if(min_quantity!='')
	{
		if(!$.isNumeric(min_quantity))
		{
			$('#prod_min').addClass('has-error');
			$('#prod_min_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Minimum Quantity field should be numeric.</label>');
			if(err=='')
			{
				$('#min_quantity').focus();
				err='set';
			}
		}
	}	

	//////////////////////maximum quantity////////////////////////////////////	
	var max_quantity=$('#max_quantity').val();
	max_quantity=max_quantity.trim();		
	//var err='';
	if(max_quantity=='')
	{
		$('#prod_max').addClass('has-error');
		$('#prod_max_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Maximum Quantity field is required.</label>');
		if(err=='')
		{
			$('#max_quantity').focus();
			err='set';
		}
	}
	if(max_quantity!='')
	{
		if(!$.isNumeric(max_quantity))
		{
			$('#prod_max').addClass('has-error');
			$('#prod_max_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Maximum Quanity field should be numeric.</label>');
			if(err=='')
			{
				$('#max_quantity').focus();
				err='set';
			}
		}
	}
	
	//////////////////////Whole Price////////////////////////////////////	
	var whole_price=$('#whole_price').val();
	whole_price=whole_price.trim();	
	//var err='';
	if(whole_price=='')
	{
		$('#prod_whole').addClass('has-error');
		$('#prod_whole_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Whole Price field is required.</label>');
		if(err=='')
		{
			$('#whole_price').focus();
			err='set';
		}
	}
	if(whole_price!='')
	{
		if(!$.isNumeric(whole_price))
		{
			$('#prod_whole').addClass('has-error');
			$('#prod_whole_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Whole Price field should be numeric.</label>');
			if(err=='')
			{
				$('#whole_price').focus();
				err='set';
			}
		}
	}
	

	
	//////////////////////Cleaned Price////////////////////////////////////	
	var cleaned_price=$('#cleaned_price').val();
	cleaned_price=cleaned_price.trim();	
	//var err='';
	if(cleaned_price=='')
	{
		$('#prod_cleaned').addClass('has-error');
		$('#prod_cleaned_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Cleaned Price field is required.</label>');
		if(err=='')
		{
			$('#cleaned_price').focus();
			err='set';
		}
	}
	if(cleaned_price!='')
	{
		if(!$.isNumeric(cleaned_price))
		{
			$('#prod_cleaned').addClass('has-error');
			$('#prod_cleaned_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Cleaned Price field should be numeric.</label>');
			if(err=='')
			{
				$('#cleaned_price').focus();
				err='set';
			}
		}
	}


}




	//////////////////////Product Categories////////////////////////////////////	
	var product_categories=$('#product_categories').val();
	//var err='';
	if(product_categories==null)
	{
		$('#prod_cat').addClass('has-error');
		$('#prod_cat_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Categories field is required.</label>');
		if(err=='')
		{
			$('#product_categories').focus();
			err='set';
		}
	}
	
	//////////////////////Product Types////////////////////////////////////	
	var product_types=$('#product_types').val();
	//var err='';
	if(product_types==null)
	{
		$('#prod_type').addClass('has-error');
		$('#prod_type_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Type field is required.</label>');
		if(err=='')
		{
			$('#product_types').focus();
			err='set';
		}
	}
	
	//////////////////////Product Preferences////////////////////////////////////	
	var product_preferences=$('#product_preferences').val();
	//var err='';
	if(product_preferences==null)
	{
		$('#prod_reference').addClass('has-error');
		$('#prod_reference_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Preferences field is required.</label>');
		if(err=='')
		{
			$('#product_preferences').focus();
			err='set';
		}
	}






	//////////////////////Cleaned Price////////////////////////////////////	
	//~ var prod_images=$('#prod_images').val();
	//~ alert(prod_images);
	//~ prod_images=prod_images.trim();		
	//~ var err='';
	//~ if(prod_images=='')
	//~ {
		//~ $('#prod_mainimage').addClass('has-error');
		//~ $('#prod_mainimage_inner').prepend('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> The Product Image field is required.</label>');
		//~ if(err=='')
		//~ {
			//~ $('#prod_images').focus();
			//~ err='set';
		//~ }
	//~ }

	if(err!='')
	{
		
		return false;
	}
	else
	{
		
		$('#prod_from').submit();
	}
}




</script>    
    
    
    
        <script data-exec-on-popstate>

    $(function () {
                    $('.form-history-back').on('click', function (event) {
    event.preventDefault();
    history.back(1);
});
                    //$(".product_type ").select2({"allowClear":true,"placeholder":"Product Type"});
                    $(".product_tags").select2({
            tags: true,
            tokenSeparators: [',']
        });
                    
$('.whole_sale.la_checkbox').bootstrapSwitch({
    size:'small',
    onText: 'ON',
    offText: 'OFF',
    onColor: 'primary',
    offColor: 'default',
    onSwitchChange: function(event, state) {
        $(event.target).closest('.bootstrap-switch').next().val(state ? 'on' : 'off').change();
    }
});

                    
$("input.productimages_product_image_").fileinput({"overwriteInitial":false,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":false,"showUpload":false,"deleteExtraData":{"productimages[product_image]":"_file_del_","_file_del_":"","_token":"APPpllRt6toGIcWrWjJvYLGUSzim24djnSaEN9Yw","_method":"PUT"},"deleteUrl":"http:\/\/devram.com:8000\/uploads\/public\/product_images","allowedFileTypes":["image"],"allowedFileExtensions": ["jpg", "png", "gif"]});

@if($image_array)

                     $("input.productimages_multiple_image_").fileinput({"initialPreview":[@foreach($image_array as $k=>$v)"{{$v}}",@endforeach],"overwriteInitial":false,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":true,"showUpload":false,"deleteExtraData":{"productimages[multiple_image]":"_file_del_","_file_del_":"","_token":"APPpllRt6toGIcWrWjJvYLGUSzim24djnSaEN9Yw","_method":"PUT"},"deleteUrl":"http:\/\/devram.com:8000\/uploads\/public\/product_images","allowedFileTypes":["image"]});
@endif                    
                    
 //~ $("input.productimages_multiple_image_").fileinput({"initialPreview":["http:\/\/laravel-admin.org\/\/uploads\/images\/paintball-gallery-banner.jpg","http:\/\/laravel-admin.org\/\/uploads\/images\/paintball-gallery-banner (1).jpg","http:\/\/laravel-admin.org\/\/uploads\/images\/L550-Q3-KV-1600-x-900_305-275201_1600x900.jpg","http:\/\/laravel-admin.org\/\/uploads\/images\/2048flower_2017.jpg","http:\/\/laravel-admin.org\/\/uploads\/images\/animal3.jpg"],"overwriteInitial":false,"initialPreviewAsData":true,"browseLabel":"Browse","showRemove":false,"showUpload":false,"initialCaption":"paintball-gallery-banner.jpg,paintball-gallery-banner (1).jpg,L550-Q3-KV-1600-x-900_305-275201_1600x900.jpg,2048flower_2017.jpg,animal3.jpg","deleteExtraData":{"pictures":"_file_del_","_file_del_":"","_token":"UD8MQWexlo95PL0aM7dAWjJoKBwGtaRK16x93qsx","_method":"PUT"},"deleteUrl":"http:\/\/laravel-admin.org\/demo\/multiple-images\/13","allowedFileTypes":["image"],
//~ "layoutTemplates": {
        //~ main2: '{preview}{browse} {remove}'
    //~ },  	 
	 //~ 
	 //~ });                    
                    
                    //$(".status").select2({"allowClear":true,"placeholder":"Status"});
            });
            
                               // $("#product_categories").select2({"allowClear":true,"placeholder":"Product Categories"});
                               // $("#product_types").select2({"allowClear":true,"placeholder":"Product Types"});
                               // $("#product_preferences").select2({"allowClear":true,"placeholder":"Product Preferences"});
                               
                               

function showdisplay(vals)
{
	if(vals==0)
	{
		$('#prod_price').hide();
		$('#close_div').show();
	}
	if(vals==1)
	{
		$('#close_div').hide();
		$('#prod_price').show();
	}
}

</script>

