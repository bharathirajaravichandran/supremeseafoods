
<!--Header-->

 @include('header')


<!--
author: layouts
author URL: http://layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->

	<!-- main-slider -->
		<ul id="demo1">
			<li>
				<img src="/images/Banner-1 copy.jpg" alt="" />
				<!--Slider Description example-->
				<div class="slide-desc">
					<h3 class="s-d-title">WE CREATE DELICOUS MEMORIES</h3>
					<p class="s-d-para">Nulla facilisi. Nunc id mi enim. Phasellus tortor nisl, auctor id elit congue, consectetur pharetra leo. Suspendisse eu libero nunc.</p>
					<!--<div class="col-xs-12 col-sm-6 slider-button-set">
						<a class="col-sm-6 col-xs-12 btn s-d-read">Read more</a>
						<a class="col-sm-6 col-xs-12 btn s-d-view">VIEW VARIETIES</a>
					</div>-->
				</div>
			</li>
			<li>
				<img src="/images/22.jpg" alt="" />
				<div class="slide-desc">
						<h3 class="s-d-title">WE CREATE DELICOUS MEMORIES</h3>
						<p class="s-d-para">Nulla facilisi. Nunc id mi enim. Phasellus tortor nisl, auctor id elit congue, consectetur pharetra leo. Suspendisse eu libero nunc.</p>
						<!--<div class="col-xs-12 col-sm-6 slider-button-set">
							<a class="col-sm-6 col-xs-12 btn s-d-read">Read more</a>
							<a class="col-sm-6 col-xs-12 btn s-d-view">VIEW VARIETIES</a>
						</div>-->
					</div>
			</li>
			
			<li>
				<img src="/images/44.jpg" alt="" />
				<div class="slide-desc">
						<h3 class="s-d-title">WE CREATE DELICOUS MEMORIES</h3>
						<p class="s-d-para">Nulla facilisi. Nunc id mi enim. Phasellus tortor nisl, auctor id elit congue, consectetur pharetra leo. Suspendisse eu libero nunc.</p>
						<!--<div class="col-xs-12 col-sm-6 slider-button-set">
							<a class="col-sm-6 col-xs-12 btn s-d-read">Read more</a>
							<a class="col-sm-6 col-xs-12 btn s-d-view">VIEW VARIETIES</a>
						</div>-->
					</div>
			</li>
		</ul>
	<!-- //main-slider -->
	<!-- //top-header and slider -->
    <div class="wave-outer">
    <div class="section-wave">
          <svg x="0px" y="0px" width="1920px" height="45px" viewBox="0 0 1920 45" preserveAspectRatio="none">
            <path class="wave" d="M1920,0c-82.8,0-108.8,44.4-192,44.4c-78.8,0-116.5-43.7-192-43.7 c-77.1,0-115.9,44.4-192,44.4c-78.2,0-114.6-44.4-192-44.4c-78.4,0-115.3,44.4-192,44.4C883.1,45,841,0.6,768,0.6 C691,0.6,652.8,45,576,45C502.4,45,461.9,0.6,385,0.6C306.5,0.6,267.9,45,191,45C115.1,45,78,0.6,0,0.6V45h1920V0z"/>
          </svg>
        </div>
     </div>
     
	<!-- top-brands -->
	
		<div class="fish-from-anywhere">
        <div class="top-brands fish-from-everywhere">
            <div class="container">
                <div class="title-shadows-outer">
                	      <div class="parallax-text-wrap">
                            <h3>Fish From Everywhere For People From Anywhere</h3>                         
                          </div>
                </div>
                <div class="row owl-carousel owl-theme" id="owl_carousel_homepage">
					@if(count(@$states) !=0)
					@foreach($states as $state)
						<div class="item">
							<div class="hover14 column">
								<div class="agile_top_brand_left_grid">
									<div class="agile_top_brand_left_grid1">
										<figure>
											<div class="snipcart-item block">
												<div class="snipcart-thumb">
											
														<a href="{{url('state'.'/'.preg_replace('/[^A-Za-z0-9\-]/','', $state->state).'/'.$state->id)}}" style="background: transparent; border: 0px; outline: none">
														<img title="" alt="" src="/uploads/{{$state->image}}">
														</a>
														
													<p>{{$state->state}}</p>
												</div>
											</div>
										</figure>
									</div>
								</div>
							</div>
						</div>
                     @endforeach   
					@endif            
                                    
                </div>
        	</div>
        </div>
	</div>
    
    <section class="main-products">
        <div class="top-brands-title">    
            <div class="container">  
             <div class="title-shadows-outer">
             <div class="parallax-text-wrap">
                <h3>OUR Categories</h3>               
              </div>
             </div>
             </div>
		   </div>	
            
        <div class="product-section">
       
         <div class="container">
      		<div class="row">
				
				@if(count(@$categories) !=0)
        		  @foreach($categories as $category)
	                 
	          			<div class="col-md-4 col-sm-6">
	                    <div class="product-box"> <strong class="subheading">{{$category->catname}}</strong><h3>{{$category->catdescription}}</h3>
	                      <div class="thumb"> <img src="/uploads/{{$category->cat_image}}" class="attachment-260x160 size-260x160 wp-post-image" alt="" width="260" height="160">
	                      </div>
	                    <a href="{{ url('category'.'/'.preg_replace('/[^A-Za-z0-9\-]/','', $category->catname).'/'.$category->id)}}"><button class="btn-style-1">SEE MORE</button></a>
	                    </div>
	                </div>
	                
	        			
					@endforeach   
					@endif 

                    

            
              </div>
          </div>
  		</div>
        
	</section>
<!-- //top-brands -->


 <!-- accordian section
    ================================================== -->
    
    	<section class="section section-variant-2 bg-gray-lighter varieties-section">
        <div class="shell shell-bigger">
          <div class="range range-ten range-50 range-sm-center range-lg-justify range-md-reverse">
            <div class="container">
            <div class="row">
            <div class="col-sm-9 col-md-6 col-xl-4">

              <div class="divider divider-default"></div>
              <div class="d3-chart-wrap">
              <img src="images/accordian-image.png" width="582" height="528" />  
              </div>
            </div>
            <div class="col-sm-9 col-md-6">
           
              <div class="divider divider-default"></div>
              <!-- Bootstrap collapse-->
              <div class="panel-group panel-group-custom panel-group-corporate" id="accordion1" role="tablist" aria-multiselectable="false">
                <!-- Bootstrap panel-->
                <div class="panel panel-custom panel-corporate">
                  <div class="panel-heading" id="accordion1Heading1" role="tab">
                    <div class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion1" href="#accordion1Collapse1" aria-controls="accordion1Collapse1" aria-expanded="true" class="">100+ varieties
                        <div class="panel-arrow"><i class="material-icons add">add</i> <i class="material-icons remove">remove</i></div></a>
                    </div>
                  </div>
                  <div class="panel-collapse collapse in" id="accordion1Collapse1" role="tabpanel" aria-labelledby="accordion1Heading1" aria-expanded="true" style="">
                    <div class="panel-body">
                      <p>More than 100 varieties of Fresh , Live , Exotic , fish , prawns , crabs , fresh water fishes and imported frozen seafood available at Supreme Seafood . Now order and buy your favorite seafood from your home or office or wherever you are . We facilitate easy availability of quality seafood at the most reasonable prices to the customers at their door steps in Chennai.</p>
                    </div>
                  </div>
                </div>
                <!-- Bootstrap panel-->
                <div class="panel panel-custom panel-corporate">
                  <div class="panel-heading" id="accordion1Heading2" role="tab">
                    <div class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#accordion1Collapse2" aria-controls="accordion1Collapse2" aria-expanded="false">Fresh and Ready to cook
                        <div class="panel-arrow"><i class="material-icons add">add</i> <i class="material-icons remove">remove</i></div>
                        </a>
                    </div>
                  </div>
                  <div class="panel-collapse collapse" id="accordion1Collapse2" role="tabpanel" aria-labelledby="accordion1Heading2" aria-expanded="false">
                    <div class="panel-body">
                      <p>After you complete the payment via our secure form you will receive the instructions for downloading the product. The source files in the download package can vary based on the type of the product you have purchased.</p>
                    </div>
                  </div>
                </div>
                <!-- Bootstrap panel-->
                <div class="panel panel-custom panel-corporate">
                  <div class="panel-heading" id="accordion1Heading3" role="tab">
                    <div class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#accordion1Collapse3" aria-controls="accordion1Collapse3" aria-expanded="false">How to Order
                        <div class="panel-arrow"><i class="material-icons add">add</i> <i class="material-icons remove">remove</i></div></a>
                    </div>
                  </div>
                  <div class="panel-collapse collapse" id="accordion1Collapse3" role="tabpanel" aria-labelledby="accordion1Heading3" aria-expanded="false">
                    <div class="panel-body">
                      <p>You may build a website using the template in any way you like. You may not resell or redistribute templates (like we do); claim intellectual or exclusive ownership to any of our products, modified or unmodified. </p>
                    </div>
                  </div>
                </div>
                <!-- Bootstrap panel-->
                <div class="panel panel-custom panel-corporate">
                  <div class="panel-heading" id="accordion1Heading4" role="tab">
                    <div class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#accordion1Collapse4" aria-controls="accordion1Collapse4" aria-expanded="false">Wholesale Fish Supply
                        <div class="panel-arrow"><i class="material-icons add">add</i> <i class="material-icons remove">remove</i> </div></a>
                    </div>
                  </div>
                  <div class="panel-collapse collapse" id="accordion1Collapse4" role="tabpanel" aria-labelledby="accordion1Heading4" aria-expanded="false">
                    <div class="panel-body">
                      <p>Our templates do not include any additional scripts. Newsletter subscriptions, search fields, forums, image galleries (in HTML versions of Flash products) are inactive. Basic scripts can be easily added at www.TemplateTuning.com</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
            </div>
          </div>
        </div>
      </section>
    
    <!-- /accordian section -->	
    
    
    
    <section class="section section-variant-1 bg-white text-left about-us" id="start">
        <!-- section wave-->
        <div class="shell-wide">
          <div class="range range-lg-right range-50">
            <div class="container">
            <div class="row">
            <div class="col-sm-6 who-we-are col-xs-12">
            <div class="col-sm-8 text-center">
              <div class="parallax-text-wrap">
                <h3>WHO WE ARE</h3>
              </div>
              <hr class="divider divider-left divider-default">
            </div>
            <div class="col-sm-12">
            	<h3 class="title">A pioneer in home delivery of premium quality, chemical free, fresh seafood in India.</h3>
                <div class="who-we-are-para">
                    Supreme Seafood, is a pioneer in home delivery of fresh seafood in India . We started full fledged operations in June 2012 . We are the first and probably the only company in India to have achieved more than one lakh home deliveries in the fresh fish segment , without getting any external funding . The company is professionally run by a bunch of brothers , in-laws and nephews , literally by a family who are passionate about providing home delivery of premium quality fresh seafood .</div>
                    <div class="read-more"><a href="#" class="read-text">READ MORE</a></div>
            </div>
            </div>
            
            <div class="col-lg-6 col-sm-6 text-center col-xs-12">
              <div class="image-position-01">
                <div class="blick-wrap"><img class="image-wrap" src="images/imac.png" alt="" width="500px" height="auto">
                  <div class="blick-overlay" data-blick-overlay="ipad"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
      </section>
<!--banner-bottom-->

<!--client details-->
<section class="section bg-gray-lighter bg-additional-image bg-additional-pattern-dark testimonial-counts">
        <div class="shell-custom ">
          <div class="range range-sm-center range-md-middle range-lg-left range-0">
            <div class="cell-sm-10 cell-md-6 testimonials">
              <div class="section-lg">
                <div class="shell-custom-inner">
                <div class="parallax-text-wrap">
                <h3>WHAT OUR CLIENT SAY....</h3>
              </div>
                  <hr class="divider divider-default">
                  <!-- Slick Carousel-->
                  <div class="slider-widget">
                    <!-- Owl Carousel-->
                    <div class="owl-carousel-widget owl-carousel">
						

							@foreach($testimonials as $testimonial)

						
					 <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="\uploads\{{$testimonial->user_profile_image}}" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">{{$testimonial->user_first_name}} {{$testimonial->user_last_name}}</p>
                              <p class="quote-classic-small">{{$testimonial->user_location}}</p>
                              <p class="date pull-right">{{$testimonial->created_at}}</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>{{$testimonial->content}}</p>
                          </div>
                        </article>
                      </div>
                      

                       @endforeach

                      
<!--
                      <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="images/quote-user-1-210x210.jpg" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">Philip Lawrence</p>
                              <p class="quote-classic-small">Madurai</p>
                              <p class="date pull-right">21/02/2018</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>This template is a universal solution, which I was looking for so long. With Brave, I can now showcase all my projects and receive instant feedback from my website visitors. It is very easy to use and at the same time a very powerful product.</p>
                          </div>
                        </article>
                      </div>
                      <div class="item">
                        <article class="quote-classic">
                          <div class="quote-classic-header"><img class="quote-classic-image" src="images/quote-user-2-210x210.jpg" alt="" width="210" height="210"/>
                            <div class="quote-classic-meta">
                              <p class="quote-classic-cite">Morgan McMillan</p>
                              <p class="quote-classic-small">chennai</p>
                              <p class="date pull-right">21/02/2018</p>
                            </div>
                          </div>
                          <div class="quote-classic-text">
                            <p>Every writer needs to refresh the look of his/hers website from time to time, and that’s why I was looking for a new design for my website. I do not often choose website templates, but Brave really amazed me with its design and functionality.</p>
                          </div>
                        </article>
                      </div>
-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="cell-md-6 bg-additional-item milestones">
              <div class="section-lg">
                <div class="shell-custom-inner shell-custom-inner-1 text-center">
						<div class="parallax-text-wrap">
								<h3>MILESTONES</h3><span class="parallax-text">ACHIEVEMENTS</span>
							  </div>
                  <div class="range range-120">
                    <div class="cell-xs-6 cell-sm-3 cell-md-6">
                      <div class="counter-wrap"><i class="fa fa-twitter" aria-hidden="true"></i>
                        <div class="heading-3"><span class="counter" data-speed="4000">4000</span><span class="counter-preffix">+</span></div>
                        <p>Tweets</p>
                      </div>
                    </div>
                    <div class="cell-xs-6 cell-sm-3 cell-md-6">
                      <div class="counter-wrap"><i class="fa fa-facebook" aria-hidden="true"></i>
                        <div class="heading-3"><span class="counter" data-step="55000">55000</span><span class="counter-preffix">+</span></div>
                        <p>FB Likes</p>
                      </div>
                    </div>
                    <div class="cell-xs-6 cell-sm-3 cell-md-6">
                      <div class="counter-wrap"><i class="fa fa-home" aria-hidden="true"></i>
                         <div class="heading-3"><span class="counter" data-step="150000">150000</span><span class="counter-preffix">+</span></div>
                        <p>HOME DELIVERIES</p>
                      </div>
                    </div>
                    <div class="cell-xs-6 cell-sm-3 cell-md-6">
                      <div class="counter-wrap"><i class="fa fa-user" aria-hidden="true"></i>
                        <div class="heading-3"><span class="counter" data-step="13000">13000</span><span class="counter-preffix">+</span></div>
                        <p>Happy customers</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<!--client details end-->

<!-- Footer -->

 @include('footer')
