<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [ 
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
         'client_id' => '1998991023747012',
         'client_secret' => '491ebbe2227a26011377788a87197855',
         'redirect' => 'https://supremeseafoods.dci.in/auth/facebook/callback',
     ],
     'google' => [
         'client_id' => '94482576543-177o4jph26vrjdjmj6b1tp69sf4o8r0s.apps.googleusercontent.com',
         'client_secret' => 'Ct3-U-gVv16cYZLKmxFQWAby',
         'redirect' => 'http://supremeseafoods.dci.in/auth/callback/google',
        
	],

];
