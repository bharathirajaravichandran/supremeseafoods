<?php

namespace App\Admin\Controllers;

use App\Models\Product;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Models\Productimages;
use App\Models\Productpreferences;
use App\Models\Producttypes;
use App\Models\Productcategories;
use App\Models\Relatedproduct;
use App\Models\Productstate;
use App\Models\Productcuts;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use App\Quotation;

class ProductController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
		
        return Admin::content(function (Content $content) {

            $content->header('Products');
            $content->description('The list page of products');

            $content->body($this->grid());
            //$content->body(view('product'));
        });
    }




     
     
     
     
    public function saveprd(Request $request)
    {
		
		 $this->validate($request,[
			 'product_sku' => 'required|unique:product'
		  ]);
				
			$multiple_image=array();
			if ($request->hasFile('multiple_image')) {
				$files = $request->file('multiple_image');
				
				
				foreach($files as $file){
					$filename = $file->getClientOriginalName();
					$extension = $file->getClientOriginalExtension();
					$picture = date('His').$filename;
					$multiple_image[]=$picture;
					$destinationPath = base_path() . '/public/uploads/public/product_images';

					$file->move($destinationPath, $picture);
				}
			}
			$picture_single='';
			if ($request->hasFile('product_image')) {
				$files_single = $request->file('product_image');
				
				
					$filename = $files_single->getClientOriginalName();
					$extension = $files_single->getClientOriginalExtension();
					$picture_single = date('His').$filename;
					$destinationPath = base_path() . '/public/uploads/public/product_images';

					$files_single->move($destinationPath, $picture_single);
			}




			$product = new Product;
			$product->product_type=	$request->input('product_type');
			$product->productname=	$request->input('productname');
			$product->product_description=	$request->input('product_description');
			$product->product_sku=	$request->input('product_sku');
			$product->combo_price=	$request->input('combo_price');
			$tags=	$request->input('product_tags');
			if(count($tags))
			{
				$product->product_tags=implode(",",$tags);
			}
			
			
			$product->min_quantity=	$request->input('min_quantity');
			$product->max_quantity=	$request->input('max_quantity');
			$product->whole_sale=	$request->input('whole_sale');
			$product->whole_price=	$request->input('whole_price');
			$product->cleaned_price=	$request->input('cleaned_price');
			$productcategories=	$request->input('product_categories');
			$related_product=	$request->input('related_product');
			$productstate=	$request->input('productstate');
			$productcuts=	$request->input('product_cuts');
			$product_types=	$request->input('product_types');
			$product_preferences=	$request->input('product_preferences');
			$product->status=	$request->input('status');
			$product->createdby=	0;
			$product->updatedby=	0;
			$product->createddatetime=	Carbon::now()->toDateTimeString();

			$product->save();

			$product_id=$product->id;
			if($picture_single!='')
			{
				$productiamges = new Productimages;
				$productiamges->product_id = $product_id;
				$productiamges->product_image  = $picture_single;
				$productiamges->main_image  = 1;				
				$productiamges->status  = 1;				
				$productiamges->save();				
			}
			if(count($multiple_image))
			{
				foreach($multiple_image as $mk=>$mv)
				{
					$productiamges = new Productimages;
					$productiamges->product_id = $product_id;
					$productiamges->product_image  = $mv;
					$productiamges->main_image  = 0;					
					$productiamges->status  = 1;
					$productiamges->save();
				}
			}
			
			if(count($productcategories))
			{
				foreach($productcategories as $k=>$v)
				{
					$Productcategories = new Productcategories;
					$Productcategories->product_id = $product_id;
					$Productcategories->cat_id  = $v;
					$Productcategories->createddatetime  = Carbon::now()->toDateTimeString();
					$Productcategories->status  = 1;
					$Productcategories->save();
				}
			}

			if(count($product_types))
			{
				foreach($product_types as $k=>$v)
				{
					$Producttypes = new Producttypes;
					$Producttypes->product_id = $product_id;
					$Producttypes->type_id  = $v;
					$Producttypes->status  = 1;
					$Producttypes->save();
				}
			}
			if(count($product_preferences))
			{
				foreach($product_preferences as $k=>$v)
				{
					$Productpreferences = new Productpreferences;
					$Productpreferences->product_id = $product_id;
					$Productpreferences->preference_id  = $v;
					$Productpreferences->producttype_status  = 1;
					$Productpreferences->save();
				}
			}
			if(count($related_product))
			{
				foreach($related_product as $k=>$v)
				{
				$relatedproduct = new relatedproduct;
				$relatedproduct->product_id = $product_id;
				$relatedproduct->related_id  = $v;
				
				$relatedproduct->save();	
			}
		}
			if(count($productstate))
			{
				foreach($productstate as $k=>$v)
				{
				$productstate = new productstate;
				$productstate->product_id = $product_id;
				$productstate->state_id  = $v;
				
				$productstate->save();	
			}
		}
			if(count($productcuts))
			{
				foreach($productcuts as $k=>$v)
				{
				$productcuts = new productcuts;
				$productcuts->product_id = $product_id;
				$productcuts->cut_id  = $v;
				
				$productcuts->save();	
			}
		}
			
			 return redirect('admin/addproduct');

	}
	
    public function editprd(Request $request)
    {
		 $productcuts=	$request->input('product_cuts');
		     $productstate=	$request->input('product_state');
			$related_product=	$request->input('related_product');
			$picture_single='';
			if ($request->hasFile('product_image')) {
				//main image Productcategories::where('product_id', '=', $id)->delete();
				$files_single = $request->file('product_image');
				
				
					$filename = $files_single->getClientOriginalName();
					$extension = $files_single->getClientOriginalExtension();
					$picture_single = date('His').$filename;
					$destinationPath = base_path() . '/public/uploads/public/product_images';

					$files_single->move($destinationPath, $picture_single);
			}
				
			$multiple_image=array();
			if ($request->hasFile('multiple_image')) {
				$files = $request->file('multiple_image');
				
				
				foreach($files as $file){
					$filename = $file->getClientOriginalName();
					$extension = $file->getClientOriginalExtension();
					$picture = date('His').$filename;
					$multiple_image[]=$picture;
					$destinationPath = base_path() . '/public/uploads/public/product_images';

					$file->move($destinationPath, $picture);
				}
			}
			

			$id=$request->input('id');
			$product = Product::find($id);

			$product->product_type=	$request->input('product_type');
			$product->productname=	$request->input('productname');
			$product->product_description=	$request->input('product_description');
			$product->product_sku=	$request->input('product_sku');
			$product->combo_price=	$request->input('combo_price');
			$tags=	$request->input('product_tags');
			if(count($tags))
			{
				$product->product_tags=implode(",",$tags);
			}
			
			
			$product->min_quantity=	$request->input('min_quantity');
			$product->max_quantity=	$request->input('max_quantity');
			$product->whole_sale=	$request->input('whole_sale');
			$product->whole_price=	$request->input('whole_price');
			$product->cleaned_price=	$request->input('cleaned_price');
			$productcategories=	$request->input('product_categories');
			$product_types=	$request->input('product_types');
			$product_preferences=	$request->input('product_preferences');
			$product->status=	$request->input('status');
			$product->createdby=	0;
			$product->updatedby=	0;
			$product->createddatetime=	Carbon::now()->toDateTimeString();

			$product->save();

			$product_id=$product->id;
			if($picture_single!='')
			{   
				$where=array("product_id"=>$product_id,"main_image"=>1);
				Productimages::where($where)->delete();	
				$productiamges = new Productimages;
				$productiamges->product_id = $product_id;
				$productiamges->product_image  = $picture_single;
				$productiamges->main_image  = 1;
				$productiamges->status  = 1;
				$productiamges->save();				
			}
			if(count($multiple_image))
			{
				$where=array("product_id"=>$id,"main_image"=>0);
				Productimages::where($where)->delete();				
				foreach($multiple_image as $mk=>$mv)
				{
					$productiamges = new Productimages;
					$productiamges->product_id = $product_id;
					$productiamges->product_image  = $mv;
					$productiamges->main_image  = 0;
					$productiamges->status  = 1;
					$productiamges->save();
				}
			}
			
			if(count($productcategories))
			{
				Productcategories::where('product_id', '=', $id)->delete();
				foreach($productcategories as $k=>$v)
				{
					$Productcategories = new Productcategories;
					$Productcategories->product_id = $product_id;
					$Productcategories->cat_id  = $v;
					$Productcategories->createddatetime  = Carbon::now()->toDateTimeString();
					$Productcategories->status  = 1;
					$Productcategories->save();
				}
			}

			if(count($product_types))
			{
				Producttypes::where('product_id', '=', $id)->delete();				
				foreach($product_types as $k=>$v)
				{
					$Producttypes = new Producttypes;
					$Producttypes->product_id = $product_id;
					$Producttypes->type_id  = $v;
					$Producttypes->status  = 1;
					$Producttypes->save();
				}
			}
			if(count($product_preferences))
			{
				Productpreferences::where('product_id', '=', $id)->delete();
				foreach($product_preferences as $k=>$v)
				{
					$Productpreferences = new Productpreferences;
					$Productpreferences->product_id = $product_id;
					$Productpreferences->preference_id  = $v;
					$Productpreferences->producttype_status  = 1;
					$Productpreferences->save();
				}
			}
						if(count($related_product))
			{
				Relatedproduct::where('product_id', '=', $id)->delete();
				foreach($related_product as $k=>$v)
				{
					
				$relatedproduct = new relatedproduct;
				$relatedproduct->product_id = $product_id;
				$relatedproduct->related_id  = $v;
				
				$relatedproduct->save();	
			}
		}
			if(count($productstate))
			{
				Productstate::where('product_id', '=', $id)->delete();
				foreach($productstate as $k=>$v)
				{
				$productstate = new productstate;
				$productstate->product_id = $product_id;
				$productstate->state_id  = $v;
				
				$productstate->save();	
			}
		}
			if(count($productcuts))
			{
				Productcuts::where('product_id', '=', $id)->delete();
				foreach($productcuts as $k=>$v)
				{
				$productcuts = new productcuts;
				$productcuts->product_id = $product_id;
				$productcuts->cut_id  = $v;
				
				$productcuts->save();	
			}
		}
			
			 return redirect('admin/addproduct');

	}
    
    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $categories = DB::table('category')->where('status', 1)->pluck('catname','id');
            $preference = DB::table('preferences')->where('preference_status', 1)->pluck('preference_name','id');
            $types = DB::table('types')->where('producttype_status', 1)->pluck('producttype_name','id');  
              $tab = DB::table('product')->pluck('productname','id');
             $stat = DB::table('state')->pluck('state','id');
             $cutt = DB::table('cuts')->pluck('cut_name','id');
            
            $stat_array=array();
            $type_array=array();
            $cat_array=array();
            $pref_array=array();
            $rel_array=array();
            $cutt_array=array();
            $product = Product::whereId($id)->first();
            $prodcategories = DB::table('productcategories')->where('product_id', $id)->get();
            $prodpreferences = DB::table('productpreference')->where('product_id', $id)->get();
             $prodtypes = DB::table('producttypes')->where('product_id', $id)->get();
            $prodimages = DB::table('productimages')->where('product_id', $id)->get();
            
            $reltabs = DB::table('relatedproduct')->where('product_id', $id)->get();
            $stats = DB::table('productstate')->where('product_id', $id)->get();
          $cutts=DB::table('productcuts')->where('product_id',$id)->get();
            
            $main_image='';
            $image_array=array();
            $combined_image='';
            if($prodimages)
            {
				foreach($prodimages as $k=>$v)
				{
					if($v->main_image==1)
					{
						$main_image=$v->product_image;
					}
					else
					{
						$image_array[]=url("/")."/uploads/public/product_images/".$v->product_image;
					}
				}

				if(count($image_array)){
					$combined_image=$image_array;

				}
			}
            if($prodtypes)
            {
				foreach($prodtypes as $k=>$v)
				{
					$type_array[]=$v->type_id;
				}
			}
            if($prodcategories)
            {
				foreach($prodcategories as $k=>$v)
				{
					$cat_array[]=$v->cat_id;
				}
			}
            if($prodpreferences)
            {
				foreach($prodpreferences as $k=>$v)
				{
					$pref_array[]=$v->preference_id;
				}
			}
            if($reltabs)
            {
				foreach($reltabs as $k=>$v)
				{
					$rel_array[]=$v->related_id;
				}
			}
            if($stats)
            {
				foreach($stats as $k=>$v)
				{
					$stat_array[]=$v->state_id;
				}
			}
           
			if($cutts)
			{
				foreach($cutts as $k=>$v)
				{
					$cutt_array[]=$v->cut_id;
				}
			}
            
         
            
            $tags_array=explode(",",trim($product->product_tags,","));


             $content->body(view('productedit',['cutt'=>$cutt,'cutt_array'=>$cutt_array,'stat'=>$stat,'stat_array'=>$stat_array,'tab'=>$tab,'rel_array'=>$rel_array,'categories'=>$categories,'preference'=>$preference,'types'=>$types,'product'=>$product,'type_array'=>$type_array,'cat_array'=>$cat_array,'pref_array'=>$pref_array,'tags_array'=>$tags_array,'pid'=>$id,'main_image'=>$main_image,'image_array'=>$combined_image]));
             
            //$content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */    
    public function create()
    {
        return Admin::content(function (Content $content) {
			
            $content->header('header');
            $content->description('description');

            $categories = DB::table('category')->where('status', 1)->pluck('catname','id');
            $preference = DB::table('preferences')->where('preference_status', 1)->pluck('preference_name','id');
            $types = DB::table('types')->where('producttype_status', 1)->pluck('producttype_name','id');
            $tab = DB::table('product')->pluck('productname','id');
            $stat = DB::table('state')->pluck('state','id');
            $cutt = DB::table('cuts')->pluck('cut_name','id');
         
            
             $content->body(view('product',['cutt'=>$cutt,'categories'=>$categories,'preference'=>$preference,'types'=>$types,'tab'=>$tab,'stat'=>$stat]));
                         //$content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Product::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->productname('Product Name')->sortable();
            $grid->product_sku('Product SKU')->sortable();
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {
			
			
			//~ echo "<pre>";
			//~ print_r($_FILES);
			//~ exit; 

            //$form->display('id', 'ID');

			$type = [ 1 => 'Combo', 0=> 'Individual'];
            $form->select('product_type ','Product Type')->rules('required')->options($type);

			//$form->html('<span>');
            $form->text('productname', 'Product Name')->rules('required');
            $form->text('productname', 'Product Name')->rules('required');
            $form->textarea('product_description', 'Product Description')->rules('required');
            //$form->html('</span>');
            $form->text('product_sku', 'Product SKU')->rules('required');
            $form->tags('product_tags','Product Tags')->rules('required');
            $form->text('combo_price', 'Price')->rules('required|numeric');
            $form->text('min_quantity', 'Minimum Quantity')->rules('required|integer'); 
            $form->text('max_quantity', 'Maximum Quantity')->rules('required|integer'); 
            $form->text('whole_price', 'Whole Price')->rules('required|numeric');
            $form->text('cleaned_price', 'Cleaned Price')->rules('required|numeric');
            $form->switch('whole_sale');
			//$form->checkbox('whole_sale','Display In Whole Sale')->options([1 => 'Yes']);  

			
            $form->image('productimages.product_image','Main Image')->rules('required')->removable();
            //$form->multipleImage('productimage.product_image','Other Product Images')->uniqueName()->rules('required')->move('public/product_images');
            $form->multipleImage('productimages.multiple_image','Other Product Images')->rules('required');
			          
 			$status = [ 1 => 'Active', 0=> 'In Active'];
            $form->select('status','Status')->rules('required')->options($status);
 
			$form->multipleSelect('productname','Product Categories')->options([1 => 'foo', 2 => 'bar', 'val' => 'Option name']);
 
			 $form->saved(function (Form $form) {
echo "sdf";
exit;

				if ($request->hasFile('productimage.multiple_image')) {
					$files = $request->file('productimage.multiple_image');
					
					echo "<pre>";
					print_r($files);
					exit;
					
					foreach($files as $file){
						$filename = $file->getClientOriginalName();
						$extension = $file->getClientOriginalExtension();
						$picture = date('His').$filename;
						$destinationPath = base_path() . '\public\product_images';
						$file->move($destinationPath, $picture);
					}
				}


				$tt=$form->productimage;
				$tt1=$form->productimagemultiple;
				echo "<pre>";
				print_r($tt);
				print_r($tt1);
				exit;
				

			});
 
 
 
             //$form->display('created_at', 'Created At');
            //$form->display('updated_at', 'Updated At');
        });
    }
}
