<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
|--------------------------------------------------------------------------
| User Create Routes
|--------------------------------------------------------------------------
|
| Welcome Page
| Register Page
| Login Page
| User Details Page
|Search Bar
| 
|
*/

Route::get('/autocomplete',array
('as'=>'autocomplete','uses'=>'SitesController@autocomplete'));

Route::get('/register', function () {
    return view('auth/register');
});


Route::get('/login', function () {
    return view('auth/login');
});

Route::get('/signup', 'SitesController@login');

Route::get('/', 'SitesController@index');
Route::post('/custom', 'SitesController@signin');
Route::get('/first', 'FirstController@FirstMethod');

Route::get('/logout','SitesController@logout');


/*
|--------------------------------------------------------------------------
| Product Routes
|--------------------------------------------------------------------------
|
| Product Page
| Product details
| Checkout details
| Thankyou Page
|
*/


Route::get('/category/{name}/{id}', 'SitesController@product');
Route::get('/state/{name}/{id}', 'SitesController@state');
Route::get('category/{name}/{name1}/{id}', 'SitesController@productdetails');
Route::get('/state/{name}/{name2}/{id}', 'SitesController@productdetail');
Route::get('/checkout','SitesController@checkout');

/*
|
| Wishlist details
|
*/


Route::post('/wishlists', 'SitesController@wishlist');
Route::get('/wishlist', 'SitesController@wishlistitems');
Route::get('/removes/{userid}/{id}/{price}/{cutname}','SitesController@removes');
Route::get('/removeall','SitesController@removeall');
Route::get('/cartadd/{pro_id}/{type}','SitesController@wishlistadd');

/*
|
| Cart details
|
*/

Route::post('/cartadd','SitesController@add');
Route::get('/cartdetail','SitesController@cartview');
Route::get('/remove/{id}','SitesController@remove');
Route::post('/update/{id}','SitesController@update');
Route::post('/thankyou', 'SitesController@thankyou');


Route::get('/detail','SitesController@profile');

Route::post('/updates','SitesController@updates');





Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('auth/facebook', 'SitesController@redirectToFacebook');
Route::get('auth/facebook/callback', 'SitesController@redirectToCallback');
Route::get('auth/google', 'SitesController@redirectToGoogle');
Route::get('auth/callback/google', 'SitesController@callback');


