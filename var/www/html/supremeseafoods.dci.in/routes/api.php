<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| 
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});  

Route::post('/test','\App\Admin\Controllers\ApiController@test');
Route::post('/login','\App\Admin\Controllers\ApiController@login');
Route::post('/register','\App\Admin\Controllers\ApiController@register');
Route::post('/pincodes','\App\Admin\Controllers\ApiController@pincodes');

Route::post('/categories','\App\Admin\Controllers\ApiController@categories');
Route::post('/category_product','\App\Admin\Controllers\ApiController@category_product');
Route::post('/product','\App\Admin\Controllers\ApiController@product_detail');
Route::post('/wishlist','\App\Admin\Controllers\ApiController@wishlist');

Route::post('/user','\App\Admin\Controllers\ApiController@user_detail');
Route::post('/productsearch','\App\Admin\Controllers\ApiController@productsearch');
Route::post('/addressedit','\App\Admin\Controllers\ApiController@addressedit');
Route::post('/addressview','\App\Admin\Controllers\ApiController@addressview');
Route::post('/singleaddressview','\App\Admin\Controllers\ApiController@singleaddressview');
Route::post('/change_delivery_address','\App\Admin\Controllers\ApiController@change_delivery_address');

Route::post('/cuts','\App\Admin\Controllers\ApiController@all_cuts');
Route::post('/change_location','\App\Admin\Controllers\ApiController@change_location');
Route::post('/update_location','\App\Admin\Controllers\ApiController@update_location');
Route::post('/filter','\App\Admin\Controllers\ApiController@product_filter');
Route::post('/forgot_password','\App\Admin\Controllers\ApiController@forgot_password');
Route::post('/wallet','\App\Admin\Controllers\ApiController@wallet');

Route::post('/user_password_update','\App\Admin\Controllers\ApiController@user_password_update');
Route::post('/order_details','\App\Admin\Controllers\ApiController@order_details');
Route::post('/order_histroy','\App\Admin\Controllers\ApiController@order_histroy');
Route::post('/place_order','\App\Admin\Controllers\ApiController@place_order');

Route::post('/recipes_list','\App\Admin\Controllers\ApiController@recipes_list');
Route::post('/recipes_detail','\App\Admin\Controllers\ApiController@recipes_detail');


Route::post('/social_login_user_check','\App\Admin\Controllers\ApiController@social_login_user_check');
Route::post('/social_login_user_save','\App\Admin\Controllers\ApiController@social_login_user_save');

Route::post('/today_product','\App\Admin\Controllers\ApiController@today_product');

Route::post('/addcart','\App\Admin\Controllers\ApiController@addcart');
Route::post('/cartview','\App\Admin\Controllers\ApiController@cartview');
Route::post('/updatecart','\App\Admin\Controllers\ApiController@updatecart');

Route::post('/cancelorder','\App\Admin\Controllers\ApiController@cancelorder');

Route::post('/imagecopy','\App\Admin\Controllers\ApiController@imagecopy');

Route::post('/gustuser','\App\Admin\Controllers\ApiController@gustuser');

Route::post('/product_overall_filter','\App\Admin\Controllers\ApiController@product_overall_filter');


